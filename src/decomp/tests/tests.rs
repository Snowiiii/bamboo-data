use super::Context;
use crate::decomp::{Type, VarKind};
use pretty_assertions::assert_eq;

pub fn run(ctx: Context) {
  // test_empty(&ctx);
  // test_if(&ctx);
  test_switch(&ctx);
}

fn test_empty(ctx: &Context) {
  let class = ctx.decomp(
    "Empty",
    "public class Empty {
      void bar() {}
    }",
  );

  assert!(class.fields.is_empty());

  let names = class.funcs.iter().map(|v| &v.0).collect::<Vec<_>>();
  assert_eq!(names, vec!["<init>", "bar"]);
}

use crate::decomp::*;

fn lit(lit: i32) -> Item {
  Item::new(Value::Lit(lit))
}
fn var(index: usize) -> Item {
  Item::new(Value::Var(index))
}

fn test_if(ctx: &Context) {
  let block = ctx.decomp_func(
    "IfClass",
    "foo",
    "public class IfClass {
      void foo(int a) {
        if (a == 3) {
          System.out.println(\"is 3!\");
        } else {
          System.out.println(\"not 3 :(\");
        }
      }
    }",
  );

  // not sure what the local is doing but might as well ensure it doesn't change.
  assert_eq!(block.vars, vec![VarKind::This, VarKind::Arg(Type::Int), VarKind::Local]);

  assert_eq!(
    block.instr,
    vec![
      Instr::If(
        Cond::EQ(var(1), lit(3)),
        vec![Instr::Expr(
          Item::new(Value::StaticField(
            "java/lang/System".into(),
            Type::Class("java/io/PrintStream".into()),
            "out".into()
          ))
          .with_op(Op::Call(
            "java/io/PrintStream".into(),
            "println".into(),
            Descriptor { args: vec![Type::Class("java/lang/String".into())], ret: Type::Void },
            vec![Item::new("is 3!")]
          ))
        )],
        vec![Instr::Expr(
          Item::new(Value::StaticField(
            "java/lang/System".into(),
            Type::Class("java/io/PrintStream".into()),
            "out".into()
          ))
          .with_op(Op::Call(
            "java/io/PrintStream".into(),
            "println".into(),
            Descriptor { args: vec![Type::Class("java/lang/String".into())], ret: Type::Void },
            vec![Item::new("not 3 :(")]
          ))
        )]
      ),
      Instr::Return(Item::new(Value::Null))
    ]
  );
}

fn test_switch(ctx: &Context) {
  // This produces a `TableSwitch`
  let block = ctx.decomp_func(
    "SwitchClass",
    "foo",
    "public class SwitchClass {
      void foo(int a) {
        int v;
        switch (a) {
          case 1:
            v = 2;
            break;
          case 2:
            v = 4;
            break;
          case 3:
            v = 5;
            break;
          default:
            v = 20;
        }
      }
    }",
  );

  assert_eq!(block.vars, vec![VarKind::This, VarKind::Arg(Type::Int), VarKind::Local]);

  assert_eq!(
    block.instr,
    vec![
      Instr::Switch(
        var(1),
        vec![
          (1, vec![Instr::Assign(2, lit(2))]),
          (2, vec![Instr::Assign(2, lit(4))]),
          (3, vec![Instr::Assign(2, lit(5))]),
        ],
        Some(vec![Instr::Assign(2, lit(20))]),
      ),
      Instr::Return(Item::new(Value::Null))
    ]
  );

  // This produces a `LookupSwitch`
  let block = ctx.decomp_func(
    "SwitchClass",
    "foo",
    "public class SwitchClass {
      void foo(int a) {
        int v;
        switch (a) {
          case 1:
            v = 2;
            break;
          case 20:
            v = 4;
            break;
          case 100:
            v = 5;
            break;
          default:
            v = 20;
        }
      }
    }",
  );

  assert_eq!(block.vars, vec![VarKind::This, VarKind::Arg(Type::Int), VarKind::Local]);

  assert_eq!(
    block.instr,
    vec![
      Instr::Switch(
        var(1),
        vec![
          (1, vec![Instr::Assign(2, lit(2))]),
          (20, vec![Instr::Assign(2, lit(4))]),
          (100, vec![Instr::Assign(2, lit(5))]),
        ],
        Some(vec![Instr::Assign(2, lit(20))]),
      ),
      Instr::Return(Item::new(Value::Null))
    ]
  );
}
