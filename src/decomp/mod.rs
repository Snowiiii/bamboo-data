mod types;
mod util;

#[cfg(test)]
mod tests;

use crate::Version;
use noak::{
  error::DecodeError,
  reader::{
    attributes::{AttributeContent, Index as IndexAttrib, RawInstruction},
    cpool,
    cpool::ConstantPool,
    Class as NClass, Method,
  },
  AccessFlags,
};
use std::{cell::RefCell, cmp::Ordering, collections::HashSet, fmt, mem, rc::Rc};

pub use types::*;
pub use util::{Descriptor, Type};

pub trait NotDecodeError {}

#[derive(Clone, Debug)]
pub enum Error<E: fmt::Debug + NotDecodeError> {
  Decode(DecodeError),
  Other(E),
}

pub type Result<T, E> = std::result::Result<T, Error<E>>;

impl<E: fmt::Debug + NotDecodeError> From<DecodeError> for Error<E> {
  fn from(value: DecodeError) -> Self {
    Error::Decode(value)
  }
}
impl<E: fmt::Debug + NotDecodeError> From<E> for Error<E> {
  fn from(value: E) -> Self {
    Error::Other(value)
  }
}

impl<E: fmt::Display + fmt::Debug + NotDecodeError> fmt::Display for Error<E> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self {
      Self::Decode(error) => write!(f, "{error}"),
      Self::Other(error) => write!(f, "{error}"),
    }
  }
}

pub trait Converter {
  type Err: std::error::Error + fmt::Debug + NotDecodeError;

  /// Returns true if the class file exists for the given deobfuscated name.
  fn has_class_file(&mut self, name: &str) -> bool;
  /// Returns a class file for the given deobfuscated name.
  fn class_file(&mut self, name: &str) -> &[u8];

  /// Returns the deobfuscated class for the given obfuscated name (like
  /// `abcd`).
  #[track_caller]
  fn class(&self, name: &str) -> String;
  /// Returns the obfuscated class for the given deobfuscated name (like
  /// `SPacketFoo`)
  #[track_caller]
  fn class_rev(&self, name: &str) -> String;

  /// Returns the deobfuscated field (like `foo`) for the given obfuscated class
  /// and obfuscated field name (like `a`).
  ///
  /// This might load class files, if the field is on a superclass. If that
  /// happens, an internal cache is updated, so this needs to be &mut.
  #[track_caller]
  fn field(&mut self, class: &str, name: &str) -> std::result::Result<String, Self::Err>;

  /// Returns the deobfuscated method (like `readInt`) for the given obfuscated
  /// class and obfuscated name (like `abcd`).
  ///
  /// This might load class files, if the field is on a superclass. If that
  /// happens, an internal cache is updated, so this needs to be &mut.
  #[track_caller]
  fn func(&mut self, class: &str, name: &str, desc: &str)
    -> std::result::Result<String, Self::Err>;

  /// Returns the version for this converter.
  fn ver(&self) -> Version;
}

#[derive(Debug, Clone)]
pub struct Class {
  pub name:    String,
  pub extends: String,
  pub fields:  Vec<(String, Type)>,
  pub funcs:   Vec<(String, Descriptor, VarBlock)>,
}

fn deobf_ty<C: Converter>(ty: &mut Type, convert: &mut C) {
  match ty {
    Type::Class(name) => {
      if !name.contains('/') {
        *name = convert.class(name);
      }
    }
    Type::Array(ty) => deobf_ty(ty, convert),
    _ => {}
  }
}
pub fn deobf_type<C: Converter>(ty: &str, convert: &mut C) -> Type {
  let mut ty = Type::from_desc(ty).0;
  deobf_ty(&mut ty, convert);
  ty
}
pub fn deobf_desc<C: Converter>(sig: &str, convert: &mut C) -> Descriptor {
  let mut desc = Descriptor::from_desc(sig);
  for a in &mut desc.args {
    deobf_ty(a, convert);
  }
  deobf_ty(&mut desc.ret, convert);
  desc
}

pub fn class<C: Converter>(
  convert: &mut C,
  bytes: &[u8],
  is_valid: impl Fn(&mut C, &str, &Descriptor, AccessFlags) -> bool,
) -> Result<Class, C::Err> {
  class_inner(convert, &mut HashSet::new(), bytes, is_valid)
}

fn class_inner<C: Converter>(
  convert: &mut C,
  call_chain: &mut HashSet<(String, String)>,
  bytes: &[u8],
  is_valid: impl Fn(&mut C, &str, &Descriptor, AccessFlags) -> bool,
) -> Result<Class, C::Err> {
  let mut class = NClass::new(bytes)?;
  let mut fields = vec![];
  let mut funcs = vec![];
  let obf_class = class.this_class_name().unwrap().to_str().unwrap();
  let obf_super_class = class.super_class_name().unwrap().unwrap().to_str().unwrap();

  let mut bootstrap = vec![];
  for a in class.attribute()? {
    let a = a?;
    let pool = class.pool()?;
    // Noak is broken, and this will error out for 1.18 minecraft. So, we ignore
    // those errors.
    if let Ok(AttributeContent::BootstrapMethods(b)) = a.read_content(pool) {
      for m in b.iter() {
        let m = m?;
        // Noak thinks this is MethodRef, but its a MethodHandle.
        let handle =
          pool.retrieve(cpool::Index::<cpool::MethodHandle>::new(m.method_ref().as_u16())?)?;
        if let cpool::Item::MethodRef(method) = handle.reference {
          let method = pool.retrieve(method.name_and_type)?;
          let name = method.name.to_str().unwrap();
          if name == "metafactory" {
            // Noak thinks this is MethodHandle, but its an Item.
            let args: Vec<_> = m
              .arguments()
              .iter()
              .map(|i| i.map(|i| cpool::Index::<cpool::Item>::new(i.as_u16()).unwrap()))
              .collect::<std::result::Result<_, _>>()?;
            assert_eq!(args.len(), 3);
            match pool.get(args[1])? {
              cpool::Item::MethodHandle(handle) => match pool.get(handle.reference)? {
                cpool::Item::MethodRef(method) => {
                  let class = pool.retrieve(method.class)?;
                  let class_name = class.name.to_str().unwrap();
                  let name_and_type = pool.retrieve(method.name_and_type)?;
                  bootstrap.push((
                    convert.class(class_name),
                    convert.func(
                      class_name,
                      name_and_type.name.to_str().unwrap(),
                      name_and_type.descriptor.to_str().unwrap(),
                    )?,
                  ));
                }
                cpool::Item::InterfaceMethodRef(method) => {
                  let class = pool.retrieve(method.class)?;
                  let class_name = class.name.to_str().unwrap();
                  let name_and_type = pool.retrieve(method.name_and_type)?;
                  bootstrap.push((
                    convert.class(class_name),
                    convert.func(
                      class_name,
                      name_and_type.name.to_str().unwrap(),
                      name_and_type.descriptor.to_str().unwrap(),
                    )?,
                  ));
                }
                _ => panic!(),
              },
              _ => panic!(),
            }
          } else {
            // We only handle 'metafactory' right now, but we still need indices to match.
            bootstrap.push(("<unknown>".into(), "<unknown>".into()));
          }
        } else {
          panic!();
        }
      }
    }
  }

  // debug!("class: {}", class.this_class_name()?.to_str().unwrap());
  let class_name = convert.class(obf_class);
  let super_class_name = convert.class(obf_super_class);
  for m in class.methods()? {
    let m = m?;
    let pool = class.pool()?;
    let mut name: String = pool.retrieve(m.name())?.to_str().unwrap().into();
    if name != "<init>" && name != "<clinit>" {
      name = convert.func(obf_class, &name, pool.retrieve(m.descriptor())?.to_str().unwrap())?;
    }
    let sig = deobf_desc(pool.retrieve(m.descriptor())?.to_str().unwrap(), convert);
    if !is_valid(convert, &name, &sig, m.access_flags()) {
      continue;
    }
    call_chain.insert((class_name.clone(), name.clone()));
    let m = method(m, &name, call_chain, pool, &bootstrap, convert, obf_class, &super_class_name)?;
    funcs.push((name, sig, m));
  }
  for f in class.fields()? {
    let f = f?;
    let pool = class.pool()?;
    let name = convert.field(obf_class, pool.retrieve(f.name())?.to_str().unwrap())?;
    let ty = deobf_type(pool.retrieve(f.descriptor())?.to_str().unwrap(), convert);
    fields.push((name, ty));
  }

  let mut sup: String = class.super_class_name()?.unwrap().to_str().unwrap().into();
  // All classes extends from Object or Enum
  if sup != "java/lang/Object" && sup != "java/lang/Enum" {
    sup = convert.class(&sup);
  }
  Ok(Class {
    name: convert
      .class(class.this_class_name().unwrap().to_str().unwrap())
      .split('/')
      .last()
      .unwrap()
      .split('$')
      .last()
      .unwrap()
      .into(),
    extends: sup.split('/').last().unwrap().into(),
    fields,
    funcs,
  })
}

macro_rules! math_op {
  ($state:expr, $name:ident) => {{
    let a = $state.stack().pop();
    let b = $state.stack().pop();
    b.ops.borrow_mut().push(Op::$name(a));
    $state.stack().push(b);
  }};
}

pub fn method<C: Converter>(
  m: Method,
  func_name: &str,
  call_chain: &mut HashSet<(String, String)>,
  pool: &ConstantPool,
  bootstrap: &[(String, String)],
  convert: &mut C,
  obf_class: &str,
  super_class_name: &str,
) -> Result<VarBlock, C::Err> {
  // debug!(
  //   "decompiling method {}#{}",
  //   convert.class(obf_class),
  //   convert.func(
  //     obf_class,
  //     pool.retrieve(m.name())?.to_str().unwrap(),
  //     pool.retrieve(m.descriptor())?.to_str().unwrap()
  //   )
  // );
  for a in m.attributes() {
    if let AttributeContent::Code(code) = a?.read_content(pool)? {
      let mut state = State::new();
      let mut vars =
        Vars::new(&[Item::new(Value::This), Item::new(Value::Buf), Item::new(Value::Null)]);
      for instr in code.raw_instructions() {
        let (idx, instr) = instr?;
        // debug!("state: {:#?}", &state);
        // dbg!(&state.stack());
        // println!("got instr {:?} {:?}", idx, instr);
        state.check_if(&mut vars, idx);
        match instr {
          // --- Memory operations

          // Loads a reference. We keep track of this in the item, so its the same as
          // storeing a value.
          RawInstruction::ALoad { index } => state.stack().push(Value::Var(index as usize).into()),
          RawInstruction::ALoad0 => state.stack().push(Value::Var(0).into()),
          RawInstruction::ALoad1 => state.stack().push(Value::Var(1).into()),
          RawInstruction::ALoad2 => state.stack().push(Value::Var(2).into()),
          RawInstruction::ALoad3 => state.stack().push(Value::Var(3).into()),

          RawInstruction::LLoad0 => state.stack().push(Value::Var(0).into()),
          RawInstruction::LLoad1 => state.stack().push(Value::Var(1).into()),
          RawInstruction::LLoad2 => state.stack().push(Value::Var(2).into()),
          RawInstruction::LLoad3 => state.stack().push(Value::Var(3).into()),

          RawInstruction::AConstNull => state.stack().push(Value::Null.into()),

          RawInstruction::AStore { index } => {
            let v = state.stack().pop();
            vars.store(index as usize, v.clone());
            state.instr(Instr::Assign(index as usize, v));
          }
          RawInstruction::AStore0 => {
            let v = state.stack().pop();
            vars.store(0, v.clone());
            state.instr(Instr::Assign(0, v));
          }
          RawInstruction::AStore1 => {
            let v = state.stack().pop();
            vars.store(1, v.clone());
            state.instr(Instr::Assign(1, v));
          }
          RawInstruction::AStore2 => {
            let v = state.stack().pop();
            vars.store(2, v.clone());
            state.instr(Instr::Assign(2, v));
          }
          RawInstruction::AStore3 => {
            let v = state.stack().pop();
            vars.store(3, v.clone());
            state.instr(Instr::Assign(3, v));
          }

          // Loads a constant
          RawInstruction::IConstM1 => state.stack().push_lit(-1),
          RawInstruction::IConst0 => state.stack().push_lit(0),
          RawInstruction::IConst1 => state.stack().push_lit(1),
          RawInstruction::IConst2 => state.stack().push_lit(2),
          RawInstruction::IConst3 => state.stack().push_lit(3),
          RawInstruction::IConst4 => state.stack().push_lit(4),
          RawInstruction::IConst5 => state.stack().push_lit(5),
          RawInstruction::BIPush { value } => state.stack().push_lit(value as i32),
          RawInstruction::SIPush { value } => state.stack().push_lit(value as i32),
          RawInstruction::FConst0 => state.stack().push_flit(0.0),
          RawInstruction::FConst1 => state.stack().push_flit(1.0),
          RawInstruction::FConst2 => state.stack().push_flit(2.0),
          RawInstruction::DConst0 => state.stack().push_flit(0.0),
          RawInstruction::DConst1 => state.stack().push_flit(1.0),
          RawInstruction::LConst0 => state.stack().push_lit(0),
          RawInstruction::LConst1 => state.stack().push_lit(0),

          // Stores a value from the stack.
          RawInstruction::IStore0 => {
            let v = state.stack().pop();
            vars.store(0, v.clone());
            state.instr(Instr::Assign(0, v));
          }
          RawInstruction::IStore1 => {
            let v = state.stack().pop();
            vars.store(1, v.clone());
            state.instr(Instr::Assign(1, v));
          }
          RawInstruction::IStore2 => {
            let v = state.stack().pop();
            vars.store(2, v.clone());
            state.instr(Instr::Assign(2, v));
          }
          RawInstruction::IStore3 => {
            let v = state.stack().pop();
            vars.store(3, v.clone());
            state.instr(Instr::Assign(3, v));
          }
          RawInstruction::IStore { index } => {
            let v = state.stack().pop();
            vars.store(index as usize, v.clone());
            state.instr(Instr::Assign(index as usize, v));
          }

          RawInstruction::FStore0 => {
            let v = state.stack().pop();
            vars.store(0, v.clone());
            state.instr(Instr::Assign(0, v));
          }
          RawInstruction::FStore1 => {
            let v = state.stack().pop();
            vars.store(1, v.clone());
            state.instr(Instr::Assign(1, v));
          }
          RawInstruction::FStore2 => {
            let v = state.stack().pop();
            vars.store(2, v.clone());
            state.instr(Instr::Assign(2, v));
          }
          RawInstruction::FStore3 => {
            let v = state.stack().pop();
            vars.store(3, v.clone());
            state.instr(Instr::Assign(3, v));
          }
          RawInstruction::FStore { index } => {
            let v = state.stack().pop();
            vars.store(index as usize, v.clone());
            state.instr(Instr::Assign(index as usize, v));
          }

          RawInstruction::DStore0 => {
            let v = state.stack().pop();
            vars.store_64(0, v.clone());
            state.instr(Instr::Assign(0, v));
          }
          RawInstruction::DStore1 => {
            let v = state.stack().pop();
            vars.store_64(1, v.clone());
            state.instr(Instr::Assign(1, v));
          }
          RawInstruction::DStore2 => {
            let v = state.stack().pop();
            vars.store_64(2, v.clone());
            state.instr(Instr::Assign(2, v));
          }
          RawInstruction::DStore3 => {
            let v = state.stack().pop();
            vars.store_64(3, v.clone());
            state.instr(Instr::Assign(3, v));
          }
          RawInstruction::DStore { index } => {
            let v = state.stack().pop();
            vars.store_64(index as usize, v.clone());
            state.instr(Instr::Assign(index as usize, v));
          }
          RawInstruction::LStore { index } => {
            let v = state.stack().pop();
            vars.store_64(index as usize, v.clone());
            state.instr(Instr::Assign(index as usize, v));
          }

          // Loads a value to the stack. These are copies in the actual JVM, but we need to
          // track everything that happens to these values, so we just push references.
          RawInstruction::ILoad { index } => state.stack().push(Value::Var(index as usize).into()),
          RawInstruction::ILoad0 => state.stack().push(Value::Var(0).into()),
          RawInstruction::ILoad1 => state.stack().push(Value::Var(1).into()),
          RawInstruction::ILoad2 => state.stack().push(Value::Var(2).into()),
          RawInstruction::ILoad3 => state.stack().push(Value::Var(3).into()),

          RawInstruction::FLoad0 => state.stack().push(Value::Var(0).into()),
          RawInstruction::FLoad1 => state.stack().push(Value::Var(1).into()),
          RawInstruction::FLoad2 => state.stack().push(Value::Var(2).into()),
          RawInstruction::FLoad3 => state.stack().push(Value::Var(3).into()),

          RawInstruction::DLoad0 => state.stack().push(Value::Var(0).into()),
          RawInstruction::DLoad1 => state.stack().push(Value::Var(1).into()),
          RawInstruction::DLoad2 => state.stack().push(Value::Var(2).into()),
          RawInstruction::DLoad3 => state.stack().push(Value::Var(3).into()),

          RawInstruction::FLoad { index } => state.stack().push(Value::Var(index as usize).into()),
          RawInstruction::DLoad { index } => state.stack().push(Value::Var(index as usize).into()),
          RawInstruction::LLoad { index } => state.stack().push(Value::Var(index as usize).into()),

          // --- Stack manipulation

          // v = stack.pop(), stack.push(v), stack.push(v)
          RawInstruction::Dup => {
            let v = state.stack().pop();
            state.stack().push(v.clone());
            state.stack().push(v);
          }
          RawInstruction::Pop => {
            let v = state.stack().pop();
            state.instr(Instr::Expr(v));
          }
          RawInstruction::LdC { index } => match pool.get(index)? {
            cpool::Item::String(string) => {
              state.stack().push(Item::new(Value::String(
                pool.get(string.string)?.content.to_str().unwrap().into(),
              )));
            }
            cpool::Item::Integer(int) => {
              state.stack().push(Item::new(Value::Lit(int.value)));
            }
            cpool::Item::Float(float) => {
              state.stack().push(Item::new(Value::LitFloat(float.value)));
            }
            cpool::Item::Class(class) => {
              state.stack().push(Item::new(Value::Class(
                convert.class(pool.get(class.name)?.content.to_str().unwrap()),
              )));
            }
            v => panic!("got unknown lcd {:?}", v),
          },
          RawInstruction::LdCW { index } => match pool.get(index)? {
            cpool::Item::String(string) => {
              state.stack().push(Item::new(Value::String(
                pool.get(string.string)?.content.to_str().unwrap().into(),
              )));
            }
            cpool::Item::Integer(int) => {
              state.stack().push(Item::new(Value::Lit(int.value)));
            }
            cpool::Item::Float(float) => {
              state.stack().push(Item::new(Value::LitFloat(float.value)));
            }
            cpool::Item::Class(class) => {
              state.stack().push(Item::new(Value::Class(
                convert.class(pool.get(class.name)?.content.to_str().unwrap()),
              )));
            }
            v => panic!("got unknown lcdw {:?}", v),
          },
          RawInstruction::LdC2W { index } => match pool.get(index)? {
            cpool::Item::Double(v) => {
              state.stack().push(Item::new(Value::LitFloat(v.value as f32)));
            }
            cpool::Item::Long(v) => {
              state.stack().push(Item::new(Value::Lit(v.value.try_into().unwrap())));
            }
            v => panic!("got unknown lcd2w {:?}", v),
          },

          // --- Operators

          // For all math ops, the pops are in reverse order.
          RawInstruction::IAnd => math_op!(state, And),
          RawInstruction::LAnd => math_op!(state, And),
          RawInstruction::IOr => math_op!(state, Or),
          RawInstruction::IAdd => math_op!(state, Add),
          RawInstruction::ISub => math_op!(state, Sub),
          RawInstruction::IMul => math_op!(state, Mul),
          RawInstruction::IDiv => math_op!(state, Div),
          RawInstruction::FAdd => math_op!(state, Add),
          RawInstruction::FSub => math_op!(state, Sub),
          RawInstruction::FMul => math_op!(state, Mul),
          RawInstruction::FDiv => math_op!(state, Div),
          RawInstruction::DAdd => math_op!(state, Add),
          RawInstruction::DSub => math_op!(state, Sub),
          RawInstruction::DMul => math_op!(state, Mul),
          RawInstruction::DDiv => math_op!(state, Div),
          RawInstruction::IShR => math_op!(state, Shr),
          RawInstruction::LRem => math_op!(state, Rem),
          RawInstruction::LShR => math_op!(state, Shr),
          RawInstruction::LUShR => math_op!(state, UShr),
          RawInstruction::IShL => math_op!(state, Shl),
          // vars[index] += value
          RawInstruction::IInc { index, value } => {
            vars
              .load_mut(index as usize)
              .ops
              .borrow_mut()
              .push(Op::Add(Item::new(Value::Lit(value as i32))));
          }
          // Negate the long ontop of the stack
          RawInstruction::LNeg => {
            let v = state.stack().pop();
            v.ops.borrow_mut().push(Op::Neg);
            state.stack().push(v);
          }

          // --- Arrays

          // arr = stack.pop(), idx = stack.pop(), stack.push(arr[idx])
          RawInstruction::AALoad => {
            let idx = state.stack().pop();
            let arr = state.stack().pop();
            let mut ops = arr.ops.borrow().clone();
            ops.push(Op::Get(idx));
            state.stack().push(Item { initial: arr.initial, ops: Rc::new(RefCell::new(ops)) });
          }
          // arr = stack.pop(), idx = stack.pop(), stack.push(arr[idx])
          RawInstruction::SALoad => {
            let idx = state.stack().pop();
            let arr = state.stack().pop();
            let mut ops = arr.ops.borrow().clone();
            ops.push(Op::Get(idx));
            state.stack().push(Item { initial: arr.initial, ops: Rc::new(RefCell::new(ops)) });
          }
          // arr = stack.pop(), idx = stack.pop(), stack.push(arr[idx])
          RawInstruction::IALoad => {
            let idx = state.stack().pop();
            let arr = state.stack().pop();
            let mut ops = arr.ops.borrow().clone();
            ops.push(Op::Get(idx));
            state.stack().push(Item { initial: arr.initial, ops: Rc::new(RefCell::new(ops)) });
          }
          // stack.push(stack.pop().len())
          RawInstruction::ArrayLength => {
            let arr = state.stack().pop();
            arr.ops.borrow_mut().push(Op::Len);
            state.stack().push(arr);
          }
          // val = stack.pop(), idx = stack.pop(), arr = stack.pop(), arr[idx] = val
          RawInstruction::IAStore => {
            let val = state.stack().pop();
            let idx = state.stack().pop();
            let arr = state.stack().pop();
            state.instr(Instr::SetArr(arr.deep_clone(), idx.clone(), val.clone()));
            arr.ops.borrow_mut().push(Op::Set(idx, val));
          }
          // val = stack.pop(), idx = stack.pop(), arr = stack.pop(), arr[idx] = val
          RawInstruction::FAStore => {
            let val = state.stack().pop();
            let idx = state.stack().pop();
            let arr = state.stack().pop();
            state.instr(Instr::SetArr(arr.deep_clone(), idx.clone(), val.clone()));
            arr.ops.borrow_mut().push(Op::Set(idx, val));
          }
          // val = stack.pop(), idx = stack.pop(), arr = stack.pop(), arr[idx] = val
          RawInstruction::SAStore => {
            let val = state.stack().pop();
            let idx = state.stack().pop();
            let arr = state.stack().pop();
            state.instr(Instr::SetArr(arr.deep_clone(), idx.clone(), val.clone()));
            arr.ops.borrow_mut().push(Op::Set(idx, val));
          }
          // val = stack.pop(), idx = stack.pop(), arr = stack.pop(), arr[idx] = val
          RawInstruction::AAStore => {
            let val = state.stack().pop();
            let idx = state.stack().pop();
            let arr = state.stack().pop();
            state.instr(Instr::SetArr(arr.deep_clone(), idx.clone(), val.clone()));
            arr.ops.borrow_mut().push(Op::Set(idx, val));
          }
          // --- Casts ---
          RawInstruction::I2B => {
            let v = state.stack().pop();
            v.ops.borrow_mut().push(Op::Cast(Type::Byte));
            state.stack().push(v);
          }
          RawInstruction::I2S => {
            let v = state.stack().pop();
            v.ops.borrow_mut().push(Op::Cast(Type::Short));
            state.stack().push(v);
          }
          RawInstruction::L2I | RawInstruction::F2I | RawInstruction::D2I => {
            let v = state.stack().pop();
            v.ops.borrow_mut().push(Op::Cast(Type::Int));
            state.stack().push(v);
          }
          RawInstruction::I2L | RawInstruction::F2L | RawInstruction::D2L => {
            let v = state.stack().pop();
            v.ops.borrow_mut().push(Op::Cast(Type::Long));
            state.stack().push(v);
          }
          RawInstruction::I2F | RawInstruction::L2F | RawInstruction::D2F => {
            let v = state.stack().pop();
            v.ops.borrow_mut().push(Op::Cast(Type::Float));
            state.stack().push(v);
          }
          RawInstruction::I2D | RawInstruction::L2D | RawInstruction::F2D => {
            let v = state.stack().pop();
            v.ops.borrow_mut().push(Op::Cast(Type::Double));
            state.stack().push(v);
          }

          // --- Fields

          // stack.push(stack.pop()[index]). We assume the field is this.
          RawInstruction::GetField { index } => {
            let this = state.stack().pop();
            // assert_eq!(this.load(&vars).initial, Value::Var("this".into()));
            let field = pool.retrieve(index)?;
            let f = convert.field(
              field.class.name.to_str().unwrap(),
              field.name_and_type.name.to_str().unwrap(),
            )?;
            if this.initial == Value::this() {
              state.stack().push_field(&f);
            } else {
              state.stack().push(Item::new(Value::Field(Box::new(this), f)));
            }
          }
          RawInstruction::GetStatic { index } => {
            let field = pool.retrieve(index)?;
            let class = field.class.name.to_str().unwrap();
            let mut ty = Type::from_desc(field.name_and_type.descriptor.to_str().unwrap()).0;
            if let Type::Class(name) = &mut ty {
              if !name.starts_with("java/")
                && !name.starts_with("org/")
                && !name.starts_with("com/")
                && !name.starts_with("it/")
              {
                *name = convert.class(name);
              }
            }
            state.stack().push(Item::new(Value::StaticField(
              convert.class(class),
              ty,
              convert.field(class, field.name_and_type.name.to_str().unwrap())?,
            )));
          }

          // --- Constructors

          // stack.push(new ty()) (constructor called later)
          RawInstruction::New { index } => {
            let mut name: String = pool.retrieve(index)?.name.to_str().unwrap().into();
            if !name.starts_with("com/") && !name.starts_with("java/") && !name.starts_with("it/") {
              name = convert.class(&name);
            }
            state.stack().push(Item::new(Value::Class(name)));
          }
          // stack.push(new ty[stack.pop()])
          RawInstruction::NewArray { atype: _ } => {
            let len = state.stack().pop();
            state.stack().push(Item::new(Value::Array(Box::new(len))));
          }
          // stack.push(new ty[stack.pop()])
          RawInstruction::ANewArray { index: _ } => {
            let len = state.stack().pop();
            state.stack().push(Item::new(Value::Array(Box::new(len))));
          }

          // --- Control flow

          // jmp(idx)
          RawInstruction::Goto { offset } => {
            state.goto((idx.as_u32() as i32 + offset as i32) as u32, offset.into());
          }
          // if(stack.pop() < 0) { goto idx }
          RawInstruction::IfLt { offset } => {
            let cond = Cond::LT(state.stack().pop(), Item::new(Value::Lit(0)));
            state.add_if(cond, (idx.as_u32() as i32 + offset as i32) as u32, idx.as_u32())
          }
          // if(stack.pop() <= 0) { goto idx }
          RawInstruction::IfLe { offset } => {
            let cond = Cond::LE(state.stack().pop(), Item::new(Value::Lit(0)));
            state.add_if(cond, (idx.as_u32() as i32 + offset as i32) as u32, idx.as_u32())
          }
          // if(stack.pop() >= 0) { goto idx }
          RawInstruction::IfGe { offset } => {
            let cond = Cond::GE(state.stack().pop(), Item::new(Value::Lit(0)));
            state.add_if(cond, (idx.as_u32() as i32 + offset as i32) as u32, idx.as_u32())
          }
          // if(stack.pop() == 0) { goto idx }
          RawInstruction::IfEq { offset } => {
            let cond = Cond::EQ(state.stack().pop(), Item::new(Value::Lit(0)));
            state.add_if(cond, (idx.as_u32() as i32 + offset as i32) as u32, idx.as_u32())
          }
          // if(stack.pop() != 0) { goto idx }
          RawInstruction::IfNe { offset } => {
            let cond = Cond::NE(state.stack().pop(), Item::new(Value::Lit(0)));
            state.add_if(cond, (idx.as_u32() as i32 + offset as i32) as u32, idx.as_u32())
          }
          // if(stack.pop() == null) { goto idx }
          RawInstruction::IfNull { offset } => {
            let cond = Cond::EQ(state.stack().pop(), Item::new(Value::Null));
            state.add_if(cond, (idx.as_u32() as i32 + offset as i32) as u32, idx.as_u32())
          }
          // stack.push(stack.pop() <=> stack.pop())
          RawInstruction::LCmp => {
            let rhs = state.stack().pop();
            let lhs = state.stack().pop();
            lhs.ops.borrow_mut().push(Op::Cmp(rhs));
            state.stack().push(lhs);
          }
          // stack.push(stack.pop() <=> stack.pop())
          RawInstruction::FCmpL => {
            let rhs = state.stack().pop();
            let lhs = state.stack().pop();
            lhs.ops.borrow_mut().push(Op::Cmp(rhs));
            state.stack().push(lhs);
          }
          // if(stack.pop() <= stack.pop()) { goto idx }
          RawInstruction::IfICmpLe { offset } => {
            let rhs = state.stack().pop();
            let lhs = state.stack().pop();
            state.add_if(
              Cond::LE(lhs, rhs),
              (idx.as_u32() as i32 + offset as i32) as u32,
              idx.as_u32(),
            )
          }
          // if(stack.pop() < stack.pop()) { goto idx }
          RawInstruction::IfICmpLt { offset } => {
            let rhs = state.stack().pop();
            let lhs = state.stack().pop();
            state.add_if(
              Cond::LT(lhs, rhs),
              (idx.as_u32() as i32 + offset as i32) as u32,
              idx.as_u32(),
            )
          }
          // if(stack.pop() > stack.pop()) { goto idx }
          RawInstruction::IfICmpGt { offset } => {
            let rhs = state.stack().pop();
            let lhs = state.stack().pop();
            state.add_if(
              Cond::GT(lhs, rhs),
              (idx.as_u32() as i32 + offset as i32) as u32,
              idx.as_u32(),
            )
          }
          // if(stack.pop() != stack.pop()) { goto idx }
          RawInstruction::IfICmpNe { offset } => {
            let rhs = state.stack().pop();
            let lhs = state.stack().pop();
            state.add_if(
              Cond::NE(lhs, rhs),
              (idx.as_u32() as i32 + offset as i32) as u32,
              idx.as_u32(),
            )
          }
          // if(stack.pop() == stack.pop()) { goto idx }
          RawInstruction::IfICmpEq { offset } => {
            let rhs = state.stack().pop();
            let lhs = state.stack().pop();
            state.add_if(
              Cond::NE(lhs, rhs),
              (idx.as_u32() as i32 + offset as i32) as u32,
              idx.as_u32(),
            )
          }
          // if(stack.pop() != stack.pop()) { goto idx }
          RawInstruction::IfACmpNe { offset } => {
            let rhs = state.stack().pop();
            let lhs = state.stack().pop();
            state.add_if(
              Cond::NE(lhs, rhs),
              (idx.as_u32() as i32 + offset as i32) as u32,
              idx.as_u32(),
            )
          }
          // if(stack.pop() == stack.pop()) { goto idx }
          RawInstruction::IfACmpEq { offset } => {
            let rhs = state.stack().pop();
            let lhs = state.stack().pop();
            state.add_if(
              Cond::EQ(lhs, rhs),
              (idx.as_u32() as i32 + offset as i32) as u32,
              idx.as_u32(),
            )
          }
          // if(stack.pop() >= stack.pop()) { goto idx }
          RawInstruction::IfICmpGe { offset } => {
            let rhs = state.stack().pop();
            let lhs = state.stack().pop();
            state.add_if(
              Cond::GE(lhs, rhs),
              (idx.as_u32() as i32 + offset as i32) as u32,
              idx.as_u32(),
            )
          }
          // if(stack.pop() != null) { goto idx }
          RawInstruction::IfNonNull { offset } => {
            let cond = Cond::NE(state.stack().pop(), Item::new(Value::Lit(0)));
            state.add_if(cond, (idx.as_u32() as i32 + offset as i32) as u32, idx.as_u32())
          }
          // actual magic
          RawInstruction::TableSwitch(ref table) => {
            let v = state.stack().pop();
            state.switch(
              v,
              table.pairs().map(|p| (p.key(), p.offset() + idx.as_u32() as i32)).collect(),
            );
          }
          // like a switch, but different
          RawInstruction::LookupSwitch(ref table) => {
            let v = state.stack().pop();
            state.switch(
              v,
              table.pairs().map(|p| (p.key(), p.offset() + idx.as_u32() as i32)).collect(),
            );
          }

          // --- Functions

          // static(stack.pop()*)
          RawInstruction::InvokeStatic { index } => match pool.get(index)? {
            cpool::Item::MethodRef(func) => {
              let name_and_type = pool.retrieve(func.name_and_type)?;
              let mut class_name: String = pool.retrieve(func.class)?.name.to_str().unwrap().into();
              let mut name = name_and_type.name.to_str().unwrap().into();
              if !(class_name.starts_with("com/") || class_name.starts_with("java/")) {
                name = convert.func(
                  &class_name,
                  name_and_type.name.to_str().unwrap(),
                  name_and_type.descriptor.to_str().unwrap(),
                )?;
                class_name = convert.class(&class_name);
              }
              let desc = Descriptor::from_desc(name_and_type.descriptor.to_str().unwrap());
              let mut args: Vec<_> = desc.args.iter().map(|_a| state.stack().pop()).collect();
              args.reverse();
              if desc.ret == Type::Void {
                state.instr(Instr::Expr(Item::new(Value::StaticCall(class_name, name, args))));
              } else {
                state.stack().push(Item::new(Value::StaticCall(class_name, name, args)));
              }
            }
            cpool::Item::InterfaceMethodRef(func) => {
              let name_and_type = pool.retrieve(func.name_and_type)?;
              let mut class_name: String = pool.retrieve(func.class)?.name.to_str().unwrap().into();
              let mut name = name_and_type.name.to_str().unwrap().into();
              if !(class_name.starts_with("com/") || class_name.starts_with("java/")) {
                name = convert.func(
                  &class_name,
                  name_and_type.name.to_str().unwrap(),
                  name_and_type.descriptor.to_str().unwrap(),
                )?;
                class_name = convert.class(&class_name);
              }
              let desc = Descriptor::from_desc(name_and_type.descriptor.to_str().unwrap());
              let mut args: Vec<_> = desc.args.iter().map(|_a| state.stack().pop()).collect();
              args.reverse();
              if desc.ret == Type::Void {
                state.instr(Instr::Expr(Item::new(Value::StaticCall(class_name, name, args))));
              } else {
                state.stack().push(Item::new(Value::StaticCall(class_name, name, args)));
              }
            }
            i => panic!("got item {:?}", i),
          },
          // args = [stack.pop()*], this = stack.pop(), this.virtual(args)
          RawInstruction::InvokeVirtual { index } => {
            let func = pool.retrieve(index)?;
            let mut class_name: String = func.class.name.to_str().unwrap().into();
            let mut name = func.name_and_type.name.to_str().unwrap().into();
            if !class_name.starts_with("com/")
              && !class_name.starts_with("java/")
              && name != "ordinal"
            {
              name = convert.func(
                &class_name,
                func.name_and_type.name.to_str().unwrap(),
                func.name_and_type.descriptor.to_str().unwrap(),
              )?;
              class_name = convert.class(&class_name);
            }
            let desc = Descriptor::from_desc(func.name_and_type.descriptor.to_str().unwrap());
            let mut args: Vec<_> = desc.args.iter().map(|_a| state.stack().pop()).collect();
            args.reverse();
            let this = state.stack().pop();
            // If this call returns void, we clone `this`, then add an op and call it a day.
            // If this doesn't return void, then we are going to do something with the
            // return value, so we store it as an op.
            if desc.ret == Type::Void {
              let tmp = this.deep_clone();
              tmp.ops.borrow_mut().push(Op::Call(class_name, name, desc, args));
              state.instr(Instr::Expr(tmp));
            } else {
              this.ops.borrow_mut().push(Op::Call(class_name, name, desc, args));
              state.stack().push(this);
            }
          }
          RawInstruction::InvokeDynamic { index } => {
            let func = pool.retrieve(index)?;
            let (b_class, b_name) = &bootstrap[func.bootstrap_method_attr as usize];
            let _name = func.name_and_type.name.to_str().unwrap();
            let desc = Descriptor::from_desc(func.name_and_type.descriptor.to_str().unwrap());
            let mut args: Vec<_> = desc.args.iter().map(|_a| state.stack().pop()).collect();
            args.reverse();
            // Args is now a list of arguments passed to metafactory. Essentially, the
            // arguments are all of the local variables that the closure will capture. So,
            // this means the argsuments list acts as a mapping between local variables in
            // this function and local variables in the closure.
            if desc.ret != Type::Void {
              if convert.has_class_file(b_class)
                  // This prevents recursion
                && !call_chain.contains(&(b_class.clone(), b_name.clone()))
              {
                let file = convert.class_file(b_class).to_vec();
                let mut class = class_inner(convert, call_chain, &file, |_, name, _, flags| {
                  name == b_name && flags.contains(AccessFlags::PRIVATE)
                })?;
                if let Some((_, _, block)) = class.funcs.pop() {
                  state.stack().push(Item::new(Value::Closure(vec![], block)));
                } else {
                  state.stack().push(Item::new(Value::MethodRef(b_class.clone(), b_name.clone())));
                }
              } else {
                state.stack().push(Item::new(Value::MethodRef(b_class.clone(), b_name.clone())));
              }
            }
          }
          // args = [stack.pop()*], this = stack.pop(), this.virtual(args)
          RawInstruction::InvokeSpecial { index } => match pool.get(index)? {
            cpool::Item::MethodRef(func) => {
              let mut class_name: String = pool.retrieve(func.class)?.name.to_str().unwrap().into();
              let name_and_type = pool.retrieve(func.name_and_type)?;
              let mut name = name_and_type.name.to_str().unwrap().into();
              if name != "<init>" {
                name = convert.func(
                  &class_name,
                  name_and_type.name.to_str().unwrap(),
                  name_and_type.descriptor.to_str().unwrap(),
                )?;
                class_name = convert.class(&class_name);
              }
              let desc = Descriptor::from_desc(name_and_type.descriptor.to_str().unwrap());
              let mut args: Vec<_> = desc.args.iter().map(|_a| state.stack().pop()).collect();
              args.reverse();
              let this = state.stack().pop();
              if desc.ret == Type::Void {
                // When constructing inner classes, we get a `this` as the first
                // argument. This is nonsense, and we need to remove it.
                /*
                if args.get(0) == Some(&Item::new(Value::this())) {
                  args.remove(0);
                }
                */
              }
              if class_name == super_class_name && name == func_name {
                state.instr(Instr::Super(args));
                if desc.ret != Type::Void {
                  state.stack().push(this);
                }
              } else {
                this.ops.borrow_mut().push(Op::Call(
                  class_name,
                  name,
                  desc.clone().deobf(convert),
                  args,
                ));
                if desc.ret == Type::Void {
                  state.instr(Instr::Expr(this));
                } else {
                  state.stack().push(this);
                }
              }
            }
            i => debug!("got item {:?}", i),
          },
          // Make sure the value ontop of the stack is the right type (nop).
          RawInstruction::CheckCast { index: _ } => {}
          // Make sure the value ontop of the stack is an instanceof index (nop).
          RawInstruction::InstanceOf { index: _ } => {}
          // args = [stack.pop()*], this = stack.pop(), this.virtual(args)
          //
          // Count is historical. It is the number of arguments to the function, and can be
          // ignored.
          RawInstruction::InvokeInterface { index, count: _ } => {
            let func = pool.retrieve(index)?;
            let mut class_name: String = func.class.name.to_str().unwrap().into();
            let mut name = func.name_and_type.name.to_str().unwrap().into();
            if !class_name.starts_with("java/") {
              name = convert.func(
                &class_name,
                func.name_and_type.name.to_str().unwrap(),
                func.name_and_type.descriptor.to_str().unwrap(),
              )?;
              class_name = convert.class(&class_name);
            }
            let desc = Descriptor::from_desc(func.name_and_type.descriptor.to_str().unwrap());
            let args: Vec<_> = desc
              .args
              .iter()
              .rev() // We need to pop in reverse order
              .map(|_a| state.stack().pop())
              .collect();
            let this = state.stack().pop();
            // assert_eq!(this.initial, Value::Var("this".into()));
            if desc.ret == Type::Void {
              let tmp = this.deep_clone();
              tmp.ops.borrow_mut().push(Op::Call(class_name, name, desc, args));
              state.instr(Instr::Expr(tmp));
            } else {
              this.ops.borrow_mut().push(Op::Call(class_name, name, desc, args));
              state.stack().push(this);
            }
          }

          // --- Fields

          // val = stack.pop(), this = stack.pop(), this.index = val
          RawInstruction::PutField { index } => {
            let val = state.stack().pop();
            let this = state.stack().pop();
            // assert_eq!(this.initial, Value::Var("this".into()));
            let field = pool.retrieve(index)?;
            if this.initial == Value::this() {
              state.instr(Instr::SetField(
                convert.field(
                  field.class.name.to_str().unwrap(),
                  field.name_and_type.name.to_str().unwrap(),
                )?,
                val,
              ));
            } else if let Value::Field(initial, name) = this.initial {
              // This is about the worst hack I can think of. However, it will work, so I
              // couldn't care less :P.
              if initial.initial == Value::this() {
                state.instr(Instr::SetField(
                  format!(
                    "{}.{}",
                    name,
                    convert.field(
                      field.class.name.to_str().unwrap(),
                      field.name_and_type.name.to_str().unwrap()
                    )?
                  ),
                  val,
                ));
              }
            }
          }
          // val = stack.pop(), this = stack.pop(), this.index = val
          RawInstruction::PutStatic { index } => {
            let val = state.stack().pop();
            let field = pool.retrieve(index)?;
            if field.class.name.to_str().unwrap() == obf_class {
              state.instr(Instr::SetField(
                convert.field(
                  field.class.name.to_str().unwrap(),
                  field.name_and_type.name.to_str().unwrap(),
                )?,
                val,
              ));
            }
          }

          // --- Special

          // This instruction pops an item, clears the stack, then pushes that item to be
          // thrown. We don't really care about that, and it would add a bunch of edge cases
          // if we could clear the stack. So, this ends up being a nop.
          RawInstruction::AThrow => {}
          // Return void from the function. Doesn't modify the stack.
          RawInstruction::Return => {
            state.instr(Instr::Return(Item::new(Value::Null)));
            if state.statements.is_empty() {
              break;
            }
          }
          // Return a value from the function. Pops the value off the stack.
          RawInstruction::IReturn
          | RawInstruction::FReturn
          | RawInstruction::DReturn
          | RawInstruction::LReturn
          | RawInstruction::AReturn => {
            let v = state.stack().pop();
            state.instr(Instr::Return(v));
            if state.statements.is_empty() {
              break;
            }
          }
          ref i => panic!("unhandled instruction {:?}", i),
        }
      }
      if !state.statements.is_empty() {
        let stat = state.statements.pop().unwrap();
        if state.statements.is_empty() && matches!(stat, Statement::Switch { .. }) {
          if let Statement::Switch { val, table, def, .. } = stat {
            state.instr(Instr::Switch(
              val,
              table.into_iter().map(|(key, _offset, block)| (key, block)).collect(),
              def,
            ));
          } else {
            unreachable!()
          }
        } else {
          // For the error messsage.
          state.statements.push(stat);
          if convert.class(obf_class) != "net/minecraft/block/CarvedPumpkinBlock" {
            error!(
              "while handling class {} func {}",
              convert.class(obf_class),
              convert.func(
                obf_class,
                pool.get(m.descriptor())?.content.to_str().unwrap(),
                pool.get(m.name())?.content.to_str().unwrap()
              )?
            );
            panic!("unprocessed statements: {:#?}", state.statements);
          }
        }
      }
      return Ok(VarBlock::new(
        m.access_flags().contains(AccessFlags::STATIC),
        Descriptor::from_desc(pool.retrieve(m.descriptor())?.to_str().unwrap()),
        vars.items.len(),
        state.instr,
      ));
    }
  }
  panic!("did not find code attribute");
}

impl Statement {
  pub fn instr(&mut self, instr: Instr) {
    match self {
      Statement::Cond { in_else, when_true, when_false, .. } => {
        if *in_else {
          when_false.push(instr);
        } else {
          when_true.push(instr);
        }
      }
      Statement::Switch { curr, table, def, .. } => {
        if *curr == table.len() {
          def.as_mut().unwrap().push(instr);
        } else {
          table[*curr].2.push(instr)
        }
      }
    }
  }
  pub fn stack(&mut self) -> &mut Stack {
    match self {
      Statement::Cond { in_else, stack_true, stack_false, .. } => {
        if *in_else {
          stack_false
        } else {
          stack_true
        }
      }
      Statement::Switch { stack, .. } => stack,
    }
  }
  pub fn stack_ref(&self) -> &Stack {
    match self {
      Statement::Cond { in_else, stack_true, stack_false, .. } => {
        if *in_else {
          stack_false
        } else {
          stack_true
        }
      }
      Statement::Switch { stack, .. } => stack,
    }
  }
}

impl Vars {
  pub fn new(initial: &[Item]) -> Self {
    Vars { items: initial.into() }
  }

  /// Stores a single item on the variables list.
  pub fn store(&mut self, idx: usize, v: Item) {
    match idx.cmp(&self.items.len()) {
      Ordering::Less => self.items[idx] = v,
      Ordering::Equal => self.items.push(v),
      Ordering::Greater => {
        for _ in self.items.len()..idx {
          self.items.push(Item::new(Value::Null));
        }
        self.items.push(v);
      }
    }
  }
  /// Stores a 64 bit item into the variables list. This takes up two slots, so
  /// this is used to make future indexing work correctly.
  pub fn store_64(&mut self, idx: usize, v: Item) {
    self.store(idx, v);
    self.store(idx + 1, Item::new(Value::Partial64));
  }

  pub fn load_mut(&mut self, idx: usize) -> &mut Item {
    &mut self.items[idx]
  }
}

impl Stack {
  pub fn new() -> Self {
    Stack { items: vec![] }
  }
  pub fn push(&mut self, val: Item) {
    self.items.push(val);
  }
  pub fn push_lit(&mut self, val: i32) {
    self.push(Item::new(Value::Lit(val)));
  }
  pub fn push_flit(&mut self, val: f32) {
    self.push(Item::new(Value::LitFloat(val)));
  }
  pub fn push_field(&mut self, val: &str) {
    self.push(Item::new(Value::Field(Box::new(Item::new(Value::this())), val.into())));
  }
  #[track_caller]
  pub fn pop(&mut self) -> Item {
    self.items.pop().expect("stack is empty")
  }
}

impl State {
  pub fn new() -> Self {
    State { stack: Stack::new(), statements: vec![], instr: vec![] }
  }
  pub fn stack(&mut self) -> &mut Stack {
    if let Some(stat) = self.statements.last_mut() {
      stat.stack()
    } else {
      &mut self.stack
    }
  }
  pub fn stack_ref(&self) -> &Stack {
    if let Some(stat) = self.statements.last() {
      stat.stack_ref()
    } else {
      &self.stack
    }
  }
  /// Adds the given conditional. This will be negated internally, so the
  /// conditional passed in should match the assembly instruction.
  ///
  /// `target` is the jump target, and `curr` is the current instruction.
  pub fn add_if(&mut self, mut c: Cond, target: u32, curr: u32) {
    if let Some(Statement::Cond { jmp, cond, when_true, when_false, .. }) =
      self.statements.last_mut()
    {
      // Current is where the current conditional is. curr + 3 is where the next
      // instruction is. So, if we have assembly like so:
      //
      // 22 IfEq 11               -- This is on the stack right now
      // 25 ALoad0
      // 26 GetField something
      // 29 IConst5
      // 30 IfICmpNe 26           -- We are calling add_if for this statement
      // 33 < if body >
      // ..
      // 56 Return
      //
      // So, adding 3 to the current statement will get our previous jump location.
      // This means we have an or operation. We also need to negate the previous
      // instruction again, so we get eq (not neq).
      if *jmp == curr + 3 {
        *jmp = target;
        // If its already an or, then we've already inverted it.
        if !matches!(cond, Cond::Or(_, _)) {
          cond.invert();
        }
        c.invert();
        cond.or(c);
        return;
      }
      // We could also have multiple or statements, in which case we would have
      // two If instructions pointing to the same place.
      //
      // In an ideal world, everything would be simple, and we could just check jmp ==
      // target. However, in packet S3E, we have a foor loop right after a chain of or
      // statements. So, we need to check if any extra statements are between this if
      // and the previous one. If there are, then this isn't as simple as an or
      // statement, and it will end up being a nested if or a for loop.
      if *jmp == target && when_true.is_empty() && when_false.is_empty() {
        cond.invert();
        c.invert();
        cond.or(c);
        return;
      }
    }
    c.invert();
    self.statements.push(Statement::Cond {
      cond:        c,
      jmp:         target,
      when_true:   vec![],
      when_false:  vec![],
      in_else:     false,
      stack_true:  self.stack_ref().clone(),
      stack_false: self.stack_ref().clone(),
    });
  }
  pub fn switch(&mut self, val: Item, table: Vec<(i32, i32)>) {
    self.statements.push(Statement::Switch {
      val,
      table: table.into_iter().map(|(key, offset)| (key, offset, vec![])).collect(),
      def: None,
      curr: 0,
      stack: Stack::new(),
      end: None,
    });
  }
  pub fn check_if(&mut self, _vars: &mut Vars, idx: IndexAttrib) {
    while let Some(last) = self.statements.last_mut() {
      match last {
        Statement::Cond { jmp, .. } => {
          if idx.as_u32() == *jmp {
            if let Statement::Cond {
              cond,
              mut stack_true,
              mut stack_false,
              when_true,
              when_false,
              ..
            } = self.statements.pop().unwrap()
            {
              stack_true.items.drain(..self.stack.items.len());
              stack_false.items.drain(..self.stack.items.len());
              for (item, other) in stack_true.items.into_iter().zip(stack_false.items) {
                if item != other {
                  item.ops.borrow_mut().push(Op::If(cond.clone(), other));
                }
                self.stack().push(item);
              }
              if !when_true.is_empty() || !when_false.is_empty() {
                self.instr(Instr::If(cond, when_true, when_false));
              }
            } else {
              unreachable!()
            }
            continue;
          } else {
            // Don't want integer overflow
            if self.statements.len() >= 2 {
              let i = self.statements.len() - 2;
              if let Statement::Switch { table, stack, curr, .. } = &mut self.statements[i] {
                if table.iter().any(|(_, offset, _)| *offset as u32 == idx.as_u32()) {
                  stack.items.drain(self.stack.items.len()..);
                  let idx = *curr;
                  if let Statement::Cond { cond, when_true, when_false, .. } =
                    self.statements.pop().unwrap()
                  {
                    if !when_true.is_empty() || !when_false.is_empty() {
                      if let Statement::Switch { table, .. } = &mut self.statements[i] {
                        table[idx].2.push(Instr::If(cond, when_true, when_false));
                      } else {
                        unreachable!();
                      }
                    }
                  } else {
                    panic!("we don't support nested switches yet")
                  }
                  continue;
                }
              }
            }
          }
        }
        Statement::Switch { end, .. } => {
          if let Some(end) = end {
            if *end == idx.as_u32() {
              if let Statement::Switch { val, table, def, .. } = self.statements.pop().unwrap() {
                self.instr(Instr::Switch(
                  val,
                  table.into_iter().map(|(key, _, instr)| (key, instr)).collect(),
                  def,
                ));
              } else {
                unreachable!()
              }
              continue;
            }
          }
        }
      }
      break;
    }
  }
  /// Index is the index we are going to, and offset is the offset of this new
  /// index. Offset is used to determine if we are jumping forwards/backwards.
  pub fn goto(&mut self, index: u32, offset: i32) {
    match self.statements.last_mut().unwrap() {
      Statement::Cond { jmp, in_else, .. } => {
        if offset > 0 {
          // This is an if statement
          *jmp = index;
          *in_else = true;
        } else {
          // This is a loop
          let last = self.statements.pop().unwrap();
          if let Statement::Cond { cond, when_true, .. } = last {
            self.instr(Instr::Loop(cond, when_true));
          } else {
            unreachable!()
          }
        }
      }
      Statement::Switch { stack, end, curr, table, def, .. } => {
        stack.items.drain(..self.stack.items.len());
        *curr += 1;
        if *curr == table.len() {
          *def = Some(vec![]);
        }
        if let Some(e) = end {
          assert_eq!(*e, index);
        } else {
          *end = Some(index);
        }
      }
    }
  }

  pub fn instr(&mut self, instr: Instr) {
    if let Some(last) = self.statements.last_mut() {
      last.instr(instr);
    } else {
      self.instr.push(instr);
    }
  }
}

impl Cond {
  #[track_caller]
  pub fn invert(&mut self) {
    let old = mem::replace(self, Cond::EQ(Item::new(Value::Lit(0)), Item::new(Value::Lit(0))));
    match old {
      Cond::LT(lhs, rhs) => *self = Cond::GE(lhs, rhs),
      Cond::GT(lhs, rhs) => *self = Cond::LE(lhs, rhs),
      Cond::LE(lhs, rhs) => *self = Cond::GT(lhs, rhs),
      Cond::GE(lhs, rhs) => *self = Cond::LT(lhs, rhs),
      Cond::NE(lhs, rhs) => *self = Cond::EQ(lhs, rhs),
      Cond::EQ(lhs, rhs) => *self = Cond::NE(lhs, rhs),
      _ => {} // _ => panic!("cannot invert {:?}", old),
    }
  }

  pub fn or(&mut self, other: Cond) {
    let old = mem::replace(self, Cond::EQ(Item::new(Value::Lit(0)), Item::new(Value::Lit(0))));
    *self = Cond::Or(Box::new(old), Box::new(other));
  }
}

impl Item {
  pub fn new(initial: impl Into<Value>) -> Self {
    Item { initial: initial.into(), ops: Rc::new(RefCell::new(vec![])) }
  }

  pub fn deep_clone(&self) -> Self {
    Item {
      initial: self.initial.clone(),
      ops:     Rc::new(RefCell::new(self.ops.borrow().clone())),
    }
  }
}

impl Value {
  /// A variable reference to `this`.
  pub fn this() -> Self {
    Value::Var(0)
  }
}

impl fmt::Debug for Item {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    if self.ops.borrow().is_empty() {
      write!(f, "Item(")?;
      self.initial.fmt(f)?;
      write!(f, ")")
    } else {
      f.debug_tuple("Item").field(&self.initial).field(&self.ops.borrow()).finish()
    }
  }
}

impl From<Value> for Item {
  fn from(v: Value) -> Self {
    Item::new(v)
  }
}
