use super::Converter;
use serde::Serialize;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize)]
pub enum Type {
  /// Only present for return types
  Void,

  Byte,
  Char,
  Double,
  Float,
  Int,
  Long,
  Short,
  Bool,
  Class(String),
  Array(Box<Type>),
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Descriptor {
  pub args: Vec<Type>,
  pub ret:  Type,
}

impl Type {
  pub fn from_desc(d: &str) -> (Self, usize) {
    match d.chars().next().unwrap() {
      'V' => (Type::Void, 1),
      'B' => (Type::Byte, 1),
      'C' => (Type::Char, 1),
      'D' => (Type::Double, 1),
      'F' => (Type::Float, 1),
      'I' => (Type::Int, 1),
      'J' => (Type::Long, 1),
      'S' => (Type::Short, 1),
      'Z' => (Type::Bool, 1),
      'L' => {
        let class = d[1..].split(';').next().unwrap();
        let len = class.len() + 2; // 1 for the 'L', and 1 for the ';'
        (Type::Class(class.into()), len)
      }
      '[' => {
        let (inner, len) = Type::from_desc(&d[1..]);
        (Type::Array(Box::new(inner)), len + 1)
      }
      _ => panic!("invalid type: {}", d),
    }
  }

  pub fn to_desc(&self, s: &mut String) {
    s.push(match self {
      Type::Void => 'V',
      Type::Byte => 'B',
      Type::Char => 'C',
      Type::Double => 'D',
      Type::Float => 'F',
      Type::Int => 'I',
      Type::Long => 'J',
      Type::Short => 'S',
      Type::Bool => 'Z',
      Type::Class(name) => {
        s.push('L');
        s.push_str(name);
        s.push(';');
        return;
      }
      Type::Array(ty) => {
        s.push('[');
        ty.to_desc(s);
        return;
      }
    });
  }

  pub fn deobf<C: Converter>(&mut self, conv: &mut C) {
    match self {
      Type::Class(name) => {
        *name = conv.class(name);
      }
      Type::Array(ty) => {
        ty.deobf(conv);
      }
      _ => {}
    }
  }
}

impl Descriptor {
  pub fn from_desc(a: &str) -> Self {
    let desc = a.split('(').nth(1).unwrap().split(')').next().unwrap();
    let ret = Type::from_desc(a.split(')').nth(1).unwrap()).0;
    let mut args = vec![];
    let mut i = 0;
    while i < desc.len() {
      let (ty, len) = Type::from_desc(&desc[i..]);
      i += len;
      args.push(ty);
    }
    Descriptor { args, ret }
  }

  pub fn to_desc(&self) -> String {
    let mut out = String::new();
    out += "(";
    for a in &self.args {
      a.to_desc(&mut out);
    }
    out += ")";
    self.ret.to_desc(&mut out);
    out
  }

  pub fn deobf<C: Converter>(mut self, conv: &mut C) -> Self {
    for a in &mut self.args {
      a.deobf(conv);
    }
    self.ret.deobf(conv);
    self
  }
}
