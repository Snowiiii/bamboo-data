use std::{
  fs,
  fs::File,
  io,
  io::{Read, Write},
  path::{Path, PathBuf},
  thread,
  time::Duration,
};

pub mod progress;

pub fn json<T: serde::de::DeserializeOwned>(url: &str) -> io::Result<T> {
  let mut delay = Duration::from_millis(100);
  for _ in 0..10 {
    match ureq::get(url).call() {
      Ok(v) => {
        return v.into_json().map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e.to_string()))
      }
      Err(_) => {}
    }
    thread::sleep(delay);
    delay *= 2;
    if delay.as_secs() > 30 {
      delay = Duration::from_secs(30);
    }
  }
  Err(io::Error::new(io::ErrorKind::ConnectionAborted, "failed to download after 10 tries"))
}

/// Unzips the given file into the given directory.
pub fn unzip(filename: &Path, dir: &Path) -> io::Result<()> {
  info!("extracting to {}", dir.display());

  let file = File::open(filename)?;
  let mut archive = zip::ZipArchive::new(file).unwrap();

  for i in 0..archive.len() {
    let mut file = archive.by_index(i).unwrap();
    let out = match file.enclosed_name() {
      Some(path) => dir.join(path.to_owned()),
      None => continue,
    };

    {
      let comment = file.comment();
      if !comment.is_empty() {
        info!("File {} comment: {}", i, comment);
      }
    }

    if (&*file.name()).ends_with('/') {
      fs::create_dir_all(&out).unwrap();
    } else {
      if let Some(p) = out.parent() {
        if !p.exists() {
          fs::create_dir_all(&p).unwrap();
        }
      }
      let mut outfile = fs::File::create(&out).unwrap();
      io::copy(&mut file, &mut outfile).unwrap();
    }

    // Get and Set permissions
    #[cfg(unix)]
    {
      use std::os::unix::fs::PermissionsExt;

      if let Some(mode) = file.unix_mode() {
        fs::set_permissions(&out, fs::Permissions::from_mode(mode)).unwrap();
      }
    }
  }

  info!("done extracting to {}", dir.display());

  Ok(())
}

/// Returns `true` if the file was changed on disk. If there is already a file
/// on disk, and the size matches either the parameter `size`, or the metadata
/// from the remove host, then the download will be skipped.
pub fn download_file(url: &str, path: &Path, size: Option<u64>) -> io::Result<bool> {
  // Make sure the containing directory exists.
  if !path.parent().unwrap().exists() {
    return Err(io::Error::new(
      io::ErrorKind::NotFound,
      format!("the directory {} does not exist", path.parent().unwrap().display()),
    ));
  }
  // If we know the size beforehand, avoid making a request at all.
  if let Some(size) = size {
    if path.exists() {
      let file = File::open(path)?;
      if file.metadata()?.len() == size {
        return Ok(false);
      }
    }
  }

  let res =
    ureq::get(url).call().map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e.to_string()))?;
  let total = res.header("Content-Length").and_then(|s| s.parse::<u64>().ok()).unwrap_or(0);
  let mut res = res.into_reader();
  if path.exists() {
    let file = File::open(path)?;
    if file.metadata()?.len() != total {
      info!(
        "file size differs from server and disk, redownloading {} (url: {})",
        path.display(),
        url
      );
    } else {
      info!("file size is the same on server and disk, skipping {} (url: {})", path.display(), url);
      return Ok(false);
    }
  }

  let mut bar = progress::PROGRESS.add(path.display().to_string(), total);
  let mut file = File::create(path)?;

  // We don't want half a megabyte on the stack
  let chunk: &mut [u8] = &mut vec![0u8; 512 * 1024];
  let mut written = 0;
  loop {
    let len = res.read(chunk).unwrap();
    if len == 0 {
      break;
    }
    written += len;
    bar.set(written as u64);

    file.write_all(&chunk[..len])?;
  }
  Ok(true)
}

pub struct GitRepo {
  path: PathBuf,
}

pub fn clone_repo(url: &str, path: &Path, branch: Option<&str>) -> io::Result<GitRepo> {
  let mut repo = GitRepo { path: path.into() };
  if path.join(".git").exists() {
    repo.run(&["pull", "--ff-only"])?;
  } else {
    let old = std::mem::replace(&mut repo.path, path.parent().unwrap().into());
    repo.run(&["clone", url])?;
    repo.path = old;
  };
  if let Some(name) = branch {
    repo.checkout(name)?;
  }
  Ok(repo)
}

use std::process::Command;
impl GitRepo {
  fn run(&self, args: &[&str]) -> io::Result<()> {
    let output = Command::new("git")
      .args(args)
      .current_dir(&self.path)
      .output()
      .expect("failed to execute git");
    info!(
      "`git {:?}` stdout (in {}):\n{}",
      args,
      self.path.display(),
      String::from_utf8_lossy(&output.stdout).trim()
    );
    if !output.status.success() {
      info!(
        "`git {:?}` stderr (in {}):\n{}",
        args,
        self.path.display(),
        String::from_utf8_lossy(&output.stderr).trim()
      );
      panic!("git failed");
    }
    Ok(())
  }
  pub fn path(&self) -> &Path {
    &self.path
  }
  pub fn checkout(&self, branch: &str) -> io::Result<()> {
    self.run(&["checkout", branch])
  }
}

pub fn copy_dir(from: &Path, to: &Path) -> io::Result<()> {
  let output =
    Command::new("cp").arg("-r").arg(from).arg(to).output().expect("failed to execute cp");
  if !output.status.success() {
    info!(
      "`cp {} {}` stderr:\n{}",
      from.display(),
      to.display(),
      String::from_utf8_lossy(&output.stderr).trim()
    );
    panic!("cp failed");
  }
  Ok(())
}
