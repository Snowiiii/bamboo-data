use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use parking_lot::Mutex;
use std::io;

pub static PROGRESS: Progress = Progress::new();

pub struct ProgressOutput;

impl From<ProgressOutput> for fern::Output {
  fn from(v: ProgressOutput) -> Self {
    fern::Output::writer(Box::new(v), "\n")
  }
}

impl io::Write for ProgressOutput {
  fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
    let lock = PROGRESS.inner.lock();
    match &lock.multi {
      Some(p) => p.suspend(|| std::io::stdout().write(buf)),
      None => std::io::stdout().write(buf),
    }
  }

  fn flush(&mut self) -> io::Result<()> {
    let lock = PROGRESS.inner.lock();
    match &lock.multi {
      Some(p) => p.suspend(|| std::io::stdout().flush()),
      None => std::io::stdout().flush(),
    }
  }
}

pub struct Progress {
  inner: Mutex<ProgressInner>,
}

struct ProgressInner {
  multi: Option<MultiProgress>,
  names: Vec<String>,
}

pub struct Bar {
  inner: ProgressBar,
  name:  String,
}

impl Bar {
  fn new(name: String, pb: ProgressBar) -> Self {
    let style = ProgressStyle::with_template(
      "[{eta_precise}] {bar:40.cyan/blue} {bytes:>11} / {total_bytes:>10} {msg}",
    )
    .unwrap()
    .progress_chars("##-");
    pb.set_message(name.clone());
    pb.set_style(style);
    Bar { name, inner: pb }
  }
  pub fn set(&mut self, value: u64) {
    self.inner.set_position(value);
  }
}

impl Drop for Bar {
  fn drop(&mut self) {
    self.inner.finish_and_clear();
    PROGRESS.inner.lock().remove(&self.name);
  }
}

impl Progress {
  const fn new() -> Self {
    Progress { inner: Mutex::new(ProgressInner { multi: None, names: vec![] }) }
  }

  pub fn add(&self, name: String, max: u64) -> Bar {
    let pb = {
      let mut inner = self.inner.lock();
      inner.add(name.clone(), ProgressBar::new(max))
    };
    Bar::new(name, pb)
  }
}

impl ProgressInner {
  fn check_init(&mut self) {
    if self.multi.is_none() {
      self.multi = Some(MultiProgress::new());
    }
  }

  fn add(&mut self, name: String, pb: ProgressBar) -> ProgressBar {
    self.check_init();
    let idx = self.names.binary_search(&name).unwrap_err();
    self.names.insert(idx, name.clone());
    self.multi.as_ref().unwrap().insert(idx, pb)
  }

  fn remove(&mut self, name: &str) {
    let idx = self.names.iter().position(|v| v == name).unwrap();
    self.names.remove(idx);
  }
}
