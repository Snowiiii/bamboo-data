use super::Version;
use crate::{download, download::GitRepo};
use std::{fs, io, path::PathBuf};

pub fn mcp(url: &str, path: &str) -> io::Result<()> {
  let p: PathBuf = path.into();
  info!("downloading mcp to {}", path);

  fs::create_dir_all(&p)?;
  if download::download_file(url, &p.join("mcp.zip"), None)? {
    download::unzip(&p.join("mcp.zip"), &p.join("mcp"))?;
  }

  if path.ends_with("12-02") {
    let changed = download::download_file(
      "http://files.minecraftforge.net/maven/de/oceanlabs/mcp/mcp/1.12.2/mcp-1.12.2-srg.zip",
      &p.join("srg.zip"),
      None,
    )?;
    if changed {
      download::unzip(&p.join("srg.zip"), &p.join("mcp/conf"))?;
    }
  }

  Ok(())
}

pub fn intermediary(path: &str) -> io::Result<()> {
  let p: PathBuf = path.into();
  info!("git cloning intermediary mappings to {}", path);

  fs::create_dir_all(&p)?;
  download::clone_repo("https://github.com/FabricMC/intermediary.git", &p, None)?;

  Ok(())
}

pub fn yarn(path: &str) -> io::Result<GitRepo> {
  let p: PathBuf = path.into();
  info!("git cloning yarn mappings to {}", path);

  fs::create_dir_all(&p)?;
  download::clone_repo("https://github.com/FabricMC/yarn.git", &p, None)
}

pub fn copy_yarn(intermediary: &str, yarn: &GitRepo, path: &str, ver: &Version) -> io::Result<()> {
  let p: PathBuf = path.into();
  let intermediary: PathBuf = intermediary.into();

  info!("checking out branch {} in yarn mappings", ver);
  yarn.checkout(&ver.to_string())?;

  info!("copying yarn mappings to {}", path);
  fs::create_dir_all(&p)?;
  download::copy_dir(&yarn.path().join("mappings"), &p.join("yarn")).unwrap();

  let inter_name = format!("{ver}.tiny");
  let target_inter_path = p.join("intermediary.tiny");
  info!("copying intermediary mapping {} to {}", inter_name, target_inter_path.display());
  fs::copy(intermediary.join("mappings").join(inter_name), target_inter_path).unwrap();

  Ok(())
}
