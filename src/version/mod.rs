use std::fmt;

/// A version.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Version<'a> {
  /// 1.19 or 1.19.1
  Release(ReleaseVersion),
  /// `1.19-pre1` or `1.19-rc2`
  PreRelease(ReleaseVersion, &'a str),
  /// 22w24a
  Snapshot(SnapshotVersion),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct ReleaseVersion {
  maj: u32,
  min: u32,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SnapshotVersion {
  /// Used for `maj()` and `min()`. It is the release version this snapshot is
  /// for.
  release: ReleaseVersion,
  year:    u32,
  week:    u32,
  sub:     char,
}

macro_rules! ver {
  ( 1 - $maj:literal - $min:literal ) => {
    Version::new_release($maj, $min)
  };
  ( 1 - $maj:literal - $min:literal - $pre:tt ) => {
    Version::new_pre_release($maj, $min, stringify!($pre))
  };
  ( $year:literal - w - $week:literal - $sub:tt $($extra:tt)* ) => {
    Version::new_snap($year, $week, stringify!($sub).as_bytes()[0] as char)
  };
}
macro_rules! release_for_if {
  (
    $year:expr, $week:expr, $sub:expr,
    1 - $maj:literal - $min:literal $(- $pre:tt)?
  ) => {};
  (
    $year:expr, $week:expr, $sub:expr,
    $year_match:literal - w - $week_match:literal - $sub_match:tt
    ->
    1 - $maj:literal - $min:literal
  ) => {
    if $year == $year_match
      && $week == $week_match
      && $sub == stringify!($sub_match).as_bytes()[0] as char
    {
      return Some(ReleaseVersion::new($maj, $min));
    }
  };
}
macro_rules! release_for {
  ( $year:expr, $week:expr, $sub:expr,
    [ $( ($($arg:tt)*), )* ]
  ) => {{
    $(
      release_for_if!($year, $week, $sub, $($arg)*);
    )*
    None
  }}
}
macro_rules! versions {
  [ $( ($($arg:tt)*), )* ] => {
    pub static VERSIONS: &[Version] = &[
      $( ver!($($arg)*), )*
    ];

    impl ReleaseVersion {
      pub const fn for_snap(year: u32, week: u32, sub: char) -> Option<ReleaseVersion> {
        release_for!(year, week, sub, [ $( ($($arg)*), )* ])
      }
    }
  }
}

versions![
  (1-8-9),
  (1-9-4),
  (1-10-2),
  (1-11-2),
  (1-12-2),
  (1-14-4),
  (1-15-2),
  (1-16-5),
  (1-17-1),
  (1-18-0),
  (1-18-2),

  (22-w-11-a -> 1-19-0),
  (22-w-12-a -> 1-19-0),
  (22-w-13-a -> 1-19-0),
  (22-w-14-a -> 1-19-0),
  (22-w-15-a -> 1-19-0),
  (22-w-16-a -> 1-19-0),
  (22-w-16-b -> 1-19-0),
  (22-w-17-a -> 1-19-0),
  (22-w-18-a -> 1-19-0),
  (22-w-19-a -> 1-19-0),
  (1-19-0-pre1),
  (1-19-0-pre2),
  (1-19-0-pre3),
  (1-19-0-pre4),
  (1-19-0-pre5),
  (1-19-0-rc1),
  (1-19-0-rc2),
  (1-19-0),

  (22-w-24-a -> 1-19-1),
  (1-19-1-pre1),
  (1-19-1-rc1),
  (1-19-1-pre2),
  (1-19-1-pre3),
  (1-19-1-pre4),
  (1-19-1-pre5),
  (1-19-1-pre6),
  (1-19-1-rc2),
  (1-19-1-rc3),
  (1-19-1),
  (1-19-2-rc1),
  (1-19-2-rc2),
  (1-19-2),

  (22-w-42-a -> 1-19-3),
  (22-w-43-a -> 1-19-3),
  (22-w-44-a -> 1-19-3),
  (22-w-45-a -> 1-19-3),
  (22-w-46-a -> 1-19-3),
  (1-19-3-pre1),
  (1-19-3-pre2),
  (1-19-3-pre3),
  (1-19-3-rc1),
  (1-19-3-rc2),
  (1-19-3-rc3),
  (1-19-3),

  (23-w-03-a -> 1-19-4),
  (23-w-04-a -> 1-19-4),
  (23-w-05-a -> 1-19-4),
  (23-w-06-a -> 1-19-4),
  (23-w-07-a -> 1-19-4),
  (1-19-4-pre1),
  (1-19-4-pre2),
  (1-19-4-pre3),
  (1-19-4-pre4),
  (1-19-4-rc1),
  (1-19-4-rc2),
  (1-19-4-rc3),
  (1-19-4),

  (1-20-0),
  (1-20-1),
  (1-20-2),

];

impl ReleaseVersion {
  pub const fn next() -> Self {
    ReleaseVersion::new(19, 4)
  }
  pub const fn from_num(num: u32) -> Option<Self> {
    Some(match num {
      8 => ReleaseVersion::new(8, 9),
      9 => ReleaseVersion::new(9, 4),
      10 => ReleaseVersion::new(10, 2),
      11 => ReleaseVersion::new(11, 2),
      12 => ReleaseVersion::new(12, 2),
      14 => ReleaseVersion::new(14, 4),
      15 => ReleaseVersion::new(15, 2),
      16 => ReleaseVersion::new(16, 5),
      17 => ReleaseVersion::new(17, 1),
      18 => ReleaseVersion::new(18, 2),
      19 => ReleaseVersion::new(19, 4),
      20 => ReleaseVersion::new(20, 2),
      _ => return None,
    })
  }

  pub const fn new(maj: u32, min: u32) -> Self {
    ReleaseVersion { maj, min }
  }
}

#[derive(Debug, Clone)]
pub struct VersionErr(String);

impl fmt::Display for VersionErr {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "invalid version: {}", self.0)
  }
}

impl std::error::Error for VersionErr {}

impl<'a> Version<'a> {
  pub fn from_str(s: &'a str) -> Result<Self, VersionErr> {
    Self::from_str_opt(s).ok_or_else(|| VersionErr(s.into()))
  }
  fn from_str_opt(s: &'a str) -> Option<Self> {
    if let Ok(num) = s.parse::<u32>() {
      Some(Version::Release(ReleaseVersion::from_num(num)?))
    } else if let Some(maj_min) = s.strip_prefix("1.") {
      let mut pre_sections = maj_min.split('-');
      let maj_min = pre_sections.next()?;
      let mut sections = maj_min.split('.');
      let release = ReleaseVersion::new(
        sections.next()?.parse().ok()?,
        sections.next().map(|v| v.parse()).unwrap_or(Ok(0)).ok()?,
      );
      Some(if let Some(pre) = pre_sections.next() {
        Version::PreRelease(release, pre)
      } else {
        Version::Release(release)
      })
    } else {
      let mut sections = s.split('w');
      let year = sections.next()?.parse().ok()?;
      let week_sub = sections.next()?;
      let sub = week_sub.chars().last()?;
      let week = week_sub[..week_sub.len() - 1].parse().ok()?;
      Version::new_snap_opt(year, week, sub)
    }
  }
  pub const fn next_release() -> Self {
    Version::Release(ReleaseVersion::next())
  }
  pub const fn new_release(maj: u32, min: u32) -> Self {
    Version::Release(ReleaseVersion { maj, min })
  }
  pub const fn new_pre_release(maj: u32, min: u32, pre: &'a str) -> Self {
    Version::PreRelease(ReleaseVersion { maj, min }, pre)
  }
  #[track_caller]
  pub const fn new_snap(year: u32, week: u32, sub: char) -> Self {
    match ReleaseVersion::for_snap(year, week, sub) {
      Some(release) => Version::Snapshot(SnapshotVersion { release, year, week, sub }),
      None => panic!("unknown snapshot"),
    }
  }
  pub const fn new_snap_opt(year: u32, week: u32, sub: char) -> Option<Self> {
    match ReleaseVersion::for_snap(year, week, sub) {
      Some(release) => Some(Version::Snapshot(SnapshotVersion { release, year, week, sub })),
      None => None,
    }
  }

  pub fn maj(&self) -> u32 {
    match self {
      Version::Release(v) => v.maj,
      Version::PreRelease(v, _) => v.maj,
      Version::Snapshot(v) => v.release.maj,
    }
  }
  pub fn minor(&self) -> u32 {
    match self {
      Version::Release(v) => v.min,
      Version::PreRelease(v, _) => v.min,
      Version::Snapshot(v) => v.release.min,
    }
  }
  pub fn is_old(&self) -> bool {
    self.maj() < 13
  }

  pub fn is_release(&self) -> bool {
    matches!(self, Version::Release(_))
  }
  pub fn is_snapshot(&self) -> bool {
    matches!(self, Version::Snapshot(_) | Version::PreRelease(..))
  }

  pub fn build_dir(&self) -> String {
    match self {
      Version::Release(v) => format!("mc-release-{:02}-{:02}", v.maj, v.min),
      Version::PreRelease(v, pre) => format!("mc-pre-release-{:02}-{:02}-{}", v.maj, v.min, pre),
      Version::Snapshot(v) => format!("mc-snapshot-{}", v),
    }
  }

  pub fn release(&self) -> ReleaseVersion {
    match self {
      Version::Release(v) => *v,
      Version::PreRelease(v, _) => *v,
      Version::Snapshot(v) => v.release,
    }
  }
}

use std::cmp::Ordering;
impl PartialOrd for Version<'_> {
  fn partial_cmp(&self, other: &Version) -> Option<Ordering> {
    Some(self.release().cmp(&other.release()))
  }
}
impl Ord for Version<'_> {
  fn cmp(&self, other: &Version) -> Ordering {
    self.release().cmp(&other.release())
  }
}

impl fmt::Display for Version<'_> {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      Version::Release(v) => v.fmt(f),
      Version::PreRelease(v, pre) => write!(f, "{v}-{pre}"),
      Version::Snapshot(v) => v.fmt(f),
    }
  }
}

impl fmt::Display for ReleaseVersion {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    if self.min != 0 {
      write!(f, "1.{}.{}", self.maj, self.min)
    } else {
      write!(f, "1.{}", self.maj)
    }
  }
}

impl fmt::Display for SnapshotVersion {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}w{:02}{}", self.year, self.week, self.sub)
  }
}
