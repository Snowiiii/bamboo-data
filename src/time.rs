use std::{
  fmt,
  time::{Duration, Instant},
};

pub struct PrettyDuration(Duration);

pub fn time(end: Instant) -> PrettyDuration {
  PrettyDuration(Instant::now().duration_since(end))
}

impl fmt::Display for PrettyDuration {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    if self.0 >= Duration::from_millis(10) {
      write!(f, "{:0.2}s", self.0.as_millis() as f64 / 1000.0)
    } else if self.0 >= Duration::from_micros(10) {
      write!(f, "{:0.2}ms", self.0.as_micros() as f64 / 1000.0)
    } else if self.0 >= Duration::from_nanos(10) {
      write!(f, "{:0.2}us", self.0.as_nanos() as f64 / 1000.0)
    } else {
      write!(f, "{:0.2}ns", self.0.as_nanos())
    }
  }
}

#[test]
fn format_times() {
  fn fmt(dur: Duration) -> String {
    PrettyDuration(dur).to_string()
  }

  assert_eq!("1.00s", fmt(Duration::from_millis(1000)));
  assert_eq!("0.50s", fmt(Duration::from_millis(500)));
  assert_eq!("0.01s", fmt(Duration::from_millis(10)));
  assert_eq!("9.00ms", fmt(Duration::from_millis(9)));
  assert_eq!("9.01ms", fmt(Duration::from_millis(9) + Duration::from_micros(10)));
  assert_eq!("1.00ms", fmt(Duration::from_millis(1)));
}
