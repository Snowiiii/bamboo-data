use super::{
  super::{block::BlockDef, Jar},
  Item, ItemDef,
};
use crate::decomp::{Instr, Value};
use anyhow::Result;
use std::collections::HashMap;

pub fn process(jar: &mut Jar, blocks: &BlockDef) -> Result<ItemDef> {
  let class = jar.decomp("net/minecraft/item/Item", |_, name, _| name == "registerItems")?;

  let (_, _, block) = class.funcs.first().unwrap();
  let items =
    generate(&block.instr, blocks, jar).map_err(|e| e.context("error while parsing blocks"))?;

  Ok(ItemDef { items })
}

fn generate(instr: &[Instr], blocks: &BlockDef, jar: &mut Jar) -> Result<Vec<Item>> {
  let block_names = find_block_names(jar)?;
  let mut items = vec![];
  for i in instr {
    match i.clone() {
      Instr::Expr(it) => match it.initial {
        Value::StaticCall(_, name, args) => match name.as_str() {
          "registerItemBlock" => {
            assert!(
              args.len() == 1 || args.len() == 2,
              "registerItemBlock takes 1 or 2 args, not {:?}",
              args
            );
            let (class, _, name) = args[0].clone().unwrap_initial().unwrap_static_field();
            assert_eq!(class, "net/minecraft/init/Blocks");
            let name = &block_names[&name];
            let block = blocks.get(&name).unwrap_or_else(|| {
              panic!(
                "could not find block {} in def: {:?}",
                name,
                blocks.blocks.iter().map(|b| b.name.as_str()).collect::<Vec<_>>()
              )
            });
            items.push(Item {
              id: block.id,
              name: block.name.clone(),
              class: "net/minecraft/item/ItemBlock".into(),
              ..Default::default()
            });
          }
          "registerItem" => {
            assert!(args.len() == 3, "registerItem takes 3 args, not {:?}", args);
            let id = args[0].clone().unwrap_initial().unwrap_int();
            let name = args[1].clone().unwrap_initial().unwrap_string();
            // TODO: Read class passed as args[2]
            items.push(Item {
              id: id as u32,
              name,
              class: "net/minecraft/item/Item".into(),
              ..Default::default()
            });
          }
          _ => todo!("unknown item register name: {:?}", name),
        },
        Value::Class(_) => {}
        v => todo!("unknown item {:?}", v),
      },
      _ => {}
    }
  }
  if items[0].name != "air" {
    items.insert(
      0,
      Item {
        id: 0,
        name: "air".into(),
        class: "net/minecraft/item/ItemBlock".into(),
        ..Default::default()
      },
    );
  }
  Ok(items)
}

/// Returns a map of `Blocks.field_name` to block names
fn find_block_names(jar: &mut Jar) -> Result<HashMap<String, String>> {
  let class = jar.decomp("net/minecraft/init/Blocks", |_, name, _| name == "<clinit>")?;

  let mut out = HashMap::new();

  let (_, _, block) = class.funcs.first().unwrap();
  for i in &block.instr {
    match i {
      Instr::SetField(field_name, it) => {
        let (_, name, args) = it.clone().unwrap_initial().unwrap_static_call();
        if name != "getRegisteredBlock" {
          continue;
        }
        out.insert(field_name.clone(), args[0].clone().unwrap_initial().unwrap_string());
      }
      _ => {}
    }
  }

  Ok(out)
}
