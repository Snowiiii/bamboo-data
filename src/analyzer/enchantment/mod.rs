use super::Jar;
use anyhow::Result;
use serde::Serialize;

mod new;
mod v8;
mod v9;

#[derive(Debug, Clone, Serialize)]
pub struct Enchantment {
  name: String,
  id:   u32,
}

#[derive(Debug, Clone, Serialize)]
pub struct EnchantmentDef {
  enchantments: Vec<Enchantment>,
}

pub fn generate(jar: &mut Jar) -> Result<impl Serialize> {
  if jar.ver().maj() == 8 {
    v8::process(jar)
  } else if jar.ver().maj() <= 12 {
    v9::process(jar)
  } else {
    new::process(jar)
  }
}
