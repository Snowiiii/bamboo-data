//! Handles 1.14+

use super::{super::Jar, Enchantment, EnchantmentDef};
use crate::decomp::{Instr, Value};
use anyhow::Result;

pub fn process(jar: &mut Jar) -> Result<EnchantmentDef> {
  let class =
    jar.decomp("net/minecraft/enchantment/Enchantments", |_, name, _| name == "<clinit>")?;

  let (_, _, block) = class.funcs.first().unwrap();
  let enchantments =
    generate(&block.instr, jar).map_err(|e| e.context("error while parsing enchantment"))?;

  Ok(EnchantmentDef { enchantments })
}

fn generate(instr: &[Instr], _jar: &mut Jar) -> Result<Vec<Enchantment>> {
  let mut id = 0;
  let mut enchantments = vec![];
  for i in instr {
    match i.clone() {
      Instr::SetField(_field_name, it) => {
        if !it.ops.borrow().is_empty() {
          continue;
        }
        match it.unwrap_initial() {
          Value::StaticCall(_, name, args) => match name.as_str() {
            "a" | "register" => {
              let name = args[0].clone().unwrap_initial().unwrap_string();
              enchantments.push(Enchantment { name, id });
              id += 1;
            }
            _ => todo!("unknown particle register name: {:?}", name),
          },
          v => todo!("unknown particle {:?}", v),
        }
      }
      _ => {}
    }
  }
  Ok(enchantments)
}
