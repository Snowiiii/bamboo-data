//! Handles 1.8

use super::{super::Jar, Enchantment, EnchantmentDef};
use crate::decomp::{Instr, Op, Value};
use anyhow::Result;

pub fn process(jar: &mut Jar) -> Result<EnchantmentDef> {
  let class =
    jar.decomp("net/minecraft/enchantment/Enchantment", |_, name, _| name == "<clinit>")?;

  let (_, _, block) = class.funcs.first().unwrap();
  let enchantments =
    generate(&block.instr, jar).map_err(|e| e.context("error while parsing enchantment"))?;

  Ok(EnchantmentDef { enchantments })
}

fn generate(instr: &[Instr], _jar: &mut Jar) -> Result<Vec<Enchantment>> {
  let mut enchantments = vec![];
  for i in instr {
    match i.clone() {
      Instr::SetField(_name, it) => {
        if !matches!(it.initial, Value::Class(_)) {
          continue;
        }
        match &it.ops.borrow()[0] {
          Op::Call(_, name, _, args) => match name.as_str() {
            "<init>" => {
              let id = args[0].clone().unwrap_initial().unwrap_int();
              // This is a `new ResourceLocation("name")`, so we unwrap it.
              let name = args[1].clone().ops.borrow().first().unwrap().clone().unwrap_call().3[0]
                .clone()
                .unwrap_initial()
                .unwrap_string();
              enchantments.push(Enchantment { name, id: id.try_into().unwrap() });
            }
            _ => todo!("unknown particle register name: {:?}", name),
          },
          v => todo!("unknown particle {:?}", v),
        }
      }
      _ => {}
    }
  }
  Ok(enchantments)
}
