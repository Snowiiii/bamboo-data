use super::Version;
use std::{
  collections::HashMap,
  fs,
  fs::File,
  io,
  io::Write,
  path::{Path, PathBuf},
  time::Instant,
};

mod block;
mod command;
mod convert;
mod enchantment;
mod entity;
mod item;
mod jar;
mod packet;
mod particle;
mod tag;

use anyhow::Result;
use convert::Converter;
use jar::Jar;

pub struct Output {
  base:    PathBuf,
  files:   HashMap<&'static str, String>,
  pub all: HashMap<&'static str, serde_json::Value>,
}

/// Analyzes a given path. This should be something like `build/mcp-08`. This
/// function will create an output folder next to `mc-{ver}/src`. It will look
/// for various files within `mc-{ver}/src` to perform the analysis.
pub fn analyze(b: &Path, ver: Version, pretty: bool) -> Result<Output> {
  let mut files = HashMap::new();

  let p = b.join(ver.build_dir());
  fs::create_dir_all(p.join("generated"))?;

  let start = Instant::now();
  // info!("generating output json for {}", p.display());

  let file = File::open(p.join("mc/mc.jar"))?;
  let mut jar = Jar::new(file, ver, &p);
  let mut all = HashMap::new();

  macro_rules! generate {
    ($m:ident, $name:expr $(, $arg:expr)*) => {{
      let def = $m::generate(&mut jar $(,$arg)*)?;
      all.insert($name, serde_json::to_value(&def)?);
      files.insert($name, if pretty {
        serde_json::to_string_pretty(&def)?
      } else {
        serde_json::to_string(&def)?
      });
      def
    }};
  }
  macro_rules! generate_opt {
    ($m:ident, $name:expr $(, $arg:expr)*) => {{
      if let Some(def) = $m::generate(&mut jar $(,$arg)*)? {
        all.insert($name, serde_json::to_value(&def)?);
        files.insert($name, if pretty {
          serde_json::to_string_pretty(&def)?
        } else {
          serde_json::to_string(&def)?
        });
        Some(def)
      } else {
        None
      }
    }};
  }

  generate!(packet, "protocol");
  generate!(particle, "particles");
  generate!(enchantment, "enchantments");
  generate_opt!(command, "commands");
  let tags = generate!(tag, "tags");
  let blocks = generate!(block, "blocks", &tags);
  generate!(item, "items", &blocks, &tags);
  generate!(entity, "entities", &tags);

  files.insert(
    "all",
    if pretty { serde_json::to_string_pretty(&all)? } else { serde_json::to_string(&all)? },
  );

  info!("generated output json for {} in {}", p.display(), crate::time(start));
  Ok(Output { base: b.join(ver.build_dir()), files, all })
}

impl Output {
  pub fn to_disk(&self) -> io::Result<()> {
    for (name, data) in &self.files {
      let path = &self.base.join(format!("generated/{name}.json"));
      let mut file = File::create(path)?;
      file.write_all(data.as_bytes())?;
    }
    Ok(())
  }
}
