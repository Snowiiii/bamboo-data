use super::{super::Jar, Entity, EntityDef, MetadataField, MetadataType};
use crate::decomp::{Instr, Value};
use anyhow::Result;

pub fn process(jar: &mut Jar) -> Result<EntityDef> {
  let class = jar.decomp("net/minecraft/entity/EntityList", |ver, name, _| {
    if ver.maj() >= 11 {
      name == "init"
    } else {
      name == "<clinit>"
    }
  })?;

  let (_, _, block) = class.funcs.first().unwrap();
  let entities =
    generate(jar, &block.instr).map_err(|e| e.context("error while parsing entities"))?;

  Ok(EntityDef { entities })
}

fn generate(jar: &mut Jar, instr: &[Instr]) -> Result<Vec<Option<Entity>>> {
  let mut entities = vec![];

  let mut player = Entity {
    class: "net/minecraft/entity/player/EntityPlayer".into(),
    name: "player".into(),
    id: 0,
    ..Default::default()
  };
  read_entity_metadata(jar, &mut player)?;
  entities.push(Some(player));

  for i in instr {
    match i.clone() {
      Instr::Expr(ref it) if !it.ops.borrow().is_empty() => {}
      Instr::Expr(it) => match it.unwrap_initial() {
        Value::StaticCall(_, name, args) => {
          let mut entity = match name.as_str() {
            "addMapping" => {
              assert!(
                args.len() == 3 || args.len() == 5,
                "addMapping takes 3 or 5 args, not {:?}",
                args
              );
              let class = args[0].clone().unwrap_initial().unwrap_class();
              let name = if args[1].ops.borrow().is_empty() {
                args[1].clone().unwrap_initial().unwrap_string()
              } else {
                match &class {
                  s if s.contains("EntityMinecartEmpty") => "MinecartRideable",
                  s if s.contains("EntityMinecartChest") => "MinecartChest",
                  s if s.contains("EntityMinecartFurnace") => "MinecartFurnace",
                  s if s.contains("EntityMinecartTNT") => "MinecartTNT",
                  s if s.contains("EntityMinecartHopper") => "MinecartHopper",
                  s if s.contains("EntityMinecartMobSpawner") => "MinecartSpawner",
                  s if s.contains("EntityMinecartCommandBlock") => "MinecartCommandBlock",
                  _ => panic!(),
                }
                .into()
              };
              let id = args[2].clone().unwrap_initial().unwrap_int();
              Entity { class, name, id: id.try_into().unwrap(), ..Default::default() }
            }
            // addMapping for 1.11 and 1.12
            "func_191303_a" => {
              assert!(args.len() == 4, "addMapping takes 4 args, not {:?}", args);
              let id = args[0].clone().unwrap_initial().unwrap_int();
              let name = args[1].clone().unwrap_initial().unwrap_string();
              let class = args[2].clone().unwrap_initial().unwrap_class();
              Entity { class, name, id: id.try_into().unwrap(), ..Default::default() }
            }
            // Set's an entities spawn egg color
            "func_191305_a" => {
              assert!(args.len() == 3, "setSpawnEgg takes 4 args, not {:?}", args);
              let _name = args[0].clone().unwrap_initial().unwrap_string();
              let _color1 = args[1].clone().unwrap_initial().unwrap_int();
              let _color2 = args[2].clone().unwrap_initial().unwrap_int();
              continue;
            }
            _ => todo!("unknown entity register name: {:?}", name),
          };
          read_entity_metadata(jar, &mut entity)?;
          while entities.len() <= entity.id as usize {
            entities.push(None);
          }
          let id = entity.id as usize;
          entities[id] = Some(entity);
        }
        v => todo!("unknown item {:?}", v),
      },
      _ => {}
    }
  }
  Ok(entities)
}

fn read_entity_metadata(jar: &mut Jar, entity: &mut Entity) -> Result<()> {
  let fields = read_entity_metadata_class(jar, &entity.class, &entity.name)?;
  entity.metadata = fields;
  Ok(())
}

fn read_entity_metadata_class(
  jar: &mut Jar,
  class_name: &str,
  entity_name: &str,
) -> Result<Vec<MetadataField>> {
  if class_name == "net/minecraft/entity/Entity" {
    // TODO: Figure out what version this change happened (it was something between
    // 1.8 and 1.12.2)
    if jar.ver().maj() >= 9 {
      return Ok(vec![
        MetadataField { id: 0, name: "flags".into(), ty: MetadataType::Byte },
        MetadataField { id: 1, name: "air".into(), ty: MetadataType::VarInt },
        MetadataField { id: 2, name: "custom_name".into(), ty: MetadataType::String },
        MetadataField { id: 3, name: "name_visible".into(), ty: MetadataType::Bool },
        MetadataField { id: 4, name: "silent".into(), ty: MetadataType::Bool },
        MetadataField { id: 5, name: "no_gravity".into(), ty: MetadataType::Bool },
      ]);
    } else {
      return Ok(vec![
        MetadataField { id: 0, name: "flags".into(), ty: MetadataType::Byte },
        MetadataField { id: 1, name: "air".into(), ty: MetadataType::VarInt },
        MetadataField { id: 2, name: "custom_name".into(), ty: MetadataType::String },
        MetadataField { id: 3, name: "name_visible".into(), ty: MetadataType::Bool },
        MetadataField { id: 4, name: "silent".into(), ty: MetadataType::Bool },
      ]);
    }
  }

  let sup = jar.deobf_super(class_name)?;
  let mut fields = read_entity_metadata_class(jar, &sup, entity_name)?;

  let class = jar.decomp(&class_name, |_, name, _| name == "entityInit")?;
  if let Some((_, _, block)) = class.funcs.first() {
    for instr in &block.instr {
      match instr {
        Instr::Expr(expr) => {
          let (_, name, _, args) = if expr.initial == Value::Var(0) {
            expr.ops.borrow()[1].clone().unwrap_call()
          } else if matches!(expr.initial, Value::Field(_, ref field)
            if matches!(field.as_str(), "dataWatcher" | "dataManager"))
          {
            expr.ops.borrow()[0].clone().unwrap_call()
          } else if matches!(expr.initial, Value::Class(_)) {
            continue;
          } else {
            panic!("invalid expr {expr:?}");
          };
          match name.as_str() {
            "addObject" => {
              assert_eq!(jar.ver().maj(), 8);
              let id = args[0].clone().unwrap_initial().unwrap_int().try_into().unwrap();
              let field = convert_field(class_name.split('/').last().unwrap(), id);
              fields.push(MetadataField {
                id,
                ty: super::entity_metadata_type(&field, &args[1]),
                name: field,
              });
            }
            "addObjectByDataType" => {
              assert_eq!(jar.ver().maj(), 8);
              let id = args[0].clone().unwrap_initial().unwrap_int().try_into().unwrap();
              let field = convert_field(class_name.split('/').last().unwrap(), id);
              let ty = args[1].clone().unwrap_initial().unwrap_int();
              fields.push(MetadataField { id, ty: entity_metadata_type_id(ty), name: field });
            }
            "register" => {
              assert!(jar.ver().maj() >= 9);
              let (_, _, field) = args[0].clone().unwrap_initial().unwrap_static_field();
              let field = field.to_ascii_lowercase();
              let id = fields.len() as u32;
              fields.push(MetadataField {
                id,
                ty: super::entity_metadata_type(&field, &args[1]),
                name: old_metadata_name(&field, entity_name).into(),
              });
            }
            _ => panic!("invalid call {name}"),
          }
        }
        Instr::Super(_) => {}
        Instr::Return(_) => {}
        Instr::Assign(_, _) => {}
        Instr::Loop(_, _) => {}
        _ => panic!("unexpected instr {instr:?} in class {class_name}"),
      }
    }
  }
  Ok(fields)
}

fn convert_field(name: &str, id: u32) -> String {
  match (name, id) {
    ("EntityLivingBase", 6) => "health",
    ("EntityLivingBase", 7) => "potion_swirls_color",
    ("EntityLivingBase", 8) => "potion_swirls_ambient",
    ("EntityLivingBase", 9) => "stuck_arrow_count",

    ("EntityFireworkRocket", 8) => "item",
    ("EntityEnderCrystal", 8) => "show_bottom",

    ("EntityItemFrame", 8) => "item_stack",
    ("EntityItemFrame", 9) => "rotation",

    ("EntityItem", 10) => "stack",
    ("EntityWitherSkull", 10) => "charged",

    ("EntityPlayer", 10) => "player_model_parts",
    ("EntityPlayer", 16) => "main_arm",
    ("EntityPlayer", 17) => "absorption_amount",
    ("EntityPlayer", 18) => "score",

    ("EntityArmorStand", 10) => "armor_stand_flags",
    ("EntityArmorStand", 11) => "tracker_head_rotation",
    ("EntityArmorStand", 12) => "tracker_body_rotation",
    ("EntityArmorStand", 13) => "tracker_left_arm_rotation",
    ("EntityArmorStand", 14) => "tracker_right_arm_rotation",
    ("EntityArmorStand", 15) => "tracker_left_leg_rotation",
    ("EntityArmorStand", 16) => "tracker_right_leg_rotation",

    ("EntityAgeable", 12) => "child",

    ("EntityZombie", 12) => "baby",
    ("EntityZombie", 13) => "zombie_type",
    ("EntityZombie", 14) => "converting_in_water",

    ("EntitySkeleton", 13) => "connverting",

    // Whether entity has AI disabled for this version, but it
    // means more things in newer versions
    ("EntityLiving", 15) => "living_flags",

    ("EntityArrow", 16) => "projectile_flags",
    ("EntityMinecartFurnace", 16) => "lit", // is powered
    ("EntitySpider", 16) => "spider_flags",
    ("EntitySlime", 16) => "slime_size",
    ("EntityGhast", 16) => "shooting",
    ("EntityBlaze", 16) => "blaze_flags",
    ("EntityBat", 16) => "bat_flags",
    ("EntityPig", 16) => "saddled",
    ("EntitySheep", 16) => "color",
    ("EntityIronGolem", 16) => "iron_golem_flags",
    ("EntityVillager", 16) => "villager_data",

    ("EntityEnderman", 16) => "carried_block",
    ("EntityEnderman", 17) => "angry",
    ("EntityEnderman", 18) => "provoked",

    ("EntityCreeper", 16) => "fuse_speed",
    ("EntityCreeper", 17) => "charged",
    ("EntityCreeper", 18) => "ignited",

    ("EntityGuardian", 16) => "spikes_retracted",
    ("EntityGuardian", 17) => "beam_target_id",

    ("EntityTameable", 16) => "tameable_flags",
    ("EntityTameable", 17) => "owner_uuid",

    ("EntityHorse", 16) => "horse_flags",
    ("EntityHorse", 19) => "variant",
    ("EntityHorse", 20) => "horse_color",
    ("EntityHorse", 21) => "owner_uuid",
    ("EntityHorse", 22) => "horse_armor",

    ("EntityBoat", 17) => "damage_wobble_ticks",
    ("EntityBoat", 18) => "forward_direction", // Has no new alternative!
    ("EntityBoat", 19) => "damage_taken",      // Has no new alternative!

    ("EntityWither", 17) => "tracked_entity_id_1",
    ("EntityWither", 18) => "tracked_entity_id_2",
    ("EntityWither", 19) => "tracked_entity_id_3",
    ("EntityWither", 20) => "invul_timer",

    ("EntityMinecart", 17) => "damage_wobble_ticks",
    ("EntityMinecart", 18) => "damage_wobble_side",
    ("EntityMinecart", 19) => "damage_wobble_strength",
    ("EntityMinecart", 20) => "custom_block_id",
    ("EntityMinecart", 21) => "custom_block_offset",
    ("EntityMinecart", 22) => "custom_block_present",

    ("EntityOcelot", 18) => "trusting",
    ("EntityRabbit", 18) => "rabbit_type",

    ("EntityWolf", 18) => "begging",
    ("EntityWolf", 19) => "collar_color",
    ("EntityWolf", 20) => "anger_time",

    ("EntityWitch", 21) => "drinking",

    ("EntityMinecartCommandBlock", 23) => "command",
    ("EntityMinecartCommandBlock", 24) => "last_output",

    _ => {
      println!("unknown field {name} {id}");
      return name.into();
    }
  }
  .into()
}

fn entity_metadata_type_id(id: i32) -> MetadataType {
  match id {
    5 => MetadataType::Item,
    _ => panic!("invalid metadata type {id}"),
  }
}

fn old_metadata_name(entity: &str, name: &str) -> String {
  match name {
    "item" => match entity {
      "item" => "stack",
      "potion" => "item",
      _ => panic!("invalid entity with metadata field {name}: {entity}"),
    },
    _ => name,
  }
  .into()
}
