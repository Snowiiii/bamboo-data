use super::{tag::TagsDef, Jar};
use crate::decomp::{Item, Value};
use anyhow::Result;
use serde::Serialize;

mod new;
mod old;

#[derive(Debug, Clone, Serialize)]
pub struct EntityDef {
  entities: Vec<Option<Entity>>,
}

#[derive(Debug, Clone, Default, Serialize)]
pub struct Entity {
  /// The id of the entity.
  id:    u32,
  /// The name of the entity.
  name:  String,
  /// The full class of this entity.
  class: String,

  tags: Vec<String>,

  category:       String,
  width:          f32,
  height:         f32,
  fire_immune:    bool,
  tracking_range: u32,

  metadata: Vec<MetadataField>,
}

#[derive(Debug, Clone, Serialize)]
pub struct MetadataField {
  /// The index of this metadata field.
  id:   u32,
  /// The name of this field. This is how cross-versioning works. We use the
  /// yarn mappings here, and convert MCP mappings into yarn mappings.
  name: String,
  /// The kind of metadata.
  ty:   MetadataType,
}

/// An entity metadata type. Note that the documentation for this type is for
/// 1.18.2. Older versions will have different serializing/deserializing rules.
///
/// Use this:
/// ```text
/// net/minecraft/entity/data/TrackedDataHandlerRegistryvanet/minecraft/entity/data/TrackedDataHandlerRegistry.java
/// ```
#[derive(Debug, Clone, Serialize)]
pub enum MetadataType {
  /// A single byte.
  Byte,
  /// A varint (same as protocol).
  VarInt,
  /// A long. Only present on 1.19.3+
  Long,
  /// A short. Only present on 1.8-1.12.
  Short,
  /// A 4 byte floating point number
  Float,
  /// A varint prefixed string
  String,
  /// A string, which is JSON encoded chat data.
  Chat,
  /// A boolean. If true, this is followed by a Chat field.
  OptChat,
  /// An item stack. Same as protocol.
  Item,
  /// A single byte.
  Bool,
  /// 3 floats for X, Y, then Z.
  Rotation,
  /// A position encoded as a long.
  Position,
  /// A boolean. If true, this is followed by a Position.
  OptPosition,
  /// A VarInt. This will be from 0-5 (inclusive), which maps to a direction
  /// like so:
  /// - 0: Down
  /// - 1: Up
  /// - 2: North
  /// - 3: South
  /// - 4: West
  /// - 5: East
  Direction,
  /// A boolean. If true, then a 16 byte UUID follows.
  OptUUID,
  /// A varint, which should be parsed as a block ID.
  BlockID,
  /// An NBT tag. This is not length prefixed. The entire tag must be parsed to
  /// find the end of this field.
  NBT,
  /// A VarInt for the particle ID, followed by some data. The data following
  /// must be infered from the particle ID.
  Particle,
  /// 3 VarInts: villager type, villager profession, and villager level.
  VillagerData,
  /// A boolean. If true, a VarInt follows.
  OptVarInt,
  /// A VarInt, from 0-7 (inclusive). The numbers map to these poses:
  /// - 0: Standing
  /// - 1: Fall flying
  /// - 2: Sleeping
  /// - 3: Swiming
  /// - 4: Spin attack
  /// - 5: Sneaking
  /// - 6: Long jumping
  /// - 7: Dying
  Pose,

  /// TODO: Figure out what this is!
  FireworkData,
  /// A varint
  CatVariant,
  /// A varint
  FrogVariant,
  /// A varint
  PaintingVariant,

  // 1.19.4+
  SnifferState,
  Vector3,
  Vector4,
}

pub fn generate(jar: &mut Jar, tags: &TagsDef) -> Result<impl Serialize> {
  if jar.ver().is_old() {
    old::process(jar)
  } else {
    let mut def = new::process(jar)?;
    for entity in &mut def.entities {
      if let Some(e) = entity {
        e.tags = tags.get("entity_type").get(&e.name);
      }
    }
    Ok(def)
  }
}

fn entity_metadata_type(field: &str, it: &Item) -> MetadataType {
  if it.ops.borrow().is_empty() {
    match it.clone().unwrap_initial() {
      Value::String(_) => MetadataType::String,
      Value::StaticCall(class, name, _args) => {
        if name == "valueOf" {
          match class.as_str() {
            "java/lang/Byte" => MetadataType::Byte,
            "java/lang/Integer" => MetadataType::VarInt,
            "java/lang/Long" => MetadataType::Long,
            "java/lang/Float" => MetadataType::Float,
            "java/lang/Boolean" => MetadataType::Bool,
            "java/lang/String" => MetadataType::String,
            _ => panic!("invalid valueOf class {class}"),
          }
        } else if name == "empty" || name == "absent" {
          // For optionals
          match field {
            "glow_color_override" => MetadataType::VarInt,
            "sleeping_position" | "attached_block" | "attached_block_pos" | "last_death_pos" => {
              MetadataType::OptPosition
            }
            "optional_uuid" | "owner_uuid" | "owner" | "other_trusted" | "owner_unique_id" => {
              MetadataType::OptUUID
            }
            "beam_target" => MetadataType::OptPosition,
            "carried_block" | "max_entity_id" => MetadataType::OptVarInt,
            "shooter_entity_id" => MetadataType::FireworkData,
            "item" | "firework_item" => MetadataType::Item,
            "target" => MetadataType::OptVarInt,
            "rendering_data_ids" => MetadataType::OptVarInt,
            _ => panic!("unknown field for optional: {field}"),
          }
        } else if class == "net/minecraft/entity/passive/MooshroomEntity$Type" {
          MetadataType::BlockID
        } else if class == "net/minecraft/entity/decoration/painting/PaintingEntity" {
          MetadataType::PaintingVariant
        } else {
          panic!("unknown call {it:?}");
        }
      }
      Value::StaticField(class, _, _field) => match class.as_str() {
        "net/minecraft/particle/ParticleTypes" => MetadataType::Particle,
        "net/minecraft/entity/passive/CatVariant" => MetadataType::CatVariant,
        "net/minecraft/entity/passive/FrogVariant" => MetadataType::FrogVariant,
        "net/minecraft/entity/decoration/ArmorStandEntity"
        | "net/minecraft/entity/item/EntityArmorStand" => MetadataType::Rotation,
        "net/minecraft/util/math/BlockPos" => MetadataType::Position,
        "net/minecraft/item/ItemStack" => MetadataType::Item,
        "net/minecraft/text/LiteralText"
        | "net/minecraft/screen/ScreenTexts"
        | "net/minecraft/client/gui/screen/ScreenTexts" => MetadataType::Chat,
        "net/minecraft/util/math/Direction" | "net/minecraft/util/EnumFacing" => {
          MetadataType::Direction
        }
        _ => panic!("unknown field {class} {_field}"),
      },
      Value::Field(initial, _field) => {
        match initial.unwrap_initial().unwrap_static_field().0.as_str() {
          "net/minecraft/entity/passive/MooshroomEntity$Type" => MetadataType::BlockID,
          _ => panic!(),
        }
      }
      v => panic!("unknown value {v:?}"),
    }
  } else {
    match &it.initial {
      Value::Class(name) if name == "net/minecraft/village/VillagerData" => {
        MetadataType::VillagerData
      }
      Value::Class(name)
        if matches!(
          name.as_str(),
          "net/minecraft/nbt/NBTTagCompound"
            | "net/minecraft/nbt/NbtCompound"
            | "net/minecraft/nbt/CompoundTag"
        ) =>
      {
        MetadataType::NBT
      }
      Value::Class(name)
        if name == "net/minecraft/text/LiteralText"
          || name == "net/minecraft/util/text/TextComponentString" =>
      {
        MetadataType::Chat
      }
      Value::Class(name) if name == "java/lang/Byte" => MetadataType::Byte,
      Value::Class(name) if name == "java/lang/Short" => MetadataType::Short,
      Value::Class(name) if name == "java/lang/Integer" => MetadataType::VarInt,
      Value::Class(name) if name == "java/lang/Float" => MetadataType::Float,
      Value::StaticField(class, _, field)
        if class == "net/minecraft/util/registry/Registry" && field == "CAT_VARIANT" =>
      {
        MetadataType::CatVariant
      }
      Value::StaticField(class, _, field)
        if class == "net/minecraft/registry/Registries" && field == "CAT_VARIANT" =>
      {
        MetadataType::CatVariant
      }
      Value::Class(name) if name == "org/joml/Vector3f" => MetadataType::Vector3,
      Value::Class(name) if name == "org/joml/Quaternionf" => MetadataType::Vector4,
      // This is `Blocks.AIR.getDefaultState()` (I hope)
      Value::StaticField(name, _, field)
        if name == "net/minecraft/block/Blocks" && field == "a" =>
      {
        MetadataType::BlockID
      }
      _ => panic!("unknown value {it:?}"),
    }
  }
}
