use super::{super::Jar, Entity, EntityDef, MetadataField, MetadataType};
use crate::decomp::{Instr, Item, Op, Value};
use anyhow::Result;

pub fn process(jar: &mut Jar) -> Result<EntityDef> {
  let class = jar.decomp("net/minecraft/entity/EntityType", |_, name, _| name == "<clinit>")?;

  let (_, _, block) = class.funcs.first().unwrap();
  let entities =
    generate(jar, &block.instr).map_err(|e| e.context("error while parsing entities"))?;

  Ok(EntityDef { entities })
}

fn generate(jar: &mut Jar, instr: &[Instr]) -> Result<Vec<Option<Entity>>> {
  let mut id = 0;
  let mut entities = vec![];
  for i in instr {
    match i.clone() {
      Instr::SetField(_field_name, it) => match it.unwrap_initial() {
        Value::StaticCall(_, name, args) => match name.as_str() {
          "getLogger" => {}
          "register" => {
            assert!(args.len() == 2, "register takes 2 args, not {:?}", args);
            let name = args[0].clone().unwrap_initial().unwrap_string();
            let mut entity = Entity { id, name, ..Default::default() };
            read_builder(&args[1], &mut entity);
            read_entity_metadata(jar, &mut entity)?;
            entities.push(Some(entity));
            id += 1;
          }
          _ => todo!("unknown entity register name: {:?}", name),
        },
        v => todo!("unknown item {:?}", v),
      },
      _ => {}
    }
  }
  Ok(entities)
}

fn read_builder(expr: &Item, entity: &mut Entity) {
  let (_class, name, args) = expr.initial.clone().unwrap_static_call();
  assert_eq!(name, "create");
  assert!(matches!(args.len(), 1 | 2), "create takes 1 or 2 args, not {:?}", args);
  match entity.name.as_str() {
    "item" => entity.class = "net/minecraft/entity/ItemEntity".to_string(),
    "fishing_bobber" => {
      entity.class = "net/minecraft/entity/projectile/FishingBobberEntity".to_string()
    }
    "lightning_bolt" => {
      entity.class = "net/minecraft/entity/LightningEntity".to_string();
    }
    "falling_block" => {
      entity.class = "net/minecraft/entity/FallingBlockEntity".to_string();
    }
    "painting" => {
      entity.class = "net/minecraft/entity/decoration/painting/PaintingEntity".to_string();
    }
    _ => {
      if args.len() == 2 {
        let init = args[0].clone().unwrap_initial();
        if let Value::MethodRef(class, name) = init {
          assert_eq!(name, "<init>");
          entity.class = class;
        } else {
          panic!("unknown class for entity {entity:?}");
        }
      }
    }
  }
  let (_, _, category) = args.last().unwrap().clone().unwrap_initial().unwrap_static_field();
  entity.category = category;

  if entity.name == "player" {
    entity.class = "net/minecraft/entity/player/PlayerEntity".to_string();
  }

  for op in expr.ops.borrow().iter() {
    match op {
      Op::Call(class, name, _, args) => {
        assert_eq!(class, "net/minecraft/entity/EntityType$Builder");
        match name.as_str() {
          "makeFireImmune" => {
            assert_eq!(args.len(), 0);
            entity.fire_immune = true
          }
          "setDimensions" => {
            assert_eq!(args.len(), 2);
            entity.width = match args[0].clone().unwrap_initial() {
              Value::StaticField(class, _, field) => match (class.as_str(), field.as_str()) {
                ("net/minecraft/entity/passive/TadpoleEntity", "WIDTH") => 0.4,
                _ => panic!("unknown field {class} {field}"),
              },
              v => v.clone().unwrap_float(),
            };
            entity.height = match args[1].clone().unwrap_initial() {
              Value::StaticField(class, _, field) => match (class.as_str(), field.as_str()) {
                ("net/minecraft/entity/passive/TadpoleEntity", "HEIGHT") => 0.3,
                _ => panic!("unknown field {class} {field}"),
              },
              v => v.clone().unwrap_float(),
            };
          }
          "disableSaving" => {
            assert_eq!(args.len(), 0);
            // TODO: Maybe save this? It doesn't seem needed
          }
          // I'm not reading mappings right :/
          // I cannot find what this maps to in the fernflower
          // recompilation. It probably doesn't matter.
          "d" => {
            assert_eq!(args.len(), 0);
          }
          // We don't really care about this. I would like to summon xp orbs anyways.
          "disableSummon" => {
            assert_eq!(args.len(), 0);
          }
          // Found on 1.15+
          // Used for shulkers and villagers. Again, we don't care about this.
          "spawnableFarFromPlayer" => {
            assert_eq!(args.len(), 0);
          }
          // Found on 1.16+. This is nice to have, as it allows us to easily replicate
          // mob AI.
          "maxTrackingRange" => {
            assert_eq!(args.len(), 1);
            entity.tracking_range =
              args[0].clone().unwrap_initial().unwrap_int().try_into().unwrap();
          }
          // Found on 1.16+. This is very implementation specific, so I'm not going to
          // bother storing this.
          "trackingTickInterval" => {
            assert_eq!(args.len(), 1);
          }
          // Found on 1.16+. Used for foxes and withers. This does a static lookup on `Blocks`,
          // and I couldn't be bothered to pass in the blocks table to this function.
          "allowSpawningInside" => {
            assert_eq!(args.len(), 1);
          }
          // Found on 1.19.3+. Used to only spawn the entity when a certain feature flag is set.
          "requires" => {}
          // Found on 1.19.4+. Used <edit this>
          "tickable" => {
            assert_eq!(args.len(), 1);
          }
          _ => panic!("unexpected builder call {:?} {:?}", name, args),
        }
      }
      v => panic!("unexpected builder op {:?}", v),
    }
  }
}

fn read_entity_metadata(jar: &mut Jar, entity: &mut Entity) -> Result<()> {
  let fields = read_entity_metadata_class(jar, &entity.class)?;
  entity.metadata = fields;
  Ok(())
}

fn read_entity_metadata_class(jar: &mut Jar, class_name: &str) -> Result<Vec<MetadataField>> {
  if class_name == "net/minecraft/entity/Entity" {
    let mut fields = vec![
      MetadataField { id: 0, name: "flags".into(), ty: MetadataType::Byte },
      MetadataField { id: 1, name: "air".into(), ty: MetadataType::VarInt },
      MetadataField { id: 2, name: "name_visible".into(), ty: MetadataType::Bool },
      MetadataField { id: 3, name: "custom_name".into(), ty: MetadataType::OptChat },
      MetadataField { id: 4, name: "silent".into(), ty: MetadataType::Bool },
      MetadataField { id: 5, name: "no_gravity".into(), ty: MetadataType::Bool },
      MetadataField { id: 6, name: "pose".into(), ty: MetadataType::Pose },
    ];
    if jar.ver().maj() >= 17 {
      fields.push(MetadataField {
        id:   7,
        name: "frozen_ticks".into(),
        ty:   MetadataType::VarInt,
      });
    }
    return Ok(fields);
  }

  let sup = jar.deobf_super(class_name)?;
  let mut fields = read_entity_metadata_class(jar, &sup)?;

  let class = jar.decomp(&class_name, |_, name, _| name == "initDataTracker")?;
  if let Some((_, _, block)) = class.funcs.first() {
    for instr in &block.instr {
      match instr {
        Instr::Expr(expr) => {
          let (_, name, _, args) = if expr.initial == Value::Var(0) {
            expr.ops.borrow()[1].clone().unwrap_call()
          } else if matches!(expr.initial, Value::Field(_, ref field) if field == "dataTracker") {
            expr.ops.borrow()[0].clone().unwrap_call()
          } else if expr.ops.borrow()[0].clone().unwrap_call().1 == "<init>" {
            continue;
          } else {
            panic!("invalid expr {expr:?}");
          };
          assert_eq!(name, "startTracking");
          let (_, _, field) = args[0].clone().unwrap_initial().unwrap_static_field();
          let field = field.to_ascii_lowercase();
          let id = fields.len() as u32;
          fields.push(MetadataField {
            id,
            ty: super::entity_metadata_type(&field, &args[1]),
            name: field,
          });
        }
        Instr::Super(_) => {}
        Instr::Return(_) => {}
        _ => panic!("unexpected instr {instr:?}"),
      }
    }
  }
  Ok(fields)
}
