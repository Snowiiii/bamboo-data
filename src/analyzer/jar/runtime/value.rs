use std::{cell::RefCell, collections::HashMap, rc::Rc};

#[derive(Debug, Clone)]
pub struct Object {
  pub super_class: Option<Rc<RefCell<Object>>>,
  pub class:       String,
  pub fields:      HashMap<String, RTValue>,
}

#[derive(Debug, Clone)]
pub enum RTValue {
  Int(i32),
  Float(f32),
  String(String),
  Object(Rc<RefCell<Object>>),
  Null,
}

impl Object {
  pub fn from_decomp(class: String, fields: impl Iterator<Item = String>) -> Self {
    Object { super_class: None, class, fields: fields.map(|v| (v, RTValue::Null)).collect() }
  }
  pub fn empty() -> Self {
    Object {
      super_class: None,
      class:       "java/lang/Object".into(),
      fields:      HashMap::new(),
    }
  }
}

#[allow(unused)]
impl RTValue {
  pub fn unwrap_int(self) -> i32 {
    match self {
      RTValue::Int(v) => v,
      _ => panic!("not an int: {self:?}"),
    }
  }
  pub fn unwrap_float(self) -> i32 {
    match self {
      RTValue::Int(v) => v,
      _ => panic!("not a float: {self:?}"),
    }
  }
}
