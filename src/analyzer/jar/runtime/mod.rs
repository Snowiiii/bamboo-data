use super::{DecompileError, Jar};
use crate::decomp::{Cond, Instr, Item, Op, Type, Value, VarBlock};
use std::{cell::RefCell, collections::HashMap, rc::Rc};

mod value;

pub use value::{Object, RTValue};

#[derive(Debug)]
pub enum RTError {
  UnknownInstr(Instr),
  UnknownValue(Value),
  UnknownOp(Op),
  Decompile(DecompileError),
  CallNonObject(RTValue),
  InvalidOp(RTValue, Op),
  CannotCompare(RTValue, RTValue),
}

impl From<DecompileError> for RTError {
  fn from(error: DecompileError) -> Self {
    RTError::Decompile(error)
  }
}

type Result<T> = std::result::Result<T, RTError>;

struct Runtime<'a, 'b> {
  jar:     &'a mut Jar<'b>,
  statics: HashMap<String, Rc<RefCell<Object>>>,
}

struct Context<'a, 'b, 'c> {
  runtime: &'a mut Runtime<'b, 'c>,
  this:    &'a Rc<RefCell<Object>>,
  vars:    Vec<RTValue>,
}

impl<'a, 'b> Runtime<'a, 'b> {
  fn new(jar: &'a mut Jar<'b>) -> Self {
    Runtime { jar, statics: HashMap::new() }
  }

  fn static_obj(&mut self, cls: &str) -> Result<&Rc<RefCell<Object>>> {
    if !self.statics.contains_key(cls) {
      let obj = self.load_object(cls)?;
      if self.jar.has_func(cls, "<clinit>") {
        let (_, block) = self.jar.decomp_func(cls, "<clinit>")?.clone();
        self.eval_block(&obj, vec![], &block).unwrap();
      }
      self.statics.insert(cls.into(), obj);
    }
    Ok(&self.statics[cls])
  }

  fn load_object(&mut self, cls: &str) -> Result<Rc<RefCell<Object>>> {
    Ok(Rc::new(RefCell::new(Object::from_decomp(
      cls.into(),
      self.jar.decomp_private(cls, |_, _, _| false)?.fields.into_iter().map(|(name, _)| name),
    ))))
  }
  fn new_object(&mut self, cls: &str) -> Result<Rc<RefCell<Object>>> {
    let obj = self.load_object(cls)?;
    let sup = self.jar.deobf_super(cls)?;
    if sup != "/java/lang/Object" && sup != "java/lang/Object" {
      obj.borrow_mut().super_class = Some(self.load_object(&sup)?);
    }
    Ok(obj)
  }
}

impl Runtime<'_, '_> {
  #[track_caller]
  fn eval_block(
    &mut self,
    obj: &Rc<RefCell<Object>>,
    args: Vec<RTValue>,
    block: &VarBlock,
  ) -> Result<RTValue> {
    let mut vars = vec![];
    let mut i = 0;
    for ty in block.vars.iter() {
      match ty {
        crate::decomp::VarKind::This => vars.push(RTValue::Object(obj.clone())),
        crate::decomp::VarKind::Arg(_) => {
          vars.push(args[i].clone());
          i += 1;
        }
        crate::decomp::VarKind::Local => vars.push(RTValue::Null),
      }
    }
    let mut ctx = Context { runtime: self, this: obj, vars };
    for instr in &block.instr {
      if let Some(ret) = ctx.eval_instr(instr)? {
        return Ok(ret);
      }
    }
    Ok(RTValue::Null)
  }
}

impl Context<'_, '_, '_> {
  fn eval_block(&mut self, instr: &[Instr]) -> Result<Option<RTValue>> {
    for instr in instr {
      if let Some(ret) = self.eval_instr(instr)? {
        return Ok(Some(ret));
      }
    }
    Ok(None)
  }

  fn eval_instr(&mut self, instr: &Instr) -> Result<Option<RTValue>> {
    match instr {
      Instr::Super(_) => {
        // TODO: Inheritance
      }
      Instr::Expr(v) => {
        self.eval_item(v)?;
      }
      Instr::SetField(field, v) => {
        let v = self.eval_item(v)?;
        self.this.borrow_mut().fields.insert(field.into(), v);
      }
      Instr::Return(v) => return Ok(Some(self.eval_item(v)?)),
      Instr::If(cond, thn, els) => {
        let cond = self.eval_cond(cond)?;
        if cond {
          return self.eval_block(thn);
        } else {
          return self.eval_block(els);
        }
      }
      Instr::Assign(v, value) => {
        let value = self.eval_item(value)?;
        self.vars[*v] = value;
      }
      _ => return Err(RTError::UnknownInstr(instr.clone())),
    }
    Ok(None)
  }

  fn eval_item(&mut self, item: &Item) -> Result<RTValue> {
    let mut init = self.eval_value(&item.initial)?;
    for op in item.ops.borrow().iter() {
      init = self.eval_op(init, op)?;
    }
    Ok(init)
  }

  fn eval_value(&mut self, value: &Value) -> Result<RTValue> {
    match value {
      Value::Lit(v) => Ok(RTValue::Int(*v)),
      Value::LitFloat(v) => Ok(RTValue::Float(*v)),
      Value::String(v) => Ok(RTValue::String(v.clone())),
      Value::Class(cls) => Ok(RTValue::Object(self.runtime.new_object(cls)?)),
      Value::Null => Ok(RTValue::Null),
      Value::Array(_) => todo!(),
      Value::Var(idx) => Ok(self.vars[*idx].clone()),
      /*
      Value::StaticField(cls, _, _) if cls == "net/minecraft/state/property/Properties" => {
        Ok(value.clone())
      }
      */
      Value::StaticCall(cls, func, args) => {
        let args = args.iter().map(|arg| self.eval_item(arg)).collect::<Result<Vec<_>>>()?;
        let (_, block) = self.runtime.jar.decomp_func(cls, func)?.clone();
        let obj = self.runtime.static_obj(&cls)?.clone();
        self.runtime.eval_block(&obj, args, &block)
      }
      Value::StaticField(cls, _, field) if cls == &self.this.borrow().class => {
        Ok(self.this.borrow().fields.get(field).unwrap().clone())
      }
      Value::StaticField(cls, _, field) => {
        let obj = self.runtime.static_obj(&cls)?.clone();
        let borrow = obj.borrow();
        Ok(borrow.fields[field].clone())
      }
      Value::Field(value, field) => {
        let v = self.eval_item(value)?;
        match v {
          RTValue::Object(o) => Ok(o.borrow().fields.get(field).unwrap().clone()),
          v => Err(RTError::CallNonObject(v)),
        }
      }
      _ => Err(RTError::UnknownValue(value.clone())),
    }
  }

  fn eval_op(&mut self, value: RTValue, op: &Op) -> Result<RTValue> {
    match op {
      Op::Call(cls, name, _, args) if name == "<init>" => {
        let args = args.iter().map(|arg| self.eval_item(arg)).collect::<Result<Vec<_>>>()?;
        let (_, block) = self.runtime.jar.decomp_func(cls, name)?.clone();
        match value {
          RTValue::Object(obj) => {
            self.runtime.eval_block(&obj, args, &block)?;
            Ok(RTValue::Object(obj))
          }
          value => Err(RTError::CallNonObject(value)),
        }
      }
      Op::Call(cls, name, _, args) => {
        let args = args.iter().map(|arg| self.eval_item(arg)).collect::<Result<Vec<_>>>()?;
        let (_, block) = self.runtime.jar.decomp_func(cls, name)?.clone();
        match value {
          RTValue::Object(obj) => Ok(self.runtime.eval_block(&obj, args, &block)?),
          value => Err(RTError::CallNonObject(value)),
        }
      }
      Op::Add(r) => {
        let r = self.eval_item(r)?;
        match (value, r) {
          (RTValue::Int(l), RTValue::Int(r)) => Ok(RTValue::Int(l.wrapping_add(r))),
          (value, _) => Err(RTError::InvalidOp(value, op.clone())),
        }
      }
      Op::Sub(r) => {
        let r = self.eval_item(r)?;
        match (value, r) {
          (RTValue::Int(l), RTValue::Int(r)) => Ok(RTValue::Int(l.wrapping_sub(r))),
          (value, _) => Err(RTError::InvalidOp(value, op.clone())),
        }
      }
      Op::Div(r) => {
        let r = self.eval_item(r)?;
        match (value, r) {
          (RTValue::Int(l), RTValue::Int(r)) => Ok(RTValue::Int(l.wrapping_div(r))),
          (RTValue::Float(l), RTValue::Float(r)) => Ok(RTValue::Float(l / r)),
          (value, _) => Err(RTError::InvalidOp(value, op.clone())),
        }
      }
      Op::Mul(r) => {
        let r = self.eval_item(r)?;
        match (value, r) {
          (RTValue::Int(l), RTValue::Int(r)) => Ok(RTValue::Int(l.wrapping_mul(r))),
          (value, _) => Err(RTError::InvalidOp(value, op.clone())),
        }
      }
      Op::Shl(v) => {
        let v = self.eval_item(v)?;
        match (value, v) {
          (RTValue::Int(v), RTValue::Int(shift)) if shift > 0 => {
            Ok(RTValue::Int(v.wrapping_shl(shift.try_into().unwrap())))
          }
          (value, _) => Err(RTError::InvalidOp(value, op.clone())),
        }
      }
      Op::Cast(ty) => match (value, ty) {
        (RTValue::Int(v), Type::Float) => Ok(RTValue::Float(v as f32)),
        (RTValue::Float(v), Type::Int) => Ok(RTValue::Int(v as i32)),

        (value, _) => Err(RTError::InvalidOp(value, op.clone())),
      },
      _ => Err(RTError::UnknownOp(op.clone())),
    }
  }

  fn eval_cond(&mut self, cond: &Cond) -> Result<bool> {
    let (lhs, rhs) = match cond {
      Cond::LT(lhs, rhs) => (lhs, rhs),
      Cond::GT(lhs, rhs) => (lhs, rhs),
      Cond::LE(lhs, rhs) => (lhs, rhs),
      Cond::GE(lhs, rhs) => (lhs, rhs),
      Cond::EQ(lhs, rhs) => (lhs, rhs),
      Cond::NE(lhs, rhs) => (lhs, rhs),
      Cond::Or(lhs, rhs) => {
        let l = self.eval_cond(lhs)?;
        let r = self.eval_cond(rhs)?;
        return Ok(l || r);
      }
    };
    let l = self.eval_item(lhs)?;
    let r = self.eval_item(rhs)?;
    let ord = match (l, r) {
      (RTValue::Int(l), RTValue::Int(r)) => l.cmp(&r),
      (l, r) => return Err(RTError::CannotCompare(l, r)),
    };

    Ok(match cond {
      Cond::LT(_, _) => ord.is_lt(),
      Cond::GT(_, _) => ord.is_gt(),
      Cond::LE(_, _) => ord.is_le(),
      Cond::GE(_, _) => ord.is_ge(),
      Cond::EQ(_, _) => ord.is_eq(),
      Cond::NE(_, _) => ord.is_ne(),
      _ => unreachable!(),
    })
  }
}

impl Jar<'_> {
  /// Resolves somthing into a literal int or float.
  pub fn resolve(&mut self, item: &Item) -> Result<RTValue> {
    let mut rt = Runtime::new(self);
    let this = Rc::new(RefCell::new(Object::empty()));
    let mut ctx = Context { runtime: &mut rt, this: &this, vars: vec![] };
    ctx.eval_item(item)
  }

  #[allow(unused)]
  pub fn resolve_value(&mut self, value: &Value) -> Result<RTValue> {
    let mut rt = Runtime::new(self);
    let this = Rc::new(RefCell::new(Object::empty()));
    let mut ctx = Context { runtime: &mut rt, this: &this, vars: vec![] };
    ctx.eval_value(value)
  }
}
