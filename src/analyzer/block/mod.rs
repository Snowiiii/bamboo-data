use super::{tag::TagsDef, Jar};
use anyhow::Result;
use serde::Serialize;

mod new;
mod old;

#[derive(Debug, Clone, Serialize)]
pub struct BlockDef {
  pub blocks: Vec<Block>,
}

#[derive(Debug, Clone, Default, Serialize)]
pub struct Block {
  /// The id of the block.
  pub id:           u32,
  /// The name id, used everywhere imporant.
  pub name:         String,
  /// The name used in lang files.
  unlocalized_name: String,
  /// The full class of the block.
  class:            String,
  /// The field this was set to in modern versions. Empty for old versions.
  field_name:       String,

  /// The enum name of the material.
  material:  String,
  /// The enum name of the map color. Defaults to the map color of the material.
  map_color: String,

  /// The explosion resistance of the block.
  resistance: f32,
  /// The time it takes to mine this block.
  hardness:   f32,

  /// The amount of light this block emits. Will be a number from 0 to 15. This
  /// is zero for most blocks, but will be set for things like torches.
  luminance:    u8,
  /// The slipperiness factor. If set to 0, then this is a normal block.
  /// Otherwise, this is some factor used for ice. Currently, it is always 0.98
  /// for ice, and 0 for everything else.
  slipperiness: f32,

  /// Set when this block doesn't have a hitbox.
  no_collision: bool,
  /// Enum variant of the sound this block makes when walked on.
  sound_type:   String,

  /// All the tags that this block has. Empty on old versions.
  tags: Vec<String>,

  /// A list of items this block drops.
  drops: Vec<ItemDrop>,

  /// A list of all the properties on this block. If the states are empty, there
  /// is a single valid state for this block, which has no properties. See the
  /// [`State`] docs for more.
  properties: Vec<Prop>,
}

#[derive(Debug, Clone, Serialize)]
pub struct ItemDrop {
  /// The item name of this drop.
  item: String,
  /// The minimum amount that can be dropped (inclusive).
  min:  i32,
  /// The maximum amount that can be dropped (inclusive).
  max:  i32,
}

#[derive(Debug, Clone, Serialize)]
pub struct Prop {
  /// The name of this property. This will be something like `rotation` or
  /// `waterlogged`, for example.
  name: String,

  /// The possible values of this state.
  kind: PropKind,

  /// The default index into the `kind`.
  ///
  /// This will never be `None` when we finish generating data.
  default: Option<PropValue>,
}

#[derive(Debug, Clone, Serialize)]
pub enum PropKind {
  /// A boolean property. This can either be `true` or `false`.
  Bool,
  /// An enum property. This can be any of the given values.
  Enum(Vec<String>),
  /// A number property. This can be anything from `min..=max`, where `max` is
  /// the inclusive end of the range. The start is normally zero, but can
  /// sometimes be one.
  Int { min: u32, max: u32 },
}

#[derive(Debug, Clone, Serialize)]
#[serde(untagged)]
pub enum PropValue {
  /// A boolean property. This can either be `true` or `false`.
  Bool(bool),
  /// An enum property. This can be any of the given values.
  Enum(String),
  /// A number property. This can be anything from `min..=max`, where `max` is
  /// the inclusive end of the range. The start is normally zero, but can
  /// sometimes be one.
  Int(u32),
}

pub fn generate(jar: &mut Jar, tags: &TagsDef) -> Result<BlockDef> {
  if jar.ver().is_old() {
    old::generate(jar)
  } else {
    let mut def = new::generate(jar)?;
    for block in &mut def.blocks {
      block.tags = tags.get("block").get(&block.name);
    }
    Ok(def)
  }
}

impl ItemDrop {
  pub fn new(item: String) -> Self {
    ItemDrop { item, min: 1, max: 1 }
  }
  pub fn with_amount(mut self, amount: i32) -> Self {
    self.min = amount;
    self.max = amount;
    self
  }
  pub fn with_min_max(mut self, min: i32, max: i32) -> Self {
    self.min = min;
    self.max = max;
    self
  }
}

impl PropKind {
  pub fn num_values(&self) -> u32 {
    match self {
      Self::Bool => 2,
      Self::Enum(v) => v.len() as u32,
      Self::Int { min, max } => max - min + 1,
    }
  }
}

impl BlockDef {
  pub fn get(&self, name: &str) -> Option<&Block> {
    self.blocks.iter().find(|&b| b.name == name)
  }
  pub fn get_field(&self, field_name: &str) -> Option<&Block> {
    self.blocks.iter().find(|&b| b.field_name == field_name)
  }
}

impl Block {
  /// Adds a property, only if it is not already present. This is needed for
  /// grass_block, where the superclass also defines a default value for this
  /// property.
  ///
  /// If the property name is already present, then the property value will be
  /// updated with the new value. This is because we read the highest superclass
  /// first, so the second time we call this, it should overwrite the previous
  /// information.
  #[track_caller]
  pub fn add_prop(&mut self, prop: Prop) {
    for p in &mut self.properties {
      if p.name == prop.name {
        panic!("property already exists: {p:?} {prop:?}");
      }
    }
    self.properties.push(prop);
  }

  #[track_caller]
  pub fn update_prop(&mut self, prop: Prop) {
    for p in &mut self.properties {
      if p.name == prop.name {
        if prop.default.is_some() {
          p.default = prop.default;
        }
        return;
      }
    }
    panic!("no prop found: {prop:?}");
  }
}
