use super::{Block, BlockDef, ItemDrop, Jar, Prop, PropKind, PropValue};
use crate::decomp::{deobf_type, Converter, Instr, Item, Op, Type, Value, Vars};
use anyhow::Result;
use noak::reader::{
  attributes::{AttributeContent, RawInstruction},
  cpool::Item as NItem,
  Class as NClass,
};
use std::collections::HashMap;

/// Yay we get to deal with ~the flattening~ as all the zoomers are calling it.
/// This is actually a huge pain.
pub fn generate(jar: &mut Jar) -> Result<BlockDef> {
  let (_, f) = jar.decomp_func("net/minecraft/block/Blocks", "<clinit>")?;

  let mut blocks =
    process(&f.instr.clone(), jar).map_err(|e| e.context("error while parsing blocks: {}"))?;

  let loot_table_class =
    if (jar.ver().maj() == 19 && jar.ver().minor() >= 3) || jar.ver().maj() > 19 {
      "net/minecraft/data/server/loottable/BlockLootTableGenerator"
    } else {
      "net/minecraft/data/server/BlockLootTableGenerator"
    };

  let mut cls = jar.decomp(loot_table_class, |_, _, desc| {
    desc.args.len() == 1 && desc.args[0] == Type::Class("java/util/function/BiConsumer".into())
  })?;
  let (_, _, f) = cls.funcs.pop().unwrap();

  process_loot_table(&mut blocks, &f.instr.clone(), jar)
    .map_err(|e| e.context("error while parsing block"))?;

  validate(&blocks);

  Ok(BlockDef { blocks })
}

fn process_loot_table(blocks: &mut [Block], instr: &[Instr], jar: &mut Jar) -> Result<()> {
  let names: HashMap<_, _> =
    blocks.iter().enumerate().map(|(i, b)| (b.field_name.clone(), i)).collect();

  for i in instr {
    match i.clone() {
      Instr::Expr(e) => {
        if e.initial != Value::this() {
          continue;
        }
        match e.ops.borrow().get(0).unwrap() {
          Op::Call(class, name, _, args) => {
            if (jar.ver().maj() == 19 && jar.ver().minor() >= 3) || jar.ver().maj() > 19 {
              assert_eq!(class, "net/minecraft/data/server/loottable/BlockLootTableGenerator");
            } else {
              assert_eq!(class, "net/minecraft/data/server/BlockLootTableGenerator");
            }
            match name.as_str() {
              "addDrop" => {
                let block_field = block_field_from_static(&args[0]);
                let block_id = names[&block_field];
                match args.len() {
                  // Simple case. This block drops itself.
                  1 => {
                    blocks[block_id].drops.push(ItemDrop::new(blocks[block_id].name.clone()));
                  }
                  // Complex case. The block at args[0] calls the closure at args[1].
                  2 => {
                    if args[1].ops.borrow().is_empty() {
                      match args[1].clone().unwrap_initial() {
                        Value::StaticField(_, _, drop_field) => {
                          let drop_id = names[&drop_field];
                          blocks[block_id].drops.push(ItemDrop::new(blocks[drop_id].name.clone()));
                        }
                        Value::StaticCall(..) => match blocks[block_id].name.as_str() {
                          "chorus_plant" => {
                            blocks[block_id].drops.push(ItemDrop::new("chorus_fruit".into()))
                          }
                          "sculk_shrieker" => {}
                          name => error!("edge case for drop block {name}"),
                        },
                        Value::Closure(_, block) => match &block.instr[0] {
                          Instr::Return(it) => {
                            if it.ops.borrow().is_empty() {
                              match it.clone().unwrap_initial() {
                                Value::StaticCall(class, name, args) => match args.len() {
                                  2 => {
                                    assert_eq!(
                                      class,
                                      "net/minecraft/data/server/BlockLootTableGenerator"
                                    );
                                    if name == "multifaceGrowthDrops" {
                                      // TODO: Figure out what this function
                                      // does.
                                    } else if name == "drops" || name == "dropsWithSilkTouch" {
                                      // TODO: ItemDrops should handle silk
                                      match blocks[block_id].name.as_str() {
                                        "nether_gold_ore" => blocks[block_id].drops.push(
                                          ItemDrop::new("gold_nugget".into()).with_min_max(2, 6),
                                        ),
                                        "bell" => {
                                          blocks[block_id].drops.push(ItemDrop::new("bell".into()))
                                        }
                                        _ => {
                                          if matches!(
                                            args[1].clone().unwrap_initial(),
                                            Value::StaticField(..)
                                          ) {
                                            let drop_field = block_field_from_static(&args[1]);
                                            let drop_id = names[&drop_field];
                                            blocks[block_id]
                                              .drops
                                              .push(ItemDrop::new(blocks[drop_id].name.clone()))
                                          } else {
                                            error!("edge case for block {}", blocks[block_id].name);
                                          }
                                        }
                                      }
                                    } else {
                                      panic!("invalid call {name}");
                                    }
                                  }
                                  3 => {
                                    assert_eq!(
                                      class,
                                      "net/minecraft/data/server/BlockLootTableGenerator"
                                    );
                                    match name.as_str() {
                                      "drops" => {
                                        let drop_field = block_field_from_static(&args[1]);
                                        let drop_id = names[&drop_field];
                                        let drop_amount = drop_amount(&args[2]);
                                        blocks[block_id].drops.push(
                                          ItemDrop::new(blocks[drop_id].name.clone())
                                            .with_amount(drop_amount),
                                        );
                                      }
                                      // TODO: This is used for beds, and beds should only drop if
                                      // on the head part.
                                      "dropsWithProperty" => {
                                        blocks[block_id]
                                          .drops
                                          .push(ItemDrop::new(blocks[block_id].name.clone()));
                                      }
                                      "oakLeavesDrop" => {
                                        // TODO: Differentiate drops for with/without tool
                                        blocks[block_id]
                                          .drops
                                          .push(ItemDrop::new(blocks[block_id].name.clone()));
                                        blocks[block_id].drops.push(ItemDrop::new("apple".into()));
                                        blocks[block_id].drops.push(ItemDrop::new("stick".into()));
                                      }
                                      "leavesDrop" => {
                                        // TODO: Differentiate drops for with/without tool
                                        blocks[block_id]
                                          .drops
                                          .push(ItemDrop::new(blocks[block_id].name.clone()));
                                        blocks[block_id].drops.push(ItemDrop::new("stick".into()));
                                      }
                                      _ => todo!("drop call {name}"),
                                    }
                                  }
                                  _ => todo!(),
                                },
                                _ => todo!(),
                              }
                            } else {
                              // TODO: These blocks generate random drops. We need a way to
                              // represent that in the ItemDrop struct.
                              match blocks[block_id].name.as_str() {
                                "acacia_slab" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                "cocoa" => blocks[block_id].drops.push(
                                  ItemDrop::new(blocks[block_id].name.clone()).with_amount(3),
                                ),
                                "sea_pickle" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                "composter" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                "cave_vines" => {}
                                "candle" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                "beacon" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                "shulker_box" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                "black_banner" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                "player_head" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                "bee_nest" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                "beehive" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                "mangrove_slab" => blocks[block_id]
                                  .drops
                                  .push(ItemDrop::new(blocks[block_id].name.clone())),
                                name => {
                                  error!("edge case for {name}");
                                }
                              }
                            }
                          }
                          _ => todo!(),
                        },
                        Value::MethodRef(_, name) => match name.as_str() {
                          // TODO: Drop based on double slabs/single slabs
                          "slabDrops" => blocks[block_id]
                            .drops
                            .push(ItemDrop::new(blocks[block_id].name.clone())),
                          // TODO: Drop based on half
                          "addDoorDrop" => blocks[block_id]
                            .drops
                            .push(ItemDrop::new(blocks[block_id].name.clone())),
                          // TODO: Handle whatever glow berries are
                          "glowBerryDrops" => blocks[block_id]
                            .drops
                            .push(ItemDrop::new(blocks[block_id].name.clone())),
                          // TODO: Candles
                          "candleDrops" => blocks[block_id]
                            .drops
                            .push(ItemDrop::new(blocks[block_id].name.clone())),
                          // TODO: Container tile entity drops
                          "nameableContainerDrops" => blocks[block_id]
                            .drops
                            .push(ItemDrop::new(blocks[block_id].name.clone())),
                          // TODO: This requires setting NBT of the dropped item.
                          "shulkerBoxDrops" => blocks[block_id]
                            .drops
                            .push(ItemDrop::new(blocks[block_id].name.clone())),
                          // TODO: This requires setting NBT of the dropped item
                          "bannerDrops" => blocks[block_id]
                            .drops
                            .push(ItemDrop::new(blocks[block_id].name.clone())),
                          // TODO: Only drop on bottom half of door.
                          "doorDrops" => blocks[block_id]
                            .drops
                            .push(ItemDrop::new(blocks[block_id].name.clone())),
                          "drops" => blocks[block_id]
                            .drops
                            .push(ItemDrop::new(blocks[block_id].name.clone())),
                          _ => todo!("method ref {name}"),
                        },
                        v => todo!("drop {v:?}"),
                      }
                    } else {
                      match blocks[block_id].name.as_str() {
                        "tnt" => {
                          blocks[block_id].drops.push(ItemDrop::new(blocks[block_id].name.clone()))
                        }
                        _ => todo!("drop {:?}", args[1]),
                      }
                    }
                  }
                  v => todo!("len {v}"),
                }
              }
              "addPottedPlantDrop" => {
                // TODO: Drop the flower, not the potted flower.
                let block_field = block_field_from_static(&args[0]);
                let block_id = names[&block_field];
                blocks[block_id].drops.push(ItemDrop::new(blocks[block_id].name.clone()));
              }
              // Ignore other calls, as 1.14-1.17 do a lot of other nonsense here, and we only care
              // about latest.
              _ => {}
            }
          }
          _ => todo!(),
        }
      }
      // Theres some logic at the end of this function that we can ignore.
      _ => break,
    }
  }
  Ok(())
}

fn block_field_from_static(expr: &Item) -> String {
  match expr.clone().unwrap_initial() {
    Value::StaticField(class, _, name) => match class.as_str() {
      // TODO: Convert this block to an item.
      "net/minecraft/block/Blocks" => name.clone(),
      "net/minecraft/item/Items" => name.clone(),
      _ => panic!("invalid drop class {class}"),
    },
    e => panic!("not a static field {e:?}"),
  }
}
fn drop_amount(expr: &Item) -> i32 {
  match expr.clone().unwrap_initial() {
    Value::StaticCall(class, _, args)
      if class == "net/minecraft/loot/provider/number/ConstantLootNumberProvider" =>
    {
      args[0].clone().unwrap_initial().unwrap_float() as i32
    }
    Value::StaticCall(class, _, args) if class == "net/minecraft/loot/ConstantLootTableRange" => {
      args[0].clone().unwrap_initial().unwrap_int() as i32
    }
    e => panic!("not a static call {e:?}"),
  }
}

fn process(instr: &[Instr], jar: &mut Jar) -> Result<Vec<Block>> {
  let properties = read_properties(jar)?;

  let mut out = vec![];
  let mut vars = Vars::new(&[]);
  let mut current_id = 0;
  for i in instr {
    match i.clone() {
      Instr::SetField(field_name, it) => {
        match it.unwrap_initial() {
          Value::StaticCall(_class, name, args) => {
            if name == "register" {
              let name = args[0].clone().unwrap_initial().unwrap_string();
              let value = args[1].clone();
              let (mut class, b) = match value.initial {
                Value::Class(ref name) => (name.clone(), None),
                Value::Var(id) => (vars.load_mut(id).initial.clone().unwrap_class(), None),
                Value::StaticCall(_class, create_name, args) => {
                  let name = name.clone();
                  let b = match create_name.as_str() {
                    "createStoneButtonBlock" => Block {
                      id: current_id,
                      name,
                      material: "STONE".into(),
                      hardness: 0.5,
                      resistance: 0.0,
                      sound_type: "STONE".into(),
                      class: "net/minecraft/block/ButtonBlock".into(),
                      ..Default::default()
                    },
                    "createWoodenButtonBlock" => Block {
                      id: current_id,
                      name,
                      material: "WOOD".into(),
                      hardness: 0.5,
                      resistance: 0.0,
                      sound_type: "WOOD".into(),
                      class: "net/minecraft/block/ButtonBlock".into(),
                      ..Default::default()
                    },
                    "createBambooBlock" => Block {
                      id: current_id,
                      name,
                      material: "WOOD".into(),
                      hardness: 2.0,
                      resistance: 2.0,
                      sound_type: "WOOD".into(),
                      class: "net/minecraft/block/PillarBlock".into(),
                      ..Default::default()
                    },
                    "createLogBlock" => {
                      assert!(args.len() == 2, "unexpected args {:?}", args);
                      Block {
                        id: current_id,
                        name,
                        map_color: args[0].clone().unwrap_initial().unwrap_static_field().2,
                        material: "WOOD".into(),
                        hardness: 2.0,
                        resistance: 2.0,
                        sound_type: "WOOD".into(),
                        class: "net/minecraft/block/PillarBlock".into(),
                        ..Default::default()
                      }
                    }
                    "createLeavesBlock" => {
                      // 1.16.5 doesn't take an argument
                      assert!(args.len() == 1 || args.is_empty(), "unexpected args {:?}", args);
                      Block {
                        id: current_id,
                        name,
                        // TODO: Map color based on the leaves material
                        material: "LEAVES".into(),
                        hardness: 0.2,
                        resistance: 0.2,
                        sound_type: args
                          .get(0)
                          .map(|s| s.clone().unwrap_initial().unwrap_static_field().2)
                          // 1.16.5 doesn't take an argument, and the sound type is always GRASS.
                          .unwrap_or_else(|| "GRASS".into()),
                        class: "net/minecraft/block/LeavesBlock".into(),
                        ..Default::default()
                      }
                    }
                    "createBedBlock" => {
                      assert!(args.len() == 1, "unexpected args {:?}", args);
                      Block {
                        id: current_id,
                        name,
                        // TODO: Map color based on args[0], which is a dye color
                        material: "WOOL".into(),
                        hardness: 0.2,
                        resistance: 0.2,
                        sound_type: "WOOD".into(),
                        class: "net/minecraft/block/BedBlock".into(),
                        ..Default::default()
                      }
                    }
                    "createPistonBlock" => {
                      assert!(args.len() == 1, "unexpected args {:?}", args);
                      Block {
                        id: current_id,
                        name,
                        // TODO: Map color based on material
                        material: "PISTON".into(),
                        hardness: 1.5,
                        resistance: 1.5,
                        class: "net/minecraft/block/PistonBlock".into(),
                        ..Default::default()
                      }
                    }
                    "createStainedGlassBlock" => {
                      assert!(args.len() == 1, "unexpected args {:?}", args);
                      Block {
                        id: current_id,
                        name,
                        // TODO: Map color based on args[0], which is a dye color
                        material: "GLASS".into(),
                        hardness: 0.3,
                        resistance: 0.3,
                        sound_type: "GLASS".into(),
                        class: "net/minecraft/block/StainedGlassBlock".into(),
                        ..Default::default()
                      }
                    }
                    "createShulkerBoxBlock" => {
                      assert!(args.len() == 2, "unexpected args {:?}", args);
                      let mut b = Block {
                        id: current_id,
                        name,
                        hardness: 2.0,
                        resistance: 2.0,
                        class: "net/minecraft/block/ShulkerBoxBlock".into(),
                        ..Default::default()
                      };
                      add_props(&mut b, args[1].clone());
                      b
                    }
                    "createNetherStemBlock" => {
                      assert!(args.len() == 1, "unexpected args {:?}", args);
                      Block {
                        id: current_id,
                        name,
                        map_color: args[0].clone().unwrap_initial().unwrap_static_field().2,
                        material: "NETHER_WOOD".into(),
                        hardness: 2.0,
                        resistance: 2.0,
                        sound_type: "NETHER_STEM".into(),
                        class: "net/minecraft/block/PillarBlock".into(),
                        ..Default::default()
                      }
                    }
                    "createFlowerPotBlock" => {
                      assert!(args.len() == 2, "unexpected args {:?}", args);
                      Block {
                        id: current_id,
                        name,
                        hardness: 2.0,
                        resistance: 2.0,
                        class: "net/minecraft/block/FlowerPotBlock".into(),
                        ..Default::default()
                      }
                    }
                    "createCandleBlock" => {
                      assert!(args.len() == 1, "unexpected args {:?}", args);
                      Block {
                        id: current_id,
                        map_color: args[0].clone().unwrap_initial().unwrap_static_field().2,
                        name,
                        hardness: 2.0,
                        resistance: 2.0,
                        class: "net/minecraft/block/CandleBlock".into(),
                        ..Default::default()
                      }
                    }
                    _ => panic!("unexpected {} {:?}", create_name, args),
                  };
                  (b.class.clone(), Some(b))
                }
                v => panic!("unexpected {:?}", v),
              };
              let mut classes = vec![];
              while class != "java/lang/Object" {
                classes.push(class.clone());
                class = jar.deobf_super(&class)?;
              }
              classes.reverse();
              let mut b = b.unwrap_or(Block {
                id: current_id,
                name,
                class: classes.last().unwrap().clone(),
                ..Default::default()
              });
              b.field_name = field_name;
              // These are all of the properties for this block, in order.
              let field_to_prop_field = read_field_to_prop_field(jar, &classes.last().unwrap())?;
              let all_property_fields =
                read_append_props(jar, &classes.last().unwrap(), &field_to_prop_field)?;
              for field in &all_property_fields {
                b.add_prop(properties.get(field).unwrap().clone());
              }
              // Call first, as the constructor is run before functions on the block.
              for c in classes {
                add_default_props(&mut b, jar, &c, &properties)?;
              }
              if !value.ops.borrow().is_empty() {
                let (class, name, _, args) = value.ops.borrow()[0].clone().unwrap_call();
                add_props(&mut b, Item::new(Value::StaticCall(class, name, args)));
              }
              process_id(&mut current_id, &b);
              out.push(b);
            }
          }
          v => panic!("unexpected value {:?}", v),
        }
      }
      Instr::Assign(var, value) => {
        vars.store(var, value.clone());
      }
      _ => {}
    }
  }
  Ok(out)
}

/// Updates the id based on the properties of the given block.
fn process_id(id: &mut u32, b: &Block) {
  let mut total = 1;
  for p in &b.properties {
    total *= p.kind.num_values();
  }
  *id += total;
}

/// Reads the given value, and parses what those functions mean. This parses
/// something like `new BlockStone().setResistance(10.0).setHardness(5)`. For
/// each call it finds, it will override whatever is already stored in the block
/// for that property.
fn add_props(b: &mut Block, val: Item) {
  let (_, name, args) = val.clone().unwrap_initial().unwrap_static_call();
  let val = if name == "of" {
    val
  } else {
    assert_eq!(name, "<init>", "block arg should be AbstractBlock.Settings.<init>");
    let mut val = None;
    for a in args {
      if matches!(a.initial, Value::StaticCall(_, ref name, _) if name == "of" || name == "copy") {
        val = Some(a);
        break;
      }
    }
    val.unwrap()
  };

  match &val.initial {
    Value::StaticCall(_, name, args) if name == "of" => {
      if args.len() == 1 {
        // TODO: Map color based on this material
        b.material = args[0].clone().unwrap_initial().unwrap_static_field().2;
      } else if args.len() == 2 {
        b.material = args[0].clone().unwrap_initial().unwrap_static_field().2;
        if matches!(args[1].initial, Value::StaticField(..)) {
          if args[1].ops.borrow().is_empty() {
            b.map_color = args[1].clone().unwrap_initial().unwrap_static_field().2;
          } else {
            // TODO: This is a field lookup of the material on another block.
          }
        } else {
          // TODO: This is a field lookup of the material on another block.
        }
      }
    }
    // TODO: This copies properties from another block. Need to figure this out.
    Value::StaticCall(_, name, _args) if name == "copy" => {}
    _ => {
      panic!("unexpected initial value {:?}", val.initial);
    }
  }
  for op in val.ops.borrow().clone().into_iter() {
    match op {
      Op::Call(_, name, _, args) => match name.as_str() {
        "strength" => {
          assert!(args.len() == 1 || args.len() == 2, "unexpected args for strength {:?}", args);
          if args.len() == 1 {
            b.hardness = args[0].clone().unwrap_initial().unwrap_float();
            b.resistance = b.hardness;
          } else {
            b.hardness = args[0].clone().unwrap_initial().unwrap_float();
            b.resistance = args[1].clone().unwrap_initial().unwrap_float();
          }
        }
        "breakInstantly" => {
          assert!(args.is_empty(), "unexpected args for breakInstantly {:?}", args);
          b.hardness = 0.0;
          b.resistance = 0.0;
        }
        "noCollision" => {
          assert!(args.is_empty(), "unexpected args for noCollision {:?}", args);
          b.no_collision = true;
        }
        "sounds" => {
          assert!(args.len() == 1, "unexpected args for sounds {:?}", args);
          b.sound_type = args[0].clone().unwrap_initial().unwrap_static_field().2;
        }
        "dropsLike" => {
          assert!(args.len() == 1, "unexpected args for dropsLike {:?}", args);
          // TODO: Lookup drop info for other block
        }
        "ticksRandomly" => {
          assert!(args.is_empty(), "unexpected args for ticksRandomly {:?}", args);
          // TODO: Maybe store this? I don't really see the need to keep this
          // around. Random ticking is different for every block, so it already
          // needs to be implemented manually.
        }
        "hasDynamicBounds" | "dynamicBounds" => {
          assert!(args.is_empty(), "unexpected args for dynamicBounds {:?}", args);
          // TODO: Maybe store this? I don't really see the need to keep this
          // around. I also don't know what this does.
        }
        "slipperiness" => {
          assert!(args.len() == 1, "unexpected args for slipperiness {:?}", args);
          b.slipperiness = args[0].clone().unwrap_initial().unwrap_float();
        }
        "dropsNothing" => {
          assert!(args.is_empty(), "unexpected args for dropsNothing {:?}", args);
          b.drops.clear();
        }
        "lightLevel" | "luminance" => {
          assert!(args.len() == 1, "unexpected args for luminance {:?}", args);
          match args[0].clone().unwrap_initial() {
            Value::Lit(v) => b.luminance = v.try_into().unwrap(),
            Value::StaticCall(_, name, _args) if name == "applyAsInt" => {
              // TODO: Parse once we can read InvokeDynamic calls.
            }
            Value::StaticCall(_, name, args)
              if name == "createLightLevelFromBlockState"
                || name == "createLightLevelFromLitBlockState" =>
            {
              assert!(
                args.len() == 1,
                "unexpected args for createLightLevelFromLitBlockState {:?}",
                args
              );
              b.luminance = args[0].clone().unwrap_initial().unwrap_int().try_into().unwrap();
            }
            Value::StaticCall(_, name, args) if name == "getLuminanceSupplier" => {
              assert!(args.len() == 1, "unexpected args for getLuminanceSupplier {:?}", args);
              b.luminance = args[0].clone().unwrap_initial().unwrap_int().try_into().unwrap();
            }
            Value::MethodRef(_, name) if b.name == "shroomlight" && name == "h" => {
              b.luminance = 15;
            }
            Value::MethodRef(_, name) if b.name == "small_amethyst_bud" && name == "b" => {
              b.luminance = 1;
            }
            Value::MethodRef(_, name) if b.name == "medium_amethyst_bud" && name == "c" => {
              b.luminance = 2;
            }
            Value::Closure(args, i) if args.is_empty() => {
              // The one exception we have to this is for sea pickles, whose closure looks
              // like this:
              //
              // ```
              // |state| if SeaPickleBlock.isDry(state) {
              //   0
              // } else {
              //   3 + 3 * state.get(SeaPickleBlock.PICKLES)
              // }
              // ```
              //
              // Plus, I can't decompile that correctly.
              if i.instr.len() == 1 {
                let ret = i.instr[0].clone().unwrap_return();
                match ret.initial {
                  Value::Lit(v) => b.luminance = v.try_into().unwrap(),
                  // For respawn anchor
                  Value::StaticCall(_, name, _) if name == "getLightLevel" => b.luminance = 15,
                  _ => panic!(),
                }
              }
            }
            Value::StaticField(class, _ty, val)
              if (class == "net/minecraft/block/CandleBlock"
                || class == "net/minecraft/block/LightBlock")
                && val == "STATE_TO_LUMINANCE" =>
            {
              // TODO: Store luminance so that we can parse this
              b.luminance = 0;
            }
            _ => panic!("(block {}): unexpected args for luminance {:?}", b.name, args),
          }
        }
        _ => {} //  name => warn!("unknown function on block: {}", name),
      },
      _ => {}
    }
  }
}

/// This uses the class of the given block, and reads that from the zip archive,
/// and reads the rest of the properties from the constructor. This includes
/// block material, more properties, etc.
///
/// This uses the `class` property of the block for the class to read. It will
/// then override any existing properties with any calls it finds in the
/// constructor.
fn add_default_props(
  b: &mut Block,
  jar: &mut Jar,
  class_name: &str,
  properties: &HashMap<String, Prop>,
) -> Result<()> {
  if jar.ver().maj() < 19 && b.name == "glow_lichen" {
    // Lichen is stupid. It extends from AbstractLichenBlock (already dumb), and
    // then iterates over an array called `DIRECTIONS` to get all these properties:
    b.properties.clear();
    b.add_prop(Prop {
      name:    "down".into(),
      kind:    PropKind::Bool,
      default: Some(PropValue::Bool(false)),
    });
    b.add_prop(Prop {
      name:    "up".into(),
      kind:    PropKind::Bool,
      default: Some(PropValue::Bool(false)),
    });
    b.add_prop(Prop {
      name:    "north".into(),
      kind:    PropKind::Bool,
      default: Some(PropValue::Bool(false)),
    });
    b.add_prop(Prop {
      name:    "south".into(),
      kind:    PropKind::Bool,
      default: Some(PropValue::Bool(false)),
    });
    b.add_prop(Prop {
      name:    "west".into(),
      kind:    PropKind::Bool,
      default: Some(PropValue::Bool(false)),
    });
    b.add_prop(Prop {
      name:    "east".into(),
      kind:    PropKind::Bool,
      default: Some(PropValue::Bool(false)),
    });
    b.add_prop(Prop {
      name:    "waterlogged".into(),
      kind:    PropKind::Bool,
      default: Some(PropValue::Bool(false)),
    });
    return Ok(());
  }
  // This initializes properties with a for loop, so hardcoding it is a lot
  // simpler.
  if b.name == "chiseled_bookshelf" {
    b.update_prop(Prop {
      name:    "facing".into(),
      kind:    PropKind::Enum(
        // ORDER MATTERS. This is the same as HORIZONTAL_FACING
        ["north", "south", "west", "east"].into_iter().map(|s| s.into()).collect(),
      ),
      default: Some(PropValue::Enum("north".into())),
    });
    if b.properties.iter().any(|p| p.name == "slot_0_occupied") {
      b.update_prop(Prop {
        name:    "slot_0_occupied".into(),
        kind:    PropKind::Bool,
        default: Some(PropValue::Bool(false)),
      });
      b.update_prop(Prop {
        name:    "slot_1_occupied".into(),
        kind:    PropKind::Bool,
        default: Some(PropValue::Bool(false)),
      });
      b.update_prop(Prop {
        name:    "slot_2_occupied".into(),
        kind:    PropKind::Bool,
        default: Some(PropValue::Bool(false)),
      });
      b.update_prop(Prop {
        name:    "slot_3_occupied".into(),
        kind:    PropKind::Bool,
        default: Some(PropValue::Bool(false)),
      });
      b.update_prop(Prop {
        name:    "slot_4_occupied".into(),
        kind:    PropKind::Bool,
        default: Some(PropValue::Bool(false)),
      });
      b.update_prop(Prop {
        name:    "slot_5_occupied".into(),
        kind:    PropKind::Bool,
        default: Some(PropValue::Bool(false)),
      });
    } else {
      // handle snapshot correctly
      b.update_prop(Prop {
        name:    "books_stored".into(),
        kind:    PropKind::Int { min: 0, max: 6 },
        default: Some(PropValue::Int(0)),
      });
    }
    if b.properties.iter().any(|b| b.name == "last_interaction_book_slot") {
      b.update_prop(Prop {
        name:    "last_interaction_book_slot".into(),
        kind:    PropKind::Int { min: 0, max: 6 },
        default: Some(PropValue::Int(0)),
      });
    }
    return Ok(());
  }

  let (_, f) = jar.decomp_func(class_name, "<init>")?;

  for i in f.instr.clone() {
    match i {
      Instr::Super(args) => {
        if !args.is_empty() {
          match args[0].clone().initial {
            Value::StaticField(class, _ty, val) => {
              if class == "net/minecraft/block/material/Material" {
                b.material = val;
              }
            }
            _ => {}
          }
        }
      }
      Instr::Expr(e)
        if e.initial == Value::Var(0)
          && matches!(e.ops.borrow().get(0), Some(Op::Call(_, name, _, _)) if name == "setDefaultState") =>
      {
        let (_, _, _, args) = e.ops.borrow()[0].clone().unwrap_call();
        assert!(args.len() == 1, "unexpected args for setDefaultState {:?}", args);
        let field = &args[0];
        // TODO: Parse AbstractLichenBlock.withAllDirections()
        if field.initial == Value::Var(0) || matches!(field.initial, Value::StaticCall(..)) {
          continue;
        }
        assert!(
          field.initial == Value::Field(Item::new(Value::Var(0)).into(), "stateManager".into())
        );
        {
          let (class_name, name, _, args) = field.ops.borrow()[0].clone().unwrap_call();
          assert_eq!(class_name, "net/minecraft/state/StateManager");
          assert_eq!(name, "getDefaultState");
          assert_eq!(args, vec![]);
        }
        for op in field.ops.borrow().iter().skip(1) {
          if let Op::Call(_, name, _, args) = op {
            assert_eq!(name, "with");
            assert!(args.len() == 2, "unexpected args {:?}", args);
            if args[0].initial == Value::Var(0) {
              // TODO: This is for crop blocks, and they call `this.getAgeProperty()`.
              continue;
            }
            if !args[0].ops.borrow().is_empty() {
              // Edge case for brewing stands, which iterate over bottles. We
              // handle this edge case elsewhere.
            } else {
              let (cls, _, field) = args[0].clone().unwrap_initial().unwrap_static_field();
              let name = find_prop(jar, &cls, &field).unwrap().unwrap();
              let mut prop = properties
                .get(&name)
                .unwrap_or_else(|| {
                  panic!("could not find property for key {} for class {}", name, class_name)
                })
                .clone();
              prop.default = Some(match args[1].clone().unwrap_initial() {
                Value::StaticCall(class, name, args)
                  if class == "java/lang/Boolean" && name == "valueOf" =>
                {
                  PropValue::Bool(args[0].clone().unwrap_initial().unwrap_int() != 0)
                }
                Value::StaticCall(class, name, args)
                  if class == "java/lang/Integer" && name == "valueOf" =>
                {
                  PropValue::Int(args[0].clone().unwrap_initial().unwrap_int() as u32)
                }
                Value::StaticField(_class, _ty, name) => PropValue::Enum(name.to_lowercase()),
                v => unimplemented!("value {:?}", v),
              });
              b.update_prop(prop);
            }
          }
        }
      }
      _ => {}
    }
  }

  // Almost all properties are listed in the constructor, which is handled above.
  // For the properties not listed, we must add them through the appendProperties
  // function.
  //
  // The all_property_fields looks for superclasses already, so we don't want to
  // run this when this function is being called for superclasses.
  /*
  if class_name == b.class {
    for field in all_property_fields {
      let name = match find_prop(jar, class_name, &field).unwrap() {
        Some(prop) => prop,
        None => continue,
      };
      if name == "FACING_PROPERTIES" {
        continue;
      }
      b.add_prop(
        properties
          .get(&name)
          .unwrap_or_else(|| {
            panic!("could not find property for key {} for class {}", name, class_name)
          })
          .clone(),
      )
    }
  }
  */

  for prop in &mut b.properties {
    if prop.default.is_none() {
      prop.default = Some(match prop.name.as_str() {
        "axis" => PropValue::Enum("y".into()),
        "facing" => PropValue::Enum("north".into()),
        "unstable" => PropValue::Bool(false),
        "age" => PropValue::Int(0),
        "lit" => PropValue::Bool(false),
        "has_bottle_0" | "has_bottle_1" | "has_bottle_2" => PropValue::Bool(false),
        "waterlogged" => PropValue::Bool(false),
        "type" => PropValue::Enum("bottom".into()),
        "mode" => PropValue::Enum("data".into()),

        "up" => PropValue::Bool(false),
        "down" => PropValue::Bool(false),
        "west" => PropValue::Bool(false),
        "east" => PropValue::Bool(false),
        "north" => PropValue::Bool(false),
        "south" => PropValue::Bool(false),
        /*
        name => unimplemented!("unknown name {} on block {}", name, b.name),
        */
        _ => continue,
      });
    }
  }

  Ok(())
}

fn read_field_to_prop_field(jar: &mut Jar, class_name: &str) -> Result<HashMap<String, String>> {
  // Some blocks, such as crops, use property names like `DISTANCE`, which is
  // assigned to `Properties.DISTANCE_1_7`. This map converts `DISTANCE` to
  // `DISTANCE_1_7`.
  let mut field_to_prop_field: HashMap<String, String> = HashMap::new();
  let mut classes = vec![class_name.to_string()];
  while let Some(class) = classes.pop() {
    if class.starts_with("java/lang") {
      continue;
    }
    let block = match jar.decomp_func_opt(&class, "<clinit>")? {
      Some((_, block)) => block.clone(),
      _ => {
        classes.push(jar.deobf_super(&class)?);
        classes.extend(jar.deobf_implements(&class)?.into_iter());
        continue;
      }
    };
    for instr in &block.instr {
      match instr {
        Instr::SetField(this_field, value) if value.ops.borrow().is_empty() => match &value.initial
        {
          Value::StaticField(cls, ty, prop_field) => {
            if !matches!(ty, Type::Class(name) if matches!(name.as_str(),
              "net/minecraft/state/property/IntProperty"
              | "net/minecraft/state/property/EnumProperty"
              | "net/minecraft/state/property/BooleanProperty"
              | "net/minecraft/state/property/DirectionProperty")
            ) {
              continue;
            }
            // We read the tree in reverse (we read the lowest class first). This means, if
            // a parent has the same field, it will already be in the map, so we need to
            // make sure not to override it.
            if !field_to_prop_field.contains_key(this_field) {
              match cls.as_str() {
                "net/minecraft/state/property/Properties" => {
                  field_to_prop_field.insert(this_field.clone(), prop_field.clone());
                }
                _ => {
                  let fields = read_field_to_prop_field(jar, cls)?;
                  field_to_prop_field.insert(this_field.clone(), fields[prop_field].clone());
                }
              }
            }
          }
          _ => {}
        },
        _ => {}
      }
    }
    classes.push(jar.deobf_super(&class)?);
    classes.extend(jar.deobf_implements(&class)?.into_iter());
  }

  Ok(field_to_prop_field)
}

fn read_append_props(
  jar: &mut Jar,
  class_name: &str,
  field_to_prop_field: &HashMap<String, String>,
) -> Result<Vec<String>> {
  if class_name == "net/minecraft/block/AbstractLichenBlock" {
    return Ok(vec![]);
  }
  if class_name == "net/minecraft/block/BrewingStandBlock" {
    // Properties.HAS_BOTTLE_* is valid, so this works
    return Ok(vec!["HAS_BOTTLE_0".into(), "HAS_BOTTLE_1".into(), "HAS_BOTTLE_2".into()]);
  }
  if class_name == "net/minecraft/block/MultifaceGrowthBlock" {
    // This is stored in a DIRECTIONS array, which we can't parse.
    return Ok(vec![
      "DOWN".into(),
      "UP".into(),
      "NORTH".into(),
      "SOUTH".into(),
      "WEST".into(),
      "EAST".into(),
    ]);
  }

  // Order matters for appendProperties
  let mut all_property_fields = vec![];
  let bytes = jar.class_file(class_name).to_vec();
  let mut class = NClass::new(&bytes)?;
  let obf_class = &jar.class_rev(class_name);

  for m in class.methods()? {
    let m = m?;
    let pool = class.pool()?;
    let name = jar.func(
      obf_class,
      pool.retrieve(m.name())?.to_str().unwrap(),
      pool.retrieve(m.descriptor())?.to_str().unwrap(),
    )?;
    if name != "appendProperties" {
      continue;
    }
    for a in m.attributes() {
      let a = a?;
      if let AttributeContent::Code(code) = a.read_content(pool)? {
        for instr in code.raw_instructions() {
          let (_idx, instr) = instr?;
          match instr {
            RawInstruction::GetStatic { index } => {
              let stat = pool.retrieve(index)?;
              let class = jar.class(stat.class.name.to_str().unwrap());
              let field = jar.field(
                stat.class.name.to_str().unwrap(),
                stat.name_and_type.name.to_str().unwrap(),
              )?;
              if class == class_name {
                match field.as_str() {
                  "SLOT_OCCUPIED_PROPERTIES" => {
                    all_property_fields.push("SLOT_0_OCCUPIED".into());
                    all_property_fields.push("SLOT_1_OCCUPIED".into());
                    all_property_fields.push("SLOT_2_OCCUPIED".into());
                    all_property_fields.push("SLOT_3_OCCUPIED".into());
                    all_property_fields.push("SLOT_4_OCCUPIED".into());
                    all_property_fields.push("SLOT_5_OCCUPIED".into());
                  }
                  _ => {
                    all_property_fields.push(field_to_prop_field[&field].clone());
                  }
                }
              } else {
                // Maybe someday...
                /*
                dbg!(jar.resolve_value(&Value::StaticField(class, Type::Void, field)).unwrap());
                todo!();
                */
                /*
                let (cls, _, field) = jar
                  .resolve_value(&Value::StaticField(class, Type::Void, field))
                  .unwrap()
                  .unwrap_static_field();
                assert_eq!(cls, "net/minecraft/state/property/Properties");
                all_property_fields.push(field);
                */
                all_property_fields.push(
                  match (class.as_str(), field.as_str()) {
                    ("net/minecraft/block/PillarBlock", "AXIS") => "AXIS",
                    ("net/minecraft/block/HorizontalFacingBlock", "FACING") => "HORIZONTAL_FACING",
                    // Only applies to one snapshot, this is a dummy case
                    ("net/minecraft/state/property/Properties", "LAST_INTERACTION_BOOK_SLOT") => {
                      "LAST_INTERACTION_BOOK_SLOT"
                    }
                    _ => panic!("handle edge case for class {class} field {field}"),
                  }
                  .into(),
                );
              }
            }
            RawInstruction::InvokeSpecial { index } => match pool.get(index)? {
              NItem::MethodRef(index) => {
                let obf_class = pool.retrieve(index.class)?.name.to_str().unwrap();
                let class = jar.class(obf_class);
                let name_and_type = pool.retrieve(index.name_and_type)?;
                let func = jar.func(
                  obf_class,
                  name_and_type.name.to_str().unwrap(),
                  name_and_type.descriptor.to_str().unwrap(),
                )?;
                // Calling super will append properties to the current array. Super is always
                // called before static lookups, so this will create the correct ordering.
                if func == "appendProperties" {
                  all_property_fields.extend(read_append_props(jar, &class, field_to_prop_field)?);
                } else {
                  panic!("unexpected function {}", func);
                }
              }
              _ => unreachable!(),
            },
            _ => {}
          }
        }
      }
    }
  }

  // If we didn't find anything, we look to the superclass. `appendProperties` is
  // a function on `Block`, so we only look for superclasses.
  if all_property_fields.is_empty() {
    let sup = class.super_class_name()?.unwrap().to_str().unwrap();
    if sup != "java/lang/Object" {
      all_property_fields = read_append_props(jar, &jar.class(sup), field_to_prop_field)?;
    }
  }

  Ok(all_property_fields)
}
/// Returns a map of field names (all caps) to properties.
fn read_properties(jar: &mut Jar) -> Result<HashMap<String, Prop>> {
  let (_, f) = jar.decomp_func("net/minecraft/state/property/Properties", "<clinit>")?;
  let mut props = HashMap::new();

  for i in f.instr.clone() {
    match i {
      Instr::SetField(field_name, val) => {
        let (_class, name, args) = val.unwrap_initial().unwrap_static_call();
        assert_eq!(name, "of");
        match args.len() {
          1 => {
            // We got a BooleanProperty.of("name")
            props.insert(
              field_name,
              Prop {
                name:    args[0].clone().unwrap_initial().unwrap_string(),
                kind:    PropKind::Bool,
                default: None,
              },
            );
          }
          2 => {
            match args[1].initial {
              // We got an EnumProperty.of("name", EnumClass.class)
              Value::Class(_) => {
                props.insert(
                  field_name,
                  Prop {
                    name:    args[0].clone().unwrap_initial().unwrap_string(),
                    kind:    PropKind::Enum(
                      enum_variants(jar, &args[1].clone().unwrap_initial().unwrap_class())?
                        .into_iter()
                        .map(|s| s.to_lowercase())
                        .collect(),
                    ),
                    default: None,
                  },
                );
              }
              // We got a DirectionProperty.of("name", Direction.UP, Direction.DOWN, ...)
              Value::Array(_) => {
                props.insert(
                  field_name,
                  Prop {
                    name:    args[0].clone().unwrap_initial().unwrap_string(),
                    kind:    PropKind::Enum(
                      args[1]
                        .ops
                        .borrow()
                        .iter()
                        .map(|o| {
                          o.clone()
                            .unwrap_set()
                            .1
                            .unwrap_initial()
                            .unwrap_static_field()
                            .2
                            .to_lowercase()
                        })
                        .collect(),
                    ),
                    default: None,
                  },
                );
              }
              _ => match field_name.as_str() {
                "HOPPER_FACING" => {
                  props.insert(
                    field_name,
                    Prop {
                      name:    "facing".into(),
                      kind:    PropKind::Enum(
                        // ORDER MATTERS
                        ["north", "east", "south", "west", "down"]
                          .into_iter()
                          .map(|s| s.into())
                          .collect(),
                      ),
                      default: None,
                    },
                  );
                }
                "HORIZONTAL_FACING" => {
                  props.insert(
                    field_name,
                    Prop {
                      name:    "facing".into(),
                      kind:    PropKind::Enum(
                        // ORDER MATTERS
                        ["north", "south", "west", "east"].into_iter().map(|s| s.into()).collect(),
                      ),
                      default: None,
                    },
                  );
                }
                _ => panic!("unknown field {} {:?}", field_name, args),
              },
            }
          }
          3 => {
            match args[1].initial {
              // Int property, with only some enum variants being valid (passed as an array).
              Value::Class(_) => {
                if args[2].ops.borrow().is_empty() {
                  // args[2] is a closure, handle manually
                  let kind = match field_name.as_str() {
                    "STRAIGHT_RAIL_SHAPE" => PropKind::Enum(
                      [
                        "north_south",
                        "east_west",
                        "ascending_east",
                        "ascending_west",
                        "ascending_north",
                        "ascending_south",
                      ]
                      .into_iter()
                      .map(|s| s.into())
                      .collect(),
                    ),
                    _ => panic!("unknown Properties field {}", field_name),
                  };
                  props.insert(
                    field_name,
                    Prop {
                      name: args[0].clone().unwrap_initial().unwrap_string(),
                      kind,
                      default: None,
                    },
                  );
                } else {
                  // args[2] is a list of props
                  props.insert(
                    field_name,
                    Prop {
                      name:    args[0].clone().unwrap_initial().unwrap_string(),
                      kind:    PropKind::Enum(
                        args[2]
                          .ops
                          .borrow()
                          .iter()
                          .map(|o| {
                            o.clone()
                              .unwrap_set()
                              .1
                              .unwrap_initial()
                              .unwrap_static_field()
                              .2
                              .to_lowercase()
                          })
                          .collect(),
                      ),
                      default: None,
                    },
                  );
                }
              }
              // We got an IntProperty.of("name", 0, 24)
              //
              // Where 0 is the min, and 24 is the max.
              Value::Lit(_) => {
                props.insert(
                  field_name,
                  Prop {
                    name:    args[0].clone().unwrap_initial().unwrap_string(),
                    kind:    PropKind::Int {
                      min: jar.resolve(&args[1]).unwrap().unwrap_int().try_into().unwrap(),
                      max: jar.resolve(&args[2]).unwrap().unwrap_int().try_into().unwrap(),
                    },
                    default: None,
                  },
                );
              }
              _ => panic!("unexpected value {:?}", args),
            }
          }
          _ => panic!("unexpected args {:?}", args),
        }
      }
      _ => {}
    }
  }

  for (k, p) in &props {
    if let PropKind::Enum(values) = &p.kind {
      if values.is_empty() {
        panic!("property {} doesn't have any variants", k);
      }
    }
  }

  Ok(props)
}

fn enum_variants(jar: &mut Jar, class_name: &str) -> Result<Vec<String>> {
  let bytes = jar.class_file(class_name).to_vec();
  let mut class = NClass::new(&bytes)?;
  let mut out = vec![];
  let obf_class = class.this_class_name().unwrap().to_str().unwrap();

  for f in class.fields()? {
    let f = f?;
    let pool = class.pool()?;
    let name = pool.retrieve(f.name())?.to_str().unwrap();
    let desc = pool.retrieve(f.descriptor())?.to_str().unwrap();
    if deobf_type(desc, jar) == Type::Class(class_name.into()) {
      out.push(jar.field(obf_class, name)?.to_lowercase());
    }
  }
  Ok(out)
}

/// Looks up the value of the static field `field` on class `class_name`. These
/// should both be deobfuscated names. If it finds a field, it will do one of
/// two things: if the field it finds is on the class `Properties`, it returns
/// that field name. Otherwise, it will recusively call itself for the new
/// field.
///
/// This is used for block properties, where java sucks and they use a bunch of
/// properties in superclasses and other nonsense.
///
/// If this returns Ok(None), then the field doesn't exist. This is used
/// internally when recusively calling to check interfaces. If this does return
/// Ok(None), then something has gone wrong.
fn find_prop(jar: &mut Jar, class_name: &str, field: &str) -> Result<Option<String>> {
  let bytes = jar.class_file(class_name).to_vec();
  let mut class = NClass::new(&bytes)?;

  for m in class.methods()? {
    let m = m?;
    let pool = class.pool()?;
    if pool.retrieve(m.name())?.to_str().unwrap() != "<clinit>" {
      continue;
    }
    for a in m.attributes() {
      let a = a?;
      if let AttributeContent::Code(code) = a.read_content(pool)? {
        let mut prop = None;
        let mut found = false;
        for instr in code.raw_instructions() {
          let (_idx, instr) = instr?;
          match instr {
            RawInstruction::GetStatic { index } => {
              let stat = pool.retrieve(index)?;
              prop = Some((
                stat.class.name.to_str().unwrap(),
                stat.name_and_type.name.to_str().unwrap(),
              ));
            }
            RawInstruction::PutStatic { index } => {
              let stat = pool.retrieve(index)?;
              let class = stat.class.name.to_str().unwrap();
              if jar.field(class, stat.name_and_type.name.to_str().unwrap())? == field {
                found = true;
                break;
              }
            }
            _ => {}
          }
        }
        if !found {
          break;
        }
        let (class, field) = prop.unwrap();
        let field = jar.field(class, field)?;
        let class = jar.class(class);
        if class == "net/minecraft/state/property/Properties" {
          return Ok(Some(field));
        } else {
          return find_prop(jar, &class, &field);
        }
      }
    }
  }
  let sup = class.super_class_name().unwrap().unwrap().to_str().unwrap();
  if sup == "java/lang/Object" {
    // Caller needs to look in interfaces
    Ok(None)
  } else {
    let sup = jar.class(sup);
    if let Some(field) = find_prop(jar, &sup, field)? {
      Ok(Some(field))
    } else {
      for i in class.interfaces()? {
        let pool = class.pool()?;
        let i = pool.retrieve(i?)?.name.to_str().unwrap();
        let i = jar.class(i);
        if let Some(field) = find_prop(jar, &i, field)? {
          return Ok(Some(field));
        }
      }
      Ok(None)
    }
  }
}

fn validate(blocks: &[Block]) {
  for b in blocks {
    for prop in &b.properties {
      macro_rules! fail {
        ( $msg:expr ) => {
          panic!("block: {b:#?}\ninvalid default {prop:#?}: {}", $msg)
        };
      }
      macro_rules! assert {
        ( $cond:expr, $msg:expr ) => {
          if !($cond) {
            fail!($msg);
          }
        };
      }
      match &prop.kind {
        PropKind::Enum(variants) => {
          let def = match &prop.default {
            Some(PropValue::Enum(v)) => v,
            _ => fail!("default is not an enum"),
          };
          assert!(variants.contains(&def), "default isn't within enum");
          assert!(
            variants.iter().all(|v| v.chars().all(|c| c.is_lowercase() || c == '_')),
            "property isn't lowercase"
          );
        }
        PropKind::Int { min, max } => {
          let def = match prop.default {
            Some(PropValue::Int(v)) => v,
            _ => fail!("default is not an int"),
          };
          assert!(def >= *min || def <= *max, "default out of range");
        }
        PropKind::Bool => match prop.default {
          Some(PropValue::Bool(_)) => {}
          _ => fail!("default is not a bool"),
        },
      }
    }
  }
}
