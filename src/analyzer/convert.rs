//! This handles de-obfuscating all of the minecraft jar. This is a complex task
//! to say the least, and requires a lot of manual work. For 1.8-1.12, I am
//! using the mappings provided in mcp. For 1.13+, I am using the yarn mappings.
//!
//! In order to make my life more difficult, mojang has opted to make every
//! class name be a very short sequece of random letters. This is fine, except
//! for the fact that every field/method name in that class overlaps. So, when
//! looking up field `a`, you must know what class it is in, as every class's
//! first field will always be named `a`.
//!
//! This is why the [`Converter`] type is awquard to use. It is simply because
//! you need to know the class related to every field and method.

use super::jar::ClassFiles;
use crate::{decomp, Version};
use noak::reader::{attributes::RawInstruction, cpool, AttributeContent, Class as NClass};
use std::{collections::HashMap, fs, io, path::Path};

#[derive(Debug, Clone, Default)]
pub struct Class {
  pub name:   String,
  /// Method name and descriptor
  funcs:      HashMap<(String, String), String>,
  funcs_rev:  HashMap<String, (String, String)>,
  fields:     HashMap<String, String>,
  fields_rev: HashMap<String, String>,
}

#[derive(Debug, Clone)]
pub struct Converter {
  classes:     HashMap<String, Class>,
  classes_rev: HashMap<String, String>,
}

impl Converter {
  /// Loads mcp mappings from the given path to an extracted mcp download.
  /// `path.join("mcp")` should result in a valid mcp download.
  ///
  /// This is really, really slow, simply because it has to process thousands of
  /// mappings. When I say really really slow, I mean literally hundreds of
  /// milliseconds in debug mode, and like 50 millis in release mode. This
  /// function sucks, and I can't improve it.
  pub fn new_mcp(path: &Path) -> Self {
    let methods_csv = fs::read_to_string(path.join("mcp/conf/methods.csv")).unwrap();
    let mut methods = HashMap::<String, String>::new();
    for row in methods_csv.lines().skip(1) {
      let mut sections = row.split(',');
      let obf = sections.next().unwrap();
      let deobf = sections.next().unwrap();
      methods.insert(obf.into(), deobf.into());
    }

    let fields_csv = fs::read_to_string(path.join("mcp/conf/fields.csv")).unwrap();
    let mut fields = HashMap::<String, String>::new();
    for row in fields_csv.lines().skip(1) {
      let mut sections = row.split(',');
      let obf = sections.next().unwrap();
      let deobf = sections.next().unwrap();
      fields.insert(obf.into(), deobf.into());
    }

    let srg = fs::read_to_string(path.join("mcp/conf/joined.srg")).unwrap();

    // For 1.8, joined.srg has 27,000 lines, and 2500 of those lines are classes.
    // 1/16 seems like a decent low estimate for how big this should be. This ends
    // up giving us a decent performance increase.
    let mut classes = HashMap::with_capacity(srg.lines().count() / 16);
    let mut classes_rev = HashMap::with_capacity(classes.capacity());

    for line in srg.lines() {
      let mut sections = line.split(':');
      let ty = sections.next().unwrap();
      let val = sections.next().unwrap().trim();
      let mut sections = val.split(' ');

      match ty {
        "CL" => {
          let obf: String = sections.next().unwrap().into();
          let deobf: String = sections.next().unwrap().into();
          classes.insert(obf.clone(), Class { name: deobf.clone(), ..Default::default() });
          classes_rev.insert(deobf, obf);
        }
        "FD" => {
          let obf = sections.next().unwrap();
          let deobf = sections.next().unwrap();

          let mut sections = obf.rsplitn(2, '/');
          let obf_field: String = sections.next().unwrap().into(); // This returns something like `a` or `b`
          let obf_class = sections.next().unwrap();

          let mut sections = deobf.rsplitn(2, '/');
          let mut deobf_field: String = sections.next().unwrap().into(); // This returns a unique field name, like `field_12345`
          let _deobf_class = sections.next().unwrap();
          // This converts the field into a human-readable name
          if let Some(f) = fields.get(&deobf_field) {
            deobf_field = f.clone();
          }

          classes.get_mut(obf_class).unwrap().fields.insert(obf_field.clone(), deobf_field.clone());
          classes.get_mut(obf_class).unwrap().fields_rev.insert(deobf_field, obf_field);
        }
        "MD" => {
          let obf = sections.next().unwrap();
          let obf_desc: String = sections.next().unwrap().into();
          let deobf = sections.next().unwrap();
          sections.next().unwrap(); // Deobfuscated descriptor, ignored

          let mut sections = obf.rsplitn(2, '/');
          let obf_meth: String = sections.next().unwrap().into();
          let obf_class = sections.next().unwrap();

          let mut sections = deobf.rsplitn(2, '/');
          let mut deobf_meth: String = sections.next().unwrap().into();
          let _deobf_class = sections.next().unwrap();
          if let Some(m) = methods.get(&deobf_meth) {
            deobf_meth = m.clone();
          }

          classes
            .get_mut(obf_class)
            .unwrap()
            .funcs
            .insert((obf_meth.clone(), obf_desc.clone()), deobf_meth.clone());
          classes.get_mut(obf_class).unwrap().funcs_rev.insert(deobf_meth, (obf_meth, obf_desc));
        }
        _ => {}
      }
    }

    Converter { classes, classes_rev }
  }

  /// Loads yarn mappings from the given yarn path. This should be a path to a
  /// versioned build directory. `path.join("yarn")` should result in a yarn
  /// repo.
  pub fn new_yarn(path: &Path, classes: &mut ClassFiles) -> Self {
    let mut c = Converter { classes: HashMap::new(), classes_rev: HashMap::new() };

    let mut iclasses = HashMap::new();
    let mut ifields = HashMap::new();
    let mut ifuncs = HashMap::new();

    let inter = fs::read_to_string(path.join("intermediary.tiny")).unwrap();
    for line in inter.lines() {
      let mut sections = line.split('\t');
      let ty = sections.next().unwrap();
      match ty {
        "CLASS" => {
          let obf = sections.next().unwrap();
          let deobf = sections.next().unwrap();
          iclasses.insert(deobf, obf);
        }
        "FIELD" => {
          let class = sections.next().unwrap();
          let _sig = sections.next().unwrap();
          let obf = sections.next().unwrap();
          let deobf = sections.next().unwrap();

          ifields.insert((class, deobf), obf);
        }
        "METHOD" => {
          let class = sections.next().unwrap();
          let sig = sections.next().unwrap();
          let obf = sections.next().unwrap();
          let deobf = sections.next().unwrap();

          ifuncs.insert((class, sig, deobf), obf);
        }
        _ => {}
      }
    }

    // We don't want to read the mappings under com/
    c.read_yarn(&path.join("yarn/net"), &iclasses, &ifields, &ifuncs)
      .unwrap_or_else(|e| panic!("could not read yarn mappings: {}", e));
    // NOTE: Yarn works because they generate all the enum mappings. See:
    // https://github.com/FabricMC/stitch/blob/4289c0e26203c973b2ce96e76eb1c87d9e6388df/src/main/java/net/fabricmc/stitch/util/FieldNameFinder.java#L62
    //
    // I have decided ~not~ to do this, so I am going to just wing it, and hope we
    // can figure out which enum is which. This also means we have a bunch of
    // missing mappings for all the enum variants, but that's ok.
    //
    // Correction: I went ahead and did it anyways.
    //
    // This needs to happen afterwards, so that we have the class names for the
    // unknown enums.
    c.read_enum_mappings(classes).expect("could not read enum mappings");
    c
  }

  fn read_yarn(
    &mut self,
    path: &Path,
    iclasses: &HashMap<&str, &str>,
    ifields: &HashMap<(&str, &str), &str>,
    ifuncs: &HashMap<(&str, &str, &str), &str>,
  ) -> io::Result<()> {
    for ent in path.read_dir()? {
      let ent = ent?;
      let path = ent.path();
      if ent.file_type()?.is_dir() {
        self.read_yarn(&path, iclasses, ifields, ifuncs)?;
      } else {
        self.read_mapping(fs::read_to_string(path)?, iclasses, ifields, ifuncs);
      }
    }
    Ok(())
  }

  fn read_mapping(
    &mut self,
    text: String,
    iclasses: &HashMap<&str, &str>,
    ifields: &HashMap<(&str, &str), &str>,
    ifuncs: &HashMap<(&str, &str, &str), &str>,
  ) {
    fn deobf_ty(ty: &mut decomp::Type, iclasses: &HashMap<&str, &str>) {
      match ty {
        decomp::Type::Class(name) => {
          if name.starts_with("net/") && iclasses.contains_key(name.as_str()) {
            *name = iclasses[name.as_str()].into();
          }
        }
        decomp::Type::Array(ty) => deobf_ty(ty, iclasses),
        _ => {}
      }
    }
    fn deobf_sig(sig: &str, iclasses: &HashMap<&str, &str>) -> String {
      let mut desc = decomp::Descriptor::from_desc(sig);
      for a in &mut desc.args {
        deobf_ty(a, iclasses);
      }
      deobf_ty(&mut desc.ret, iclasses);
      desc.to_desc()
    }

    let mut lines = text.lines();
    let first = lines.next().unwrap().trim();
    let mut sections = first.split(' ');
    let ty = sections.next().unwrap();
    let inter_class = sections.next().unwrap();
    let obf_class: String = iclasses.get(inter_class).copied().unwrap_or(inter_class).into();
    let deobf_class = match sections.next() {
      Some(n) => n.into(),
      None => return,
    };
    assert_eq!(ty, "CLASS");
    let mut class = Class { name: deobf_class, ..Default::default() };
    let mut inner_class: Option<(String, String, Class)> = None;

    for mut line in lines {
      let mut indent = 0;
      while line.starts_with('\t') {
        line = &line[1..];
        indent += 1;
      }
      let mut sections = line.split(' ');
      let ty = sections.next().unwrap();
      if indent == 1 {
        if let Some((obf_name, _, inner)) = inner_class.take() {
          self.classes_rev.insert(inner.name.clone(), obf_name.clone());
          self.classes.insert(obf_name, inner);
        }
      }
      match ty {
        "METHOD" => {
          let obf_name: String = sections.next().unwrap().into();
          if obf_name == "<init>" || obf_name == "equals" {
            continue;
          }
          let second: String = sections.next().unwrap().into();
          let deobf_name;
          let sig;
          if let Some(third) = sections.next() {
            deobf_name = second;
            sig = third;
          } else {
            deobf_name = obf_name.clone();
            sig = &second;
          }

          let (obf_class, c) = match &mut inner_class {
            Some(c) => (&c.0, &mut c.2),
            None => (&obf_class, &mut class),
          };
          let sig = deobf_sig(sig, iclasses);
          let obf_name: String = ifuncs
            .get(&(obf_class.as_str(), sig.as_str(), obf_name.as_str()))
            .map(|s| (*s).into())
            .unwrap_or(obf_name);
          c.funcs.insert((obf_name.clone(), sig.clone()), deobf_name.clone());
          c.funcs_rev.insert(deobf_name, (obf_name, sig));
        }
        "FIELD" => {
          let obf_name: String = sections.next().unwrap().into();
          let deobf_name: String = sections.next().unwrap().into();
          // Sometimes this is missing, and we don't care about it.
          // let _sig: String = sections.next().unwrap().into();

          let c = match &mut inner_class {
            Some(c) => &mut c.2,
            None => &mut class,
          };
          let obf_name: String = ifields
            .get(&(obf_class.as_str(), obf_name.as_str()))
            .map(|s| (*s).into())
            .unwrap_or(obf_name);
          c.fields.insert(obf_name.clone(), deobf_name.clone());
          c.fields_rev.insert(deobf_name, obf_name);
        }
        "ARG" => {}
        "CLASS" => {
          let inter_name = sections.next().unwrap();
          let deobf_name = sections.next().unwrap_or(inter_name);
          match indent {
            1 => {
              let obf_name: String =
                iclasses[format!("{}${}", inter_class, inter_name).as_str()].into();
              inner_class = Some((
                obf_name,
                inter_name.into(),
                Class { name: format!("{}${}", class.name(), deobf_name), ..Default::default() },
              ));
            }
            2 => {
              let middle_inter_name = &inner_class.as_ref().unwrap().1;
              let middle_deobf_name = inner_class.as_ref().unwrap().2.name();
              let obf_name: String = iclasses
                [format!("{}${}${}", inter_class, middle_inter_name, inter_name).as_str()]
              .into();
              self.classes_rev.insert(deobf_name.into(), obf_name.clone());
              self.classes.insert(
                obf_name,
                Class {
                  name: format!("{}${}", middle_deobf_name, deobf_name),
                  ..Default::default()
                },
              );
            }
            // TODO: Implement more indents.
            _ => {}
          }
        }
        "COMMENT" => {}
        "" => {}
        _ => panic!("unexpected mapping {}", ty),
      }
    }

    if let Some((obf_name, _, inner)) = inner_class.take() {
      self.classes_rev.insert(inner.name.clone(), obf_name.clone());
      self.classes.insert(obf_name, inner);
    }
    self.classes_rev.insert(class.name.clone(), obf_class.clone());
    self.classes.insert(obf_class, class);
  }

  fn read_enum_mappings(
    &mut self,
    classes: &mut ClassFiles,
  ) -> Result<(), decomp::Error<super::jar::DecompileError>> {
    for c in classes.class_names() {
      if !c.contains('$') || !c.split('$').last().unwrap().chars().any(|c| c.is_alphabetic()) {
        // We want to parse everything in `net.minecraft.block.enums`, and parse
        // `net.minecraft.util.math.Direction`.
        let deobf = self.classes.get(&c).map(|c| c.name()).unwrap_or(c.as_str());
        if !deobf.contains("enums") && !deobf.contains("Direction") {
          continue;
        }
      }
      let mut c = NClass::new(classes.class_file(&c)?)?;
      let class_name = c.this_class_name()?.to_str().unwrap();
      let mut enum_fields: HashMap<&str, String> = HashMap::new();
      for m in c.methods()? {
        let pool = c.pool()?;
        let m = m?;
        if pool.get(m.name())?.content.to_str().unwrap() != "<clinit>" {
          continue;
        }
        for a in m.attributes() {
          if let AttributeContent::Code(code) = a?.read_content(pool)? {
            let mut prev = None;
            let mut recent_str: Option<String> = None;
            for instr in code.raw_instructions() {
              let (_, instr) = instr?;
              if prev.is_none() {
                prev = Some(instr);
                continue;
              }
              // let p = prev.as_ref().unwrap();
              match instr {
                RawInstruction::PutStatic { index } => {
                  let field = pool.retrieve(index)?;
                  if field.class.name.to_str().unwrap() == class_name {
                    if let Some(s) = recent_str.take() {
                      enum_fields.insert(field.name_and_type.name.to_str().unwrap(), s);
                    }
                  }
                }
                RawInstruction::LdC { index } | RawInstruction::LdCW { index } => {
                  match pool.get(index)? {
                    cpool::Item::String(string) => {
                      if recent_str.is_none() {
                        if let Some(s) = pool.get(string.string)?.content.to_str() {
                          recent_str = Some(s.into())
                        }
                      }
                    }
                    _ => {}
                  }
                }
                _ => {}
              }
            }
          }
        }
      }
      let name: String = c.this_class_name()?.to_str().unwrap().into();
      let c = self.classes.entry(name.clone()).or_insert(Class { name, ..Default::default() });
      for (k, v) in enum_fields {
        c.fields.entry(k.into()).or_insert(v);
      }
    }
    Ok(())
  }
}

struct BlankConverter<'a>(Version<'a>);

impl decomp::NotDecodeError for std::convert::Infallible {}

impl decomp::Converter for BlankConverter<'_> {
  type Err = std::convert::Infallible;

  fn has_class_file(&mut self, _name: &str) -> bool {
    false
  }
  fn class_file(&mut self, _name: &str) -> &[u8] {
    panic!("cannot get class file on empty converter");
  }
  fn class(&self, name: &str) -> String {
    name.into()
  }
  fn class_rev(&self, obf: &str) -> String {
    obf.into()
  }
  fn field(&mut self, _class: &str, name: &str) -> Result<String, Self::Err> {
    Ok(name.into())
  }
  fn func(&mut self, _class: &str, _desc: &str, name: &str) -> Result<String, Self::Err> {
    Ok(name.into())
  }
  fn ver(&self) -> Version {
    self.0
  }
}

impl Converter {
  /// Returns true if the given obfuscated name is a valid class name.
  pub fn has_class(&self, name: &str) -> bool {
    self.classes.contains_key(name)
  }
  /// Returns true if the given deobfuscated name maps to an obfuscated name.
  pub fn has_class_rev(&self, obf: &str) -> bool {
    self.classes_rev.contains_key(obf)
  }
  /// Returns the deobfuscated class for the given obfuscated name (like
  /// `abcd`).
  #[track_caller]
  pub fn class(&self, name: &str) -> &Class {
    match self.classes.get(name) {
      Some(c) => c,
      None => {
        if let Some(idx) = name.find('$') {
          let (sname, _num) = name.split_at(idx);
          self
            .classes
            .get(sname)
            .unwrap_or_else(|| panic!("could not find class for de-obfuscated name {}", name))
        } else {
          panic!("could not find class for de-obfuscated name {}", name)
        }
      }
    }
  }

  /// Returns the obfuscated class for the given deobfuscated name (like
  /// `SPacketFoo`)
  #[track_caller]
  pub fn class_rev(&self, name: &str) -> String {
    match self.classes_rev.get(name) {
      Some(s) => s.clone(),
      None => {
        if let Some(idx) = name.find('$') {
          let (sname, num) = name.split_at(idx);
          let deobf = self
            .classes_rev
            .get(sname)
            .unwrap_or_else(|| panic!("could not find class for de-obfuscated name {}", name));
          // split_at includes the '$' in `num`
          format!("{}{}", deobf, num)
        } else {
          /* panic!("could not find class for de-obfuscated name {}", name) */
          // The mapping might be missing, so we just use `name`
          name.into()
        }
      }
    }
  }
}

impl Class {
  pub fn name(&self) -> &str {
    &self.name
  }

  pub fn has_field(&self, name: &str) -> bool {
    self.fields.contains_key(name)
  }
  pub fn field(&self, name: &str) -> Option<&str> {
    Some(self.fields.get(name)?.as_str())
  }

  pub fn has_func(&self, name: &str, desc: &str) -> bool {
    self.funcs.contains_key(&(name.into(), desc.into()))
  }
  #[track_caller]
  pub fn func(&self, name: &str, desc: &str) -> Option<&str> {
    Some(self.funcs.get(&(name.into(), desc.into()))?.as_str())
  }
}
