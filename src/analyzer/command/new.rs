use super::{super::Jar, Arg, CommandDef};
use crate::decomp::{Instr, Value};
use anyhow::Result;

pub fn process(jar: &mut Jar) -> Result<CommandDef> {
  let mut args = vec![];

  let brigadier_args = "net/minecraft/command/argument/BrigadierArgumentTypes";
  if jar.has_class(brigadier_args) {
    let class = jar.decomp(brigadier_args, |_, name, _| name == "register")?;

    let (_, _, block) = class.funcs.first().unwrap();
    generate(&block.instr, jar, &mut args).map_err(|e| e.context("error while parsing command"))?;
  }

  let class =
    jar.decomp("net/minecraft/command/argument/ArgumentTypes", |_, name, _| name == "register")?;

  let (_, _, block) = class.funcs.first().unwrap();
  generate(&block.instr, jar, &mut args).map_err(|e| e.context("error while parsing command"))?;

  Ok(CommandDef { args })
}

fn generate(instr: &[Instr], _jar: &mut Jar, command_args: &mut Vec<Arg>) -> Result<()> {
  for i in instr {
    match i.clone() {
      Instr::Expr(it) => {
        if !it.ops.borrow().is_empty() {
          continue;
        }
        match it.unwrap_initial() {
          Value::StaticCall(_, name, mut args) => match name.as_str() {
            "register" => {
              if matches!(args[0].clone().unwrap_initial(), Value::Var(_)) {
                args.remove(0);
              }
              let arg_name = args[0].clone().unwrap_initial().unwrap_string();
              let arg_class = match args[1].clone().initial {
                Value::Class(c) => c,
                Value::StaticCall(_, name, args) if name == "upcast" => {
                  args[0].clone().initial.unwrap_class()
                }
                v => panic!("unexpected arg class {v:?}"),
              };
              let arg_type = match args[2].clone().initial {
                Value::Class(c) => c,
                Value::StaticCall(class, name, _) if name == "of" => class,
                v => panic!("unexpected arg type {v:?}"),
              };
              let id = command_args.len() as u32;
              command_args.push(Arg {
                name: arg_name,
                id,
                class: arg_class,
                has_extra: arg_type != "net/minecraft/command/argument/ConstantArgumentSerializer",
              });
            }
            _ => todo!("unknown command register name: {name:?}"),
          },
          v => todo!("unknown command {v:?}"),
        }
      }
      _ => {}
    }
  }
  Ok(())
}
