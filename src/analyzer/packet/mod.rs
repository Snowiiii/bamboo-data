use super::{Jar, Version};
use crate::decomp::{Converter, Type};
use anyhow::Result;
use serde::Serialize;
use std::io;

mod conn_enum;
mod convert;
mod java;
// mod util;
// mod writer;

fn is_object(s: &str) -> bool {
  s == "java/lang/Object"
}

#[derive(Debug, Clone, PartialEq)]
pub enum Dir {
  CB,
  SB,
}

#[derive(Debug, Clone, PartialEq, Serialize)]
pub struct Packet {
  /// The sanitized packet name.
  pub name:    String,
  /// The class name of this packet.
  pub class:   String,
  /// The class this packet extends from.
  #[serde(skip_serializing_if = "is_object")]
  pub extends: String,
  /// A list of the fields in this packet.
  pub fields:  Vec<Field>,
  /// A list of instructs to read this packet. These are parsed from java
  /// bytecode, and translated into a more rust-like representation.
  pub reader:  VarBlock,
  /// The same format as the reader, but these instructions should be used for
  /// writing. There are a few differing instructions (like read/writer field),
  /// but the same `Instr` type should be used for both the reader and writer.
  pub writer:  VarBlock,
}

/// The body of a function or closure. This includes a table of all variables to
/// their kind. This is what maps the variable ids to either `this`, an
/// argument, or a local variable.
#[derive(Debug, Clone, PartialEq, Serialize)]
pub struct VarBlock {
  vars:  Vec<VarKind>,
  block: Vec<Instr>,
}

/// The kind of variable this is.
#[derive(Debug, Clone, PartialEq, Serialize)]
pub enum VarKind {
  This,
  Arg,
  Local,
}

#[derive(Debug, Clone, PartialEq, Serialize)]
pub struct Field {
  /// The name of this field.
  pub name: String,
  /// The java type of this field.
  pub ty:   Type,
}

/// A value. Can be a variable reference, a literal, or a function call.
#[derive(Debug, Clone, PartialEq, Serialize)]
pub enum Value {
  /// A null value. This should probably be converted to a `None` value in rust,
  /// but given how complex some of these readers are, it will be a pain to work
  /// with.
  Null,
  /// A literal value.
  Lit(Lit),
  /// A local variable.
  Var(usize),
  /// A packet field. Similar to a local variable, but may require `self.` or
  /// `this.` depending on the language.
  Field(String),
  /// A static field `1` on the class `0`.
  Static(String, String),
  /// An array, with a pre-determined length.
  Array(Box<Expr>),
  /// A static function call. The first item is the class, the second is the
  /// function, and the third is the arguments.
  CallStatic(String, String, Vec<Expr>),
  /// A static method reference. Essentially a function pointer.
  MethodRef(String, String),
  /// A closure call. The first list is a list of arguments for the closure, and
  /// the second list is the instructions inside the closure.
  Closure(Vec<Expr>, VarBlock),
  /// A `class::new()` call.
  New(String, Vec<Expr>),
}

#[derive(Debug, Clone, PartialEq, Serialize)]
pub enum Lit {
  Int(i32),
  Float(f32),
  String(String),
}

/// A rust-like instruction. This can map one-to-one with a subset of Rust
/// statements.
#[derive(Debug, Clone, PartialEq, Serialize)]
pub enum Instr {
  /// This is a very simple call. If this is in the list of instructions, the
  /// entire reader from the superclass of this packet should be inserted here.
  Super,

  /// Sets a field to the given expression. This is the simples instruction, and
  /// it is by far the most common. In simple packets, the entire reader may
  /// just be a list of Set calls.
  Set(String, Expr),
  /// Sets a value in an array. The first item is the array, the second is the
  /// index, and the last one is the value to set it to.
  SetArr(Expr, Value, Expr),

  /// Declares a new variable, and assigns it to the given value. The index is a
  /// java internal feature, and it represents a unique id for each local
  /// variable. An implementation of this might simply call all variables
  /// `var0`, `var1`, etc.
  Let(usize, Expr),

  /// If the given conditional is true, then execute the first list of
  /// instructions. Otherwise, execute the second list.
  If(Cond, Vec<Instr>, Vec<Instr>),
  /// Iterates over the given range of numbers. The variable is a local
  /// variable, which is the value that should be used when iterating (for
  /// example, if var was Var(3), then this might be converted into `for var3 in
  /// ...`).
  For(usize, Range, Vec<Instr>),
  /// A switch statement. The list is a list of keys to blocks that should be
  /// executed. We require that every java switch block has a `break` call at
  /// the end of it.
  Switch(Expr, Vec<(i32, Vec<Instr>)>, Option<Vec<Instr>>),

  /// Invokes the given expresison, and ignores the result. Used when we do
  /// things like call a function that returns void.
  Expr(Expr),

  /// Returns the given value. This will only appear in closures.
  Return(Box<Expr>),
}

/// A range, used in a for loop.
#[derive(Debug, Clone, PartialEq, Serialize)]
pub struct Range {
  /// Start of the range, inclusive.
  min: Expr,
  /// End of range, exclusive.
  max: Expr,
}

/// An expression. Each operation should be applied in order, after the initial
/// value is found.
#[derive(Debug, Clone, PartialEq, Serialize)]
pub struct Expr {
  /// The initial value of this expression. This won't change, but at runtime is
  /// the initial value that will be used when processing the given operators.
  initial: Value,
  /// The operators applied to this expresion. Each operator should be applied
  /// in order, and will mutate the value of this expression.
  #[serde(skip_serializing_if = "Vec::is_empty")]
  ops:     Vec<Op>,
}

#[derive(Debug, Clone, PartialEq, Serialize)]
pub enum Cond {
  Eq(Expr, Expr),
  Neq(Expr, Expr),
  Less(Expr, Expr),
  Greater(Expr, Expr),
  Lte(Expr, Expr),
  Gte(Expr, Expr),

  Or(Box<Cond>, Box<Cond>),
}

#[derive(Debug, Clone, PartialEq, Serialize)]
pub enum Op {
  /// Bitwise and with the given value.
  BitAnd(Expr),
  /// Bitwise or with the given value.
  BitOr(Expr),
  /// Shift right by the given value.
  Shr(Expr),
  /// Unsigned shift right by the given value.
  UShr(Expr),
  /// Shift left by the given value.
  Shl(Expr),

  /// Add the given value to the current value.
  Add(Expr),
  /// Divide the current value by the given amount.
  Div(Expr),
  /// Multiply the current value by the given amount.
  Mul(Expr),

  /// Get the length of the current value. Only valid if the current value is an
  /// array.
  Len,
  /// Get the value at the given index in this array.
  Idx(Expr),
  /// Get the field on the value.
  Field(String),

  /// If the conditional is true, replace the current value with the given
  /// value. Otherwise, do not change the current value, or execute the given
  /// expr.
  If(Box<Cond>, Expr),

  /// Calls the given function on the value. The first element is the class, the
  /// second element is the function name, and the third is the arguments.
  Call(String, String, Vec<Expr>),

  /// Casts the current value to the given type. Will only appear for numbers.
  Cast(Type),
}

pub fn read_packet(p: &str, jar: &mut Jar, dir: Dir) -> Result<Packet> {
  // First pass
  let (name, class, extends, fields, readers) = java::pass(p, jar, dir)?;
  // Second pass
  let reader = convert::pass(extends != "Object", &readers);
  // Sort of third pass, more like second pass part 2.
  // let writer = writer::pass(&reader);

  Ok(Packet {
    name,
    class,
    extends,
    fields,
    reader,
    writer: VarBlock { vars: vec![], block: vec![] },
  })
}

#[derive(Debug, Clone, Default, Serialize)]
struct PacketDefinition {
  clientbound: Vec<Packet>,
  serverbound: Vec<Packet>,
}

pub fn generate(jar: &mut Jar) -> io::Result<impl Serialize> {
  let has_old_conn_enum = jar.ver() < Version::new_release(15, 2);
  let bytes = jar.class_file(if jar.ver().is_old() {
    "net/minecraft/network/EnumConnectionState$2"
  } else if jar.ver() < Version::new_release(15, 2) {
    "net/minecraft/network/NetworkState$2"
  } else {
    "net/minecraft/network/NetworkState"
  });

  let (clientbound_classes, serverbound_classes) = if has_old_conn_enum {
    conn_enum::read_old_conn_enum(bytes)
  } else {
    conn_enum::read_new_conn_enum(bytes)
  }
  .map_err(|e| {
    io::Error::new(io::ErrorKind::Other, format!("error while reading network state: {}", e))
  })?;

  let mut clientbound = vec![];
  let mut serverbound = vec![];

  for class in clientbound_classes {
    let packet = read_packet(&class, jar, Dir::CB).map_err(|e| {
      io::Error::new(
        io::ErrorKind::Other,
        format!("error while reading serverbound packet {}: {}", class, e),
      )
    })?;
    clientbound.push(packet);
  }

  for class in serverbound_classes {
    let packet = read_packet(&class, jar, Dir::SB).map_err(|e| {
      io::Error::new(
        io::ErrorKind::Other,
        format!("error while reading clientbound packet {}: {}", class, e),
      )
    })?;
    serverbound.push(packet);
  }

  Ok(PacketDefinition { clientbound, serverbound })
}
