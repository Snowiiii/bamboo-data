//! This is another part of the second pass. This file handles converting a
//! reader function into a writer function. Most instructions have an equivalent
//! writer function (like reading/writing fields), but other are more complex
//! (like loops). In general, we make a best guess for which value we should use
//! in a for loop.

use super::{Expr, Instr, Lit, Op, Value, Var};
use std::collections::HashMap;

pub fn pass(read: &[Instr]) -> Vec<Instr> {
  let mut vars = HashMap::new();
  pass_inner(read, &mut vars)
}

fn pass_inner(read: &[Instr], vars: &mut HashMap<Var, (usize, Expr)>) -> Vec<Instr> {
  let mut out = vec![];
  for r in read {
    r.to_writer(&mut out, vars);
  }
  out
}

impl Instr {
  fn to_writer(&self, out: &mut Vec<Instr>, vars: &mut HashMap<Var, (usize, Expr)>) {
    let res = match self {
      Instr::Set(field, item) => {
        if item.ops.is_empty() {
          match &item.initial {
            Value::Lit(_) => return,
            Value::Array(arg) => {
              arg.to_writer(Expr::new(Value::Field(field.clone())).op(Op::Len), out, vars);
              return;
            }
            _ => {
              item.to_writer(Expr::new(Value::Field(field.clone())), out, vars);
              return;
            }
          }
        } else if item.ops.len() == 1 {
          match &item.ops[0] {
            Op::Call(_, name, args) => {
              item.to_writer(Expr::new(Value::Field(field.clone())), out, vars);
              return;
            }
            Op::If(cond, _replace) => Instr::If(cond.clone(), vec![], vec![]),
            // Op::BitAnd(op) => {
            //   assert_eq!(item.initial, Var::Buf.into());
            //   Instr::Expr(Expr::new(Var::Buf).op(Op::Call(
            //     convert_reader_name(&name),
            //     vec![Expr {
            //       initial: Value::Field(field.clone()),
            //       ops:     vec![Op::BitAnd(op.clone())],
            //     }],
            //   )))
            // }
            // Op::Div(op) => {
            //   assert_eq!(item.initial, Var::Buf.into());
            //   Instr::Expr(Expr::new(Var::Buf).op(Op::Call(
            //     convert_reader_name(&name),
            //     vec![Expr {
            //       initial: Value::Field(field.clone()),
            //       ops:     vec![Op::Div(op.clone())],
            //     }],
            //   )))
            // }
            _ => unimplemented!("{:?}", self),
          }
        } else {
          panic!("cannot handle ops {:?}", self);
        }
      }
      Instr::SetArr(v, idx, item) => {
        item.to_writer(v.clone().op(Op::Idx(Expr::new(idx.clone()))), out, vars);
        return;
      }
      Instr::Let(var, expr) => {
        vars.entry((*var).into()).or_insert((out.len(), expr.clone()));
        Instr::Let(*var, expr.clone())
      }
      Instr::If(cond, when_true, when_false) => {
        let when_true = pass_inner(when_true, vars);
        let when_false = pass_inner(when_false, vars);
        if when_true.is_empty() && when_false.is_empty() {
          return;
        }
        Instr::If(cond.clone(), when_true, when_false)
      }
      Instr::For(var, range, body) => {
        vars.insert(*var, (out.len(), Expr::lit(0)));
        Instr::For(*var, range.clone(), pass_inner(body, vars))
      }
      Instr::Super => Instr::Super,
      Instr::Expr(e) => {
        match e.ops.len() {
          1 => {
            match &e.ops[0] {
              Op::Call(class, name, args) => {
                match name.as_str() {
                  "add" => {
                    // This is for array.push() or set.insert().
                    //
                    // TODO: We are probably in a loop, and the length of this array should be
                    // used as the loop length.
                    assert_eq!(args.len(), 1, "add takes 1 argument");
                    args[0].to_writer(e.clone(), out, vars);
                    return;
                  }
                  "put" => {
                    // This is for map.put().
                    //
                    // TODO: We are probably in a loop, and the length of this map should be
                    // used as the loop length.
                    assert_eq!(args.len(), 2, "put takes 2 arguments");
                    args[0].to_writer(e.clone(), out, vars);
                    args[1].to_writer(e.clone(), out, vars);
                    return;
                  }
                  "readBytes" => {
                    assert_eq!(
                      e.clone().unwrap_initial(),
                      Value::Var(Var::Buf),
                      "readBytes is a function on Buf"
                    );
                    Instr::Expr(Expr::new(Var::Buf).op(Op::Call(
                      class.clone(),
                      name.clone(),
                      args.clone(),
                    )))
                  }
                  "set" => {
                    // This is for array sets. This is the same as indexing into an array.
                    assert_eq!(args.len(), 2, "set takes 2 arguments (an index and a value)");
                    args[1].to_writer(e.clone(), out, vars);
                    return;
                  }
                  _ => return, // _ => unimplemented!("unknown top level function call {:?}", self),
                }
              }
              _ => todo!(),
            }
          }
          _ => todo!(),
        }
      }
      Instr::Switch(v, table) => Instr::Switch(
        v.clone(),
        table.iter().map(|(key, body)| (*key, pass_inner(body, vars))).collect(),
      ),
      _ => unimplemented!("instr {:?}", self),
    };
    out.push(res);
  }
}

#[track_caller]
fn check_args(name: &str, field: &Expr, args: &[Expr], out: &mut Vec<Instr>) {
  if name == "readStringFromBuffer" || name == "readString" {
    let len;
    if args.is_empty() {
      len = Value::Lit(Lit::Int(2 << 14));
    } else {
      assert!(args.len() == 1, "readStringFromBuffer should take 1 arg {:?}", args);
      len = args[0].clone().unwrap_initial();
    }
    out.push(Instr::CheckStrLen(field.clone(), len));
  } else if name == "readIntArray" {
    assert!(args.is_empty() || args.len() == 1, "readIntArray should take 0 or 1 arg {:?}", args);
  } else if name == "readEnumValue" || name == "readEnumConstant" {
    assert!(args.len() == 1, "readEnumValue should take 1 arg {:?}", args);
  } else if name == "readBytes" {
    assert!(args.len() == 1, "readBytes should take 1 arg {:?}", args);
  } else if name == "readByteArray" {
    assert!(args.len() == 1 || args.is_empty(), "readByteArray should take 0 or 1 args {:?}", args);
  } else if name == "readMap" {
    assert!(args.len() == 2 || args.len() == 3, "readMap should take 2 or 3 args {:?}", args);
  } else if name == "readCollection" {
    assert!(args.len() == 2, "readMap should take 2 args {:?}", args);
  } else if name == "readList" {
    assert!(args.len() == 1, "readList should take 1 args {:?}", args);
  } else if name == "readOptional" {
    assert!(args.len() == 1, "readOptional should take 1 args {:?}", args);
  } else if name == "decode" {
    assert!(args.len() == 1, "decode should take 1 arg {:?}", args);
  } else {
    assert!(args.is_empty(), "did not expect args for buffer reader {} {:?}", name, args);
  }
}

fn convert_reader_name(read: &str) -> String {
  match read {
    "readVarIntFromBuffer" => "writeVarIntToBuffer".into(),
    "readStringFromBuffer" => "writeString".into(),
    "readItemStackFromBuffer" => "writeItemStackToBuffer".into(),
    _ => read.replace("read", "write"),
  }
}

impl Expr {
  fn to_writer(&self, field: Expr, out: &mut Vec<Instr>, vars: &mut HashMap<Var, (usize, Expr)>) {
    match &self.initial {
      Value::CallStatic(class, name, args) => {
        // if let Some(v) = v {
        //   if **v == Expr::new(Var::Buf) {
        //     check_args(name, &field, args, out);
        //     out.push(Instr::Call(
        //       Expr::new_ops(Var::Buf, &self.ops),
        //       convert_reader_name(name),
        //       vec![field],
        //     ));
        //   } else if !args.is_empty() {
        //     args[0].to_writer(field, out, vars);
        //   } else {
        //     out.push(Instr::Call((**v).clone(), name.clone(), vec![]));
        //   }
        if args.is_empty() {
          // This is something like newArrayList().
          out.push(Instr::Expr(Expr::new(Var::Buf).op(Op::Call(
            class.clone(),
            convert_reader_name(name),
            vec![field],
          ))));
        } else {
          // This is usually a getter (something like
          // GameType.getByID()). Because this is a protocol, and not a game
          // implementation, we can just ignore these, and unwrap the first argument.
          args[0].to_writer(field.clone(), out, vars);
        }
      }
      Value::Var(v) => {
        if matches!(v, Var::Local(_)) {
          let (_, v) = &vars[v];
          v.clone().to_writer(field, out, vars);
        }
      }
      Value::New(_name, _values) => {
        // for (i, v) in values.iter().enumerate() {
        //   v.to_writer(field.clone().op(Op::CollectionIdx(i)), out, vars);
        // }
      }
      Value::Array(len) => {
        len.to_writer(field.clone(), out, vars);
      }
      _ => {}
    }
  }

  pub fn op(mut self, op: Op) -> Self {
    self.ops.push(op);
    self
  }
}
