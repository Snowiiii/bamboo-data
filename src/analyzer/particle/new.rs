use super::{super::Jar, Particle, ParticleDef};
use crate::decomp::{Instr, Value};
use anyhow::Result;

pub fn process(jar: &mut Jar) -> Result<ParticleDef> {
  let class =
    jar.decomp("net/minecraft/particle/ParticleTypes", |_, name, _| name == "<clinit>")?;

  let (_, _, block) = class.funcs.first().unwrap();
  let particles =
    generate(&block.instr, jar).map_err(|e| e.context("error while parsing particles"))?;

  Ok(ParticleDef { particles })
}

fn generate(instr: &[Instr], _jar: &mut Jar) -> Result<Vec<Particle>> {
  let mut id = 0;
  let mut particles = vec![];
  for i in instr {
    match i.clone() {
      Instr::SetField(_field_name, it) => {
        if !it.ops.borrow().is_empty() {
          continue;
        }
        match it.unwrap_initial() {
          Value::StaticCall(_, name, args) => match name.as_str() {
            "a" | "register" => {
              let particle_name = args[0].clone().unwrap_initial().unwrap_string();
              particles.push(Particle { name: particle_name, id });
              id += 1;
            }
            _ => todo!("unknown particle register name: {:?}", name),
          },
          v => todo!("unknown particle {:?}", v),
        }
      }
      _ => {}
    }
  }
  Ok(particles)
}
