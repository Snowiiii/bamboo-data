use super::Jar;
use anyhow::Result;
use serde::Serialize;

mod new;
mod old;

#[derive(Debug, Clone, Serialize)]
pub struct Particle {
  name: String,
  id:   u32,
}

#[derive(Debug, Clone, Serialize)]
pub struct ParticleDef {
  particles: Vec<Particle>,
}

pub fn generate(jar: &mut Jar) -> Result<impl Serialize> {
  if jar.ver().is_old() {
    old::process(jar)
  } else {
    new::process(jar)
  }
}
