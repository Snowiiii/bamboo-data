use super::Jar;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, io, io::Read};

#[derive(Clone, Debug, Default, Serialize)]
pub struct TagsDef {
  categories: Vec<TagCategory>,
}

#[derive(Clone, Debug, Serialize)]
pub struct TagCategory {
  name:           String,
  values:         Vec<Tag>,
  #[serde(skip)]
  reverse_values: HashMap<String, Vec<String>>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Tag {
  #[serde(skip_deserializing)]
  name:    String,
  #[serde(default)]
  replace: bool,
  values:  Vec<String>,
}

pub fn generate(jar: &mut Jar) -> io::Result<TagsDef> {
  if jar.ver().is_old() {
    Ok(Default::default())
  } else {
    let mut def = TagsDef::default();
    def.add("block", collect_tags("blocks", jar)?);
    def.add("item", collect_tags("items", jar)?);
    def.add("entity_type", collect_tags("entity_types", jar)?);
    def.add("fluid", collect_tags("fluids", jar)?);
    def.add("game_event", collect_tags("game_events", jar)?);
    Ok(def)
  }
}

impl TagsDef {
  pub fn add(&mut self, category: impl Into<String>, mut tags: TagCategory) {
    tags.name = category.into();
    self.categories.push(tags);
  }
  #[track_caller]
  pub fn get(&self, name: &str) -> &TagCategory {
    for cat in &self.categories {
      if cat.name == name {
        return cat;
      }
    }
    panic!("invalid category {name}");
  }
}
impl TagCategory {
  fn add(&mut self, tag: Tag) {
    self.values.push(tag.clone());
  }
  fn create_reverse(&mut self) {
    let mut reverse_values: HashMap<String, Vec<String>> = HashMap::new();
    for tag in &self.values {
      for value in &tag.values {
        if let Some(rev) = reverse_values.get_mut(value) {
          rev.push(tag.name.clone());
        } else {
          reverse_values.insert(value.clone(), vec![tag.name.clone()]);
        }
      }
    }
    self.reverse_values = reverse_values;
  }
  #[track_caller]
  pub fn get(&self, name: &str) -> Vec<String> {
    self.reverse_values.get(name).cloned().unwrap_or_else(|| vec![])
  }
}

fn collect_tags(path: &str, jar: &mut Jar) -> io::Result<TagCategory> {
  let path = format!("data/minecraft/tags/{path}/");
  let mut cat = TagCategory {
    values:         vec![],
    reverse_values: HashMap::new(),
    name:           String::new(),
  };
  for i in 0..jar.zip().len() {
    let mut file = jar.zip().by_index(i)?;
    if let Some(tag_name) = file.name().strip_prefix(&path) {
      if let Some(tag_name) = tag_name.strip_suffix(".json") {
        let name = format!("#{tag_name}");
        let mut buf = vec![];
        file.read_to_end(&mut buf)?;
        let mut tag: Tag = serde_json::from_slice(&buf)?;
        for value in &mut tag.values {
          if let Some(new_val) = value.strip_prefix("minecraft:") {
            *value = new_val.into();
          } else if let Some(new_val) = value.strip_prefix("#minecraft:") {
            *value = new_val.into();
          } else {
            panic!("invalid tag value: {value}");
          }
        }
        tag.name = name;
        cat.add(tag);
      }
    }
  }
  cat.create_reverse();
  Ok(cat)
}
