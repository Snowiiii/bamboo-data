use crate::Version;
use html_builder::{Buffer, Html5, Node};
use std::{fmt, fmt::Write, fs, io, path::Path};

pub struct Webpage<'a> {
  pub versions: &'a [Version<'a>],
}

impl Webpage<'_> {
  pub fn generate(&self, path: &Path) -> io::Result<()> {
    let mut doc = Buffer::new();
    let mut html = doc.html().attr("lang='en'");
    self.write_head(&mut html.head()).unwrap();
    self.write_body(&mut html.body()).unwrap();

    fs::write(path, doc.finish())?;

    Ok(())
  }

  fn write_head(&self, head: &mut Node) -> fmt::Result {
    writeln!(head.title(), "Bamboo Data Downloads")?;
    writeln!(head.link().attr("rel='preconnect'").attr("href='https://fonts.googleapis.com'"))?;
    writeln!(head.link().attr("rel='preconnect'").attr("href='https://fonts.gstatic.com'"))?;
    writeln!(head
      .link()
      .attr("href='https://fonts.googleapis.com/css2?family=Noto+Sans&display=swap'")
      .attr("rel='stylesheet'"))?;
    writeln!(head.style(), "{}", include_str!("template/style.css"))?;
    writeln!(head.style(), "{}", include_str!("template/rendered.css"))?;
    head.script().attr("src='https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js'");

    Ok(())
  }
  fn write_body(&self, body: &mut Node) -> fmt::Result {
    let mut body = body.div().attr("class='main'");
    writeln!(body.h1(), "Rendered Bamboo Data")?;

    let mut select =
      body.select().attr("name='version'").attr("id='version'").attr("onchange='render()'");
    for ver in self.versions {
      writeln!(select.option().attr(&format!("value='{ver}'")), "{ver}")?;
    }

    body.div().attr("id='data'");

    body.script().attr("src='./zip.min.js'");
    body.script().attr("src='rendered.js'");
    Ok(())
  }
}
