use crate::Version;
use std::{fs, io, path::Path};

mod index;
mod rendered;

pub fn generate(build: &Path, versions: &[Version]) -> io::Result<()> {
  let w = build.join("webpage");
  let data = w.join("data");
  fs::create_dir_all(&data)?;

  let mut protocol = vec![];
  let mut blocks = vec![];
  let mut items = vec![];
  let mut entities = vec![];
  let mut particles = vec![];
  let mut enchantments = vec![];
  let mut commands = vec![];
  let mut tags = vec![];
  let mut all = vec![];

  macro_rules! copy {
    ($p:expr, $v:expr, [$($path:expr => $arr:expr,)*]) => {
      $(
        let name = format!(concat!($path, "-{}.json"), $v);
        let from = $p.join(concat!($path, ".json"));
        if from.exists() {
          fs::copy(from, data.join(&name))?;
          $arr.push(Some(name));
        } else {
          $arr.push(None);
        }
      )*
    };
  }

  for v in versions {
    let p = build.join(v.build_dir()).join("generated");
    copy!(p, v, [
      "protocol" => protocol,
      "blocks" => blocks,
      "items" => items,
      "entities" => entities,
      "particles" => particles,
      "enchantments" => enchantments,
      "commands" => commands,
      "tags" => tags,
      "all" => all,
    ]);
  }

  fs::copy(build.join("all-releases.json"), data.join("all-releases.json"))?;
  fs::copy(build.join("all-releases.json.gz"), data.join("all-releases.json.gz"))?;
  fs::copy(build.join("all-snapshots.json"), data.join("all-snapshots.json"))?;
  fs::copy(build.join("all-snapshots.json.gz"), data.join("all-snapshots.json.gz"))?;

  index::Webpage {
    versions,
    protocol,
    blocks,
    items,
    entities,
    particles,
    enchantments,
    commands,
    tags,
    all,
  }
  .generate(&w.join("index.html"))?;
  rendered::Webpage { versions }.generate(&w.join("rendered.html"))?;
  crate::download::download_file(
    "https://raw.githubusercontent.com/gildas-lormeau/zip.js/master/dist/zip.min.js",
    &w.join("zip.min.js"),
    None,
  )?;
  fs::write(w.join("rendered.js"), include_str!("template/rendered.js"))?;

  info!("webpage stored at {}", w.join("index.html").canonicalize().unwrap().to_str().unwrap());
  Ok(())
}
