use crate::{ReleaseVersion, Version};
use html_builder::{Buffer, Html5, Node};
use std::{fmt, fmt::Write, fs, io, path::Path};

pub struct Webpage<'a> {
  pub versions: &'a [Version<'a>],

  pub protocol:     Vec<Option<String>>,
  pub blocks:       Vec<Option<String>>,
  pub items:        Vec<Option<String>>,
  pub entities:     Vec<Option<String>>,
  pub particles:    Vec<Option<String>>,
  pub enchantments: Vec<Option<String>>,
  pub commands:     Vec<Option<String>>,
  pub tags:         Vec<Option<String>>,
  pub all:          Vec<Option<String>>,
}
impl Webpage<'_> {
  pub fn generate(&self, path: &Path) -> io::Result<()> {
    let mut doc = Buffer::new();
    let mut html = doc.html().attr("lang='en'");
    self.write_head(&mut html.head()).unwrap();
    self.write_body(&mut html.body()).unwrap();

    fs::write(path, doc.finish())?;

    Ok(())
  }

  fn write_head(&self, head: &mut Node) -> fmt::Result {
    writeln!(head.title(), "Bamboo Data Downloads")?;
    writeln!(head.link().attr("rel='preconnect'").attr("href='https://fonts.googleapis.com'"))?;
    writeln!(head.link().attr("rel='preconnect'").attr("href='https://fonts.gstatic.com'"))?;
    writeln!(head
      .link()
      .attr("href='https://fonts.googleapis.com/css2?family=Noto+Sans&display=swap'")
      .attr("rel='stylesheet'"))?;
    writeln!(head.style(), "{}", include_str!("template/style.css"))?;

    Ok(())
  }
  fn write_body(&self, body: &mut Node) -> fmt::Result {
    let mut body = body.div().attr("class='main'");
    writeln!(body.h1(), "Bamboo Data Downloads")?;

    let mut para = body.p();
    write!(para, "All release versions: ")?;
    write!(para, "<a href='data/all-releases.json'>all-releases.json</a>, compressed: ")?;
    write!(para, "<a href='data/all-releases.json.gz'>all-releases.json.gz</a>")?;
    write!(para, "<br>")?;
    write!(para, "All snapshot and release versions: ")?;
    write!(para, "<a href='data/all-snapshots.json'>all-snapshots.json</a>, compressed: ")?;
    write!(para, "<a href='data/all-snapshots.json.gz'>all-snapshots.json.gz</a>")?;

    if self.versions.iter().any(|v| v.release() == ReleaseVersion::next()) {
      writeln!(body.h3(), "{} Snapshots", Version::next_release())?;
      let mut div = body.div().attr("style='margin:auto'");
      self.write_table(&mut div.table(), false, |v| {
        v.is_snapshot() && v.release() == ReleaseVersion::next()
      })?;
    }
    writeln!(body.h3(), "Release Versions")?;
    let mut div = body.div().attr("style='margin:auto'");
    self.write_table(&mut div.table(), false, |v| v.is_release())?;
    writeln!(body.h3(), "All Snapshot Versions")?;
    let mut div = body.div().attr("style='margin:auto'");
    self.write_table(&mut div.table(), true, |v| v.is_snapshot())?;

    writeln!(
      body.footer(),
      "Generated from the <a href='https://gitlab.com/macmv/bamboo-data'>Bamboo Data Generator</a>"
    )?;

    Ok(())
  }

  fn write_table(
    &self,
    tab: &mut Node,
    show_release: bool,
    valid_ver: impl Fn(&Version) -> bool,
  ) -> fmt::Result {
    let columns = vec![
      ("Protocol", &self.protocol),
      ("Blocks", &self.blocks),
      ("Items", &self.items),
      ("Entities", &self.entities),
      ("Particles", &self.particles),
      ("Enchantments", &self.enchantments),
      ("Commands", &self.commands),
      ("Tags", &self.tags),
      ("All Data", &self.all),
    ];

    let mut row = tab.tr();
    if show_release {
      writeln!(row.th().attr("class='corner'"), "Release")?;
      writeln!(row.th().attr("class='top'"), "Version")?;
    } else {
      writeln!(row.th().attr("class='corner'"), "Version")?;
    }
    for (name, _) in &columns {
      writeln!(row.th().attr("class='top'"), "{}", name)?;
    }
    drop(row);
    for (i, ver) in self.versions.iter().enumerate().rev() {
      if !valid_ver(ver) {
        continue;
      }
      let mut row = tab.tr();
      if show_release {
        writeln!(row.th().attr("class='left'"), "{}", ver.release())?;
        writeln!(row.th().attr("class='middle'"), "{ver}")?;
      } else {
        writeln!(row.th().attr("class='left'"), "{ver}")?;
      }
      for (_, col) in &columns {
        match &col[i] {
          Some(text) => writeln!(row.td().a().attr(&format!("href='data/{}'", text)), "{}", text),
          None => writeln!(row.td(), "~missing~"),
        }?;
      }
    }

    Ok(())
  }
}
