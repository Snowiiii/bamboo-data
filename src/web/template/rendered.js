function status(status) {
  let data_div = $("#data")[0];
  data_div.innerHTML = "<h3>" + status + "</h3>";
}

function render() {
  let version = $("#version")[0].value;

  status("Fetching manifest...");

  fetch("https://launchermeta.mojang.com/mc/game/version_manifest.json")
    .then(res => res.json())
    .then(manifest => lookupAssets(version, manifest));
}

function lookupAssets(version, manifest) {
  let found = false;
  let url = "";
  for (let i = 0; i < manifest.versions.length; i++) {
    let ver = manifest.versions[i];
    if (ver.id === version) {
      found = true;
      url = ver.url;
      break;
    }
  }
  if (found) {
    status("Fetching " + version + " manifest...");
    fetch("https://cors-anywhere.herokuapp.com/" + url)
      .then(res => res.json())
      .then(version_manifest => writeWebpage(version, version_manifest));
  } else {
    status("Couldn't find jar for " + version);
  }
}

function asyncReadBlob(blob) {
  return new Promise((resolve, reject) => {
    let reader = new FileReader();

    reader.onload = () => {
      resolve(reader.result);
    };

    reader.onerror = reject;

    reader.readAsDataURL(blob);
  })
}

function writeWebpage(version, version_manifest) {
  status("Downloading client jar...");
  // yolo lol
  let url = "https://cors-anywhere.herokuapp.com/" + version_manifest.downloads.client.url;
  let reader = new zip.ZipReader(new zip.HttpReader(url));
  reader.getEntries().then(entries => {
    status("Reading textures...");
    let textures = {};
    let models = {};
    let promises = [];
    for (let i = 0; i < entries.length; i++) {
      let obj = entries[i];
      if (obj.filename.startsWith("assets/minecraft/textures/")) {
        const texture_name = obj.filename.slice("assets/minecraft/textures/".length);
        let writer = new zip.BlobWriter(["application/png"]);
        let promise = obj.getData(writer).then(res => {
          let promise = asyncReadBlob(writer.blob).then(res => textures[texture_name] = res);
          promises.push(promise);
        });
        promises.push(promise);
      } else if (obj.filename.startsWith("assets/minecraft/models/")) {
        const model_name = obj.filename.slice("assets/minecraft/models/".length);
        let writer = new zip.TextWriter();
        let promise = obj.getData(writer).then(res => {
          models[model_name] = JSON.parse(res);
        });
        promises.push(promise);
      }
    }
    Promise.all(promises).then((_) => {
      let data_div = $("#data")[0];
      data_div.innerHTML = "";
      $.getJSON("./data/all-" + version + ".json", function(data) {
        console.log("data:", data);
        data_div.innerHTML += "<h2>Blocks</h1>";
        data_div.innerHTML += "<p>Note: blocks only show once you hover over them.</p>";
        console.log(data.blocks.blocks);
        for (let i = 0; i < data.blocks.blocks.length; i++) {
          let block = data.blocks.blocks[i];

          let div = document.createElement("div");
          div.classList.add("entry");
          if (i === 0) {
            div.classList.add("first");
          }
          writeBlock(block, div, findTextures(models, textures, block));
          data_div.appendChild(div);
        }
      });
    });
  });
  /*
  fetch(version_manifest.downloads.client.url)
    .then(res => zip.HttpReader(res))
    .then(zip => {
      console.log(zip);
    });
    */

}

function writeBlock(block, div, textures) {
  let left = document.createElement("div");
  let right = document.createElement("div");

  left.innerHTML += "name: <code>minecraft:" + block.name + "</code><br>";
  left.innerHTML += "id: <code>" + block.id + "</code>";
  if (block.properties.length === 0) {
    left.innerHTML += "<br>";
  } else {
    left.innerHTML += " to <code>" + (block.id + numIds(block) - 1) + "</code> (inclusive)<br>";
  }

  if (block.properties.length === 0) {
    left.innerHTML += "properties: none<br>";
  } else {
    let list = document.createElement("ol");
    for (let i = 0; i < block.properties.length; i++) {
      let prop = block.properties[i];
      let elem = document.createElement("li");
      elem.innerHTML += "<code>" + prop.name + "</code>: ";
      if (prop.kind.Enum !== undefined) {
        for (let j = 0; j < prop.kind.Enum.length; j++) {
          if (j != 0) {
            elem.innerHTML += ", ";
          }
          let value = prop.kind.Enum[j];
          elem.innerHTML += "<code>" + value + "</code>";
        }
      } else if (prop.kind.Int !== undefined) {
        elem.innerHTML += "<code>" + prop.kind.Int.min + "</code> to <code>" + prop.kind.Int.max + "</code>, inclusive";
      } else if (prop.kind === "Bool") {
        elem.innerHTML += "bool (<code>true</code> or <code>false</code>)";
      } else {
        console.log(prop);
      }
      elem.innerHTML += " (default: <code>" + prop.default + "</code>)";
      list.appendChild(elem);
    }
    left.innerHTML += "properties: <br>";
    left.appendChild(list);
  }

  for (let [key, value] of Object.entries(block)) {
    if (key === "name" || key === "id" || key === "properties" || key === "drops" || key === "tags") {
      continue;
    }
    if (value === "") {
      left.innerHTML += key + ": ~empty~<br>";
    } else {
      left.innerHTML += key + ": <code>" + value + "</code><br>";
    }
  }

  /*
  if (textures.isCross) {
    right.innerHTML += "<image class='right' width=64 style='image-rendering:pixelated;' src='" + textures.cross + "'>";
  } else {
    right.innerHTML += "<image class='right' width=64 style='image-rendering:pixelated;' src='" + textures.top + "'>";
    right.innerHTML += "<image class='right' width=64 style='image-rendering:pixelated;' src='" + textures.front + "'>";
    right.innerHTML += "<image class='right' width=64 style='image-rendering:pixelated;' src='" + textures.side + "'>";
  }
  */
  let canvas = document.createElement("canvas");
  canvas.classList.add("right");
  canvas.width = 512;
  canvas.height = 512;
  canvas.style = "width:256px;height:256px;";

  right.appendChild(canvas);

  right.setAttribute("style", "margin-left:auto;margin-right:0");
  div.appendChild(left);
  div.appendChild(right);

  if (block.id < 100) {
    let gl = canvas.getContext('webgl');
    textures.draw(canvas, gl);
  }

  function drawDiv(div) {
    let canvas = $($(div).children('div')[1]).children('canvas')[0];
    if (canvas === undefined) {
      return;
    }
    let gl = canvas.getContext('webgl');
    div.textures.draw(canvas, gl);
  }

  div.onmouseenter = function(click) {
    drawDiv(div);
    let prev = div.previousSibling;
    let next = div.nextSibling;
    for (let i = 0; i < 2; i++) {
      if (prev == null || next == null) {
        break;
      }
      drawDiv(prev);
      drawDiv(next);
      prev = prev.previousSibling;
      next = next.nextSibling;
    }
  }
  div.textures = textures;
}

function numIds(block) {
  let total = 1;
  for (let i = 0; i < block.properties.length; i++) {
    let prop = block.properties[i];
    if (prop.kind.Enum !== undefined) {
      total *= prop.kind.Enum.length;
    } else if (prop.kind.Int !== undefined) {
      total *= prop.kind.Int.max - prop.kind.Int.min + 1;
    } else if (prop.kind === "Bool") {
      total *= 2;
    } else {
      console.log(prop);
    }
  }
  return total;
}

class Textures {
  constructor(textures) {
    this.isCross = false;
    this.textures = textures;
  }
  setAll(all) {
    this.top = this.getTexture(all);
    this.side = this.getTexture(all);
    this.front = this.getTexture(all);
  }
  setAllRaw(all) {
    this.top = all;
    this.side = all;
    this.front = all;
  }
  setCross(cross) {
    this.isCross = true;
    this.cross = this.getTexture(cross);
  }
  setTop(top) {
    this.top = this.getTexture(top);
  }
  setSide(side) {
    this.side = this.getTexture(side);
    this.front = this.getTexture(side);
  }

  draw(canvas, gl) {
    var side_data;
    var front_data;
    var top_data;
    if (this.isCross) {
      side_data = [
        -0.40,-0.2,0.0,
        -0.45,0.8,0.0,
        0.5,0.2,0.0,
        0.48,-0.8,0.0,
      ];
      front_data = [
        -0.48,-0.8,0.0,
        -0.5,0.2,0.0,
        0.45,0.8,0.0,
        0.40,-0.2,0.0,
      ];
      top_data = null;
    } else {
      side_data = [
        -0.68,-0.5,0.0,
        -0.7,0.5,0.0,
        0.0,0.2,0.0,
        0.0,-0.8,0.0,
      ];
      front_data = [
        0.0,-0.8,0.0,
        0.0,0.2,0.0,
        0.7,0.5,0.0,
        0.68,-0.5,0.0,
      ];
      top_data = [
        0.0,0.2,0.0,
        0.7,0.5,0.0,
        0.0,0.75,0.0,
        -0.7,0.5,0.0,
      ];
    }
    var uv = [
       0.0,1.0,
       0.0,0.0,
       1.0,0.0,
       1.0,1.0
    ];

    let indices = [3,2,1,3,1,0];

    var side_buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, side_buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(side_data), gl.STATIC_DRAW);
    var front_buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, front_buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(front_data), gl.STATIC_DRAW);
    if (top_data !== null) {
    var top_buffer = gl.createBuffer();
      gl.bindBuffer(gl.ARRAY_BUFFER, top_buffer);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(top_data), gl.STATIC_DRAW);
    }

    var uv_buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, uv_buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(uv), gl.STATIC_DRAW);

    var index_buffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_buffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

    /*====================== Shaders =======================*/

    var vertCode =
      "attribute vec3 coordinates;" +
      "attribute vec2 uvIn;" +
      "varying highp vec2 uv;" +
      "void main(void) {" +
        " gl_Position = vec4(coordinates, 1.0);" +
        " uv = uvIn;" +
      "}";
    var vertShader = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertShader, vertCode);
    gl.compileShader(vertShader);

    var fragCode =
      "varying highp vec2 uv;" +
      "uniform sampler2D texture;" +
      "void main(void) {" +
        " gl_FragColor = texture2D(texture, uv);" +
      "}";
    var fragShader = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragShader, fragCode);
    gl.compileShader(fragShader);

    var shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertShader);
    gl.attachShader(shaderProgram, fragShader);
    gl.linkProgram(shaderProgram);

    gl.useProgram(shaderProgram);

    const level = 0;
    const internalFormat = gl.RGBA;
    const srcFormat = gl.RGBA;
    const srcType = gl.UNSIGNED_BYTE;

    const top = gl.createTexture();
    const side = gl.createTexture();
    const front = gl.createTexture();

    const width = 1;
    const height = 1;
    const border = 0;
    gl.bindTexture(gl.TEXTURE_2D, top);
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
      width, height, border, srcFormat, srcType,
      new Uint8Array([255, 0, 255, 255]));
    gl.bindTexture(gl.TEXTURE_2D, side);
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
      width, height, border, srcFormat, srcType,
      new Uint8Array([255, 0, 255, 255]));
    gl.bindTexture(gl.TEXTURE_2D, front);
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
      width, height, border, srcFormat, srcType,
      new Uint8Array([255, 0, 255, 255]));

    let textures = this;
    if (!this.isCross) {
      if (textures.top === undefined || textures.side === undefined || textures.front === undefined) {
        return;
      }
      const topImg = new Image();
      topImg.onload = function() {
        gl.bindTexture(gl.TEXTURE_2D, top);
        gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, topImg);

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

        const sideImg = new Image();
        sideImg.onload = function() {
          gl.bindTexture(gl.TEXTURE_2D, side);
          gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, sideImg);

          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
          gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

          const frontImg = new Image();
          frontImg.onload = function() {
            gl.bindTexture(gl.TEXTURE_2D, front);
            gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, frontImg);

            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

            finishDraw();
          };
          frontImg.src = textures.front;
        };
        sideImg.src = textures.side;
      };
      topImg.src = textures.top;
    } else {
      if (textures.cross === undefined) {
        return;
      }
      const sideImg = new Image();
      sideImg.onload = function() {
        gl.bindTexture(gl.TEXTURE_2D, side);
        gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, sideImg);

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

        finishDraw();
      };
      sideImg.src = textures.cross;
    }

    function finishDraw() {

      /* ======= Associating shaders to buffer objects =======*/

      gl.bindBuffer(gl.ARRAY_BUFFER, uv_buffer);
      var coord = gl.getAttribLocation(shaderProgram, "uvIn");
      gl.vertexAttribPointer(coord, 2, gl.FLOAT, false, 0, 0);
      gl.enableVertexAttribArray(coord);

      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, side);
      gl.activeTexture(gl.TEXTURE1);
      gl.bindTexture(gl.TEXTURE_2D, front);
      gl.activeTexture(gl.TEXTURE2);
      gl.bindTexture(gl.TEXTURE_2D, top);

      var textureLoc = gl.getUniformLocation(shaderProgram, "texture");

      /*============= Drawing the Quad ================*/

      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_buffer);

      gl.enable(gl.BLEND);
      gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
      gl.enable(gl.DEPTH_TEST);
      gl.depthMask(false);

      gl.clearColor(0.0, 0.0, 0.0, 0.0);
      gl.clear(gl.COLOR_BUFFER_BIT);
      gl.viewport(0, 0, canvas.width, canvas.height);

      var coord = gl.getAttribLocation(shaderProgram, "coordinates");

      gl.bindBuffer(gl.ARRAY_BUFFER, side_buffer);
      gl.vertexAttribPointer(coord, 3, gl.FLOAT, false, 0, 0);
      gl.enableVertexAttribArray(coord);
      gl.uniform1i(textureLoc, 0);
      gl.drawElements(gl.TRIANGLES, indices.length, gl.UNSIGNED_SHORT, 0);

      gl.bindBuffer(gl.ARRAY_BUFFER, front_buffer);
      gl.vertexAttribPointer(coord, 3, gl.FLOAT, false, 0, 0);
      gl.enableVertexAttribArray(coord);
      if (!textures.isCross) {
        gl.uniform1i(textureLoc, 1);
      }
      gl.drawElements(gl.TRIANGLES, indices.length, gl.UNSIGNED_SHORT, 0);

      if (!textures.isCross) {
        gl.bindBuffer(gl.ARRAY_BUFFER, top_buffer);
        gl.vertexAttribPointer(coord, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(coord);
        gl.uniform1i(textureLoc, 2);
        gl.drawElements(gl.TRIANGLES, indices.length, gl.UNSIGNED_SHORT, 0);
      }
    }
  }

  getTexture(name) {
    if (name === undefined) {
      return
    }
    if (name.startsWith("minecraft:")) {
      name = name.slice("minecraft:".length);
    }
    return this.textures[name + ".png"];
  }
}

function drawCanvas(canvas) {
  console.log(canvas);
}

function findTextures(models, textures, block) {
  let res = new Textures(textures);
  let model = models["block/" + block.name + ".json"];
  if (model !== undefined && model.textures !== undefined) {
    let tex = model.textures;
    if (block.name === "oak_log") {
      console.log(tex);
    }
    if (tex.rail !== undefined) {
      res.setAll(tex.rail);
    } else if (tex.stem !== undefined) {
      res.setAll(tex.stem);
    } else if (tex.pattern !== undefined) {
      res.setAll(tex.pattern);
    } else if (tex.stem !== undefined) {
      res.setAll(tex.stem);
    } else if (tex.fire !== undefined) {
      res.setAll(tex.fire);
    } else if (tex.fan !== undefined) {
      res.setAll(tex.fan);
    } else if (tex.lantern !== undefined) {
      res.setAll(tex.lantern);
    } else if (tex.particle !== undefined) {
      res.setAll(tex.particle);
    } else if (textures["block/" + block.name + ".png"] !== undefined) {
      res.setAllRaw(textures["block/" + block.name + ".png"]);
    } else if (textures["blocks/" + block.name + ".png"] !== undefined) {
      res.setAllRaw(textures["blocks/" + block.name + ".png"]);
    }
    if (tex.all !== undefined) {
      res.setAll(tex.all);
    } else if (tex.texture !== undefined) {
      res.setAll(tex.texture);
    } else if (tex.torch !== undefined) {
      res.setAll(tex.texture);
    } else if (tex.wool !== undefined) {
      res.setAll(tex.wool);
    }
    if (tex.plant !== undefined) {
      res.setCross(tex.plant);
    } else if (tex.cross !== undefined) {
      res.setCross(tex.cross);
    }
    if (tex.top !== undefined) {
      res.setTop(tex.top);
    } else if (tex.up !== undefined) {
      res.setTop(tex.up);
    } else if (tex.end !== undefined) {
      res.setTop(tex.end);
    }
    if (tex.side !== undefined) {
      res.setSide(tex.side);
    }
  } else if (textures["block/" + block.name + ".png"] !== undefined) {
    res.setAllRaw(textures["block/" + block.name + ".png"]);
  } else if (textures["blocks/" + block.name + ".png"] !== undefined) {
    res.setAllRaw(textures["blocks/" + block.name + ".png"]);
  } else {
    console.log("couldn't find texture for " + block.name);
  }
  return res;
}

render();
