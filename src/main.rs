#![allow(clippy::single_match, clippy::too_many_arguments)]

#[macro_use]
extern crate log;

use ansi_term::Color;
use clap::Parser;
use flate2::{write::GzEncoder, Compression};
use pprof::protos::Message;
use rayon::prelude::*;
use std::{
  fs,
  fs::File,
  io,
  io::{BufWriter, Write},
  path::PathBuf,
  time::Instant,
};

mod analyzer;
mod decomp;
mod download;
mod extract;
mod mojang;
mod time;
mod version;
mod web;

pub use time::{time, PrettyDuration};
pub use version::{ReleaseVersion, SnapshotVersion, Version};

#[derive(Parser, Debug)]
#[clap(verbatim_doc_comment)]
/// Bamboo Data Extractor
///
/// There are four stages to the generator:
///
/// mc:       Downloads the client jars from mojang
/// mappings: Downloads mcp/yarn mappings
/// analyze:  Analyzes the jars using the mappings
/// webpage:  Generates a webpage to display the resulting json
///
/// All of these can be disabled with `no_` flags, or set to just perform
/// one step using one of the `only_` flags
struct Args {
  /// The build directory, where all output is stored.
  #[clap(short, long, default_value = "build")]
  build: String,

  /// If set, downloading the minecraft versions will be skipped. If not already
  /// downloaded, this may cause things to break.
  #[clap(long)]
  no_mc:       bool,
  /// If set, the mcp/yarn mappings will be skipped, and the program will
  /// assume that all mapping files have already been extracted. If the mappings
  /// are not already downloaded, this may cause things to break.
  #[clap(long)]
  no_mappings: bool,
  /// If set, the class files will not be analyzed. If the generated json isn't
  /// present, this may break things.
  #[clap(long)]
  no_analyze:  bool,
  /// If set, the webpage will be skipped.
  #[clap(long)]
  no_webpage:  bool,

  /// If set, the vanilla jars will be downloaded, and then the program will
  /// exit.
  #[clap(long)]
  only_mc:       bool,
  /// If set, only the mcp/yarn mappings will be be downloaded.
  #[clap(long)]
  only_mappings: bool,
  /// If set, the class files will be analyzed, and all other steps will be
  /// skipped.
  #[clap(long)]
  only_analyze:  bool,
  /// If set, only the webpage will be generated.
  #[clap(long)]
  only_webpage:  bool,

  /// A list of major versions to process. If left empty, all versions will be
  /// processed.
  ///
  /// Versions can be on one of these formats:
  /// 19:         Major release version, which uses the latest minor version
  /// 1.19.1:     Specific release version
  /// 1.19.1-rc2: Pre-release or release candidate
  /// 22w24a:     Snapshot version
  #[clap(verbatim_doc_comment, short, long)]
  versions: Vec<String>,

  /// If set, all json outputs will be generated with the 'pretty' formatting.
  /// This will indent nested structures with 2 spaces, instead of just placing
  /// everything on one line.
  ///
  /// This is much less space efficient, but makes the files easy to read.
  #[clap(short, long)]
  pretty: bool,

  /// If set, the logger will print line numbers.
  #[clap(short, long)]
  lines:   bool,
  /// If set, a profiler will run, and generate `pprof.pb`, which can be opened
  /// like so: `go tool pprof -http 0.0.0.0:6060 pprof.pb`.
  #[clap(long)]
  profile: bool,
}

macro_rules! bool_arg {
  ( $name:ident, $only_name:ident, $no_name:ident) => {
    fn $name(&self) -> bool {
      if self.$only_name {
        true
      } else if self.has_only() {
        false
      } else {
        !self.$no_name
      }
    }
  };
}

impl Args {
  bool_arg!(mc, only_mc, no_mc);
  bool_arg!(mappings, only_mappings, no_mappings);
  bool_arg!(analyze, only_analyze, no_analyze);
  bool_arg!(webpage, only_webpage, no_webpage);

  fn has_only(&self) -> bool {
    self.only_mc || self.only_mappings || self.only_analyze || self.only_webpage
  }
}

fn main() {
  let args = Args::parse();
  let guard = if args.profile {
    info!("starting cpu profiler");
    Some(pprof::ProfilerGuard::new(100).unwrap())
  } else {
    None
  };
  match run(args) {
    Ok(_) => {}
    Err(e) => {
      error!("{}", e);
    }
  }
  if let Some(guard) = guard {
    match guard.report().build() {
      Ok(report) => {
        let mut file = File::create("pprof.pb").unwrap();
        let profile = report.pprof().unwrap();

        let mut content = Vec::new();
        profile.encode(&mut content).unwrap();
        file.write_all(&content).unwrap();
      }
      Err(e) => {
        error!("failed to generate report: {}", e);
      }
    };
  }
}

fn run(args: Args) -> io::Result<()> {
  fs::create_dir_all(&args.build)?;
  init(args.lines);

  let build = PathBuf::new().join(&args.build);

  let versions = if args.versions.is_empty() {
    version::VERSIONS.to_vec()
  } else if args.versions.len() == 1 {
    match args.versions[0].as_str() {
      "all" => version::VERSIONS.to_vec(),
      "all-release" => version::VERSIONS.iter().filter(|v| v.is_release()).cloned().collect(),
      v => match Version::from_str(v) {
        Ok(v) => vec![v],
        Err(e) => {
          error!("{e}");
          vec![]
        }
      },
    }
  } else {
    args
      .versions
      .iter()
      .flat_map(|v| match Version::from_str(v) {
        Ok(v) => Some(v),
        Err(e) => {
          error!("{e}");
          None
        }
      })
      .collect()
  };

  if args.mc() {
    let start = Instant::now();
    info!("> DOWNLOADING minecraft versions");
    mojang::download_all(&build, &versions)?;
    info!("> FINISHED downloading minecraft versions in {}", time(start));
  }

  if args.mappings() {
    let start = Instant::now();
    info!("> DOWNLOADING mcp and yarn");

    macro_rules! mcp {
      ( $ver:expr => $url:expr ) => {
        if versions.contains(&$ver) {
          extract::mcp($url, &format!("build/{}", $ver.build_dir()))?;
        }
      };
    }

    mcp!(Version::new_release(8, 9) => "http://www.modcoderpack.com/files/mcp918.zip");
    mcp!(Version::new_release(9, 4) => "http://www.modcoderpack.com/files/mcp928.zip");
    mcp!(Version::new_release(10, 2) => "http://www.modcoderpack.com/files/mcp931.zip");
    mcp!(Version::new_release(11, 2) => "http://www.modcoderpack.com/files/mcp937.zip");
    mcp!(Version::new_release(12, 2) => "http://www.modcoderpack.com/files/mcp940.zip");

    let mut yarn = None;
    for version in &versions {
      if version.maj() < 14 {
        continue;
      }
      if yarn.is_none() {
        extract::intermediary("build/intermediary")?;
        yarn = Some(extract::yarn("build/yarn")?);
      }
      extract::copy_yarn(
        "build/intermediary",
        yarn.as_ref().unwrap(),
        &format!("build/{}", version.build_dir()),
        version,
      )?;
    }

    info!("> FINISHED downloading mcp and yarn in {}", time(start));
  }

  if args.analyze() {
    let start = Instant::now();
    info!("> ANALYZING versions");
    let outputs: Vec<_> = versions
      .par_iter()
      .flat_map(|&ver| match analyzer::analyze(&build, ver, args.pretty) {
        Ok(out) => Some((ver, out)),
        Err(e) => {
          error!("could not process version {ver}: {:?}", e);
          None
        }
      })
      .collect();
    {
      let start = Instant::now();
      info!("writing versions to disk...");
      for (ver, out) in &outputs {
        match out.to_disk() {
          Ok(()) => {}
          Err(e) => error!("could not write version {ver} to disk: {:?}", e),
        }
      }
      {
        let all_snap_json = outputs
          .iter()
          .map(|(ver, out)| (ver.to_string(), &out.all))
          .collect::<std::collections::HashMap<_, _>>();
        let data = if args.pretty {
          serde_json::to_string_pretty(&all_snap_json).unwrap()
        } else {
          serde_json::to_string(&all_snap_json).unwrap()
        };
        File::create(build.join("all-snapshots.json"))?.write_all(data.as_bytes())?;
        let f = BufWriter::new(File::create(build.join("all-snapshots.json.gz"))?);
        GzEncoder::new(f, Compression::best()).write_all(data.as_bytes())?;
      }
      {
        let all_release_json = outputs
          .iter()
          .flat_map(
            |(ver, out)| if ver.is_release() { Some((ver.to_string(), &out.all)) } else { None },
          )
          .collect::<std::collections::HashMap<_, _>>();
        let data = if args.pretty {
          serde_json::to_string_pretty(&all_release_json).unwrap()
        } else {
          serde_json::to_string(&all_release_json).unwrap()
        };
        File::create(build.join("all-releases.json"))?.write_all(data.as_bytes())?;
        let f = BufWriter::new(File::create(build.join("all-releases.json.gz"))?);
        GzEncoder::new(f, Compression::best()).write_all(data.as_bytes())?;
      }
      info!("finished writing versions to disk in {}", time(start));
    }
    info!("> FINISHED analyzing versions in {}", time(start));
  }

  if args.webpage() {
    info!("> GENERATING webpage");
    web::generate(&build, &versions)?;
    info!("> FINISHED generating webpage");
  }

  Ok(())
}

/// Initializes logger
pub fn init(show_line: bool) {
  fern::Dispatch::new()
    .format(move |out, message, record| {
      out.finish(format_args!(
        "{}{} [{}] {}",
        chrono::Local::now().format("%Y-%m-%d %H:%M:%S:%f"),
        {
          if show_line {
            format!(" {}:{}", record.file().unwrap(), record.line().unwrap())
          } else {
            "".into()
          }
        },
        match record.level() {
          l @ log::Level::Error => Color::Red.bold().paint(l.to_string()),
          l @ log::Level::Warn => Color::Yellow.paint(l.to_string()),
          l @ log::Level::Info => Color::Green.paint(l.to_string()),
          l @ log::Level::Debug => Color::Blue.paint(l.to_string()),
          l => Color::White.paint(l.to_string()),
        },
        message
      ))
    })
    .level(log::LevelFilter::Debug)
    .level_for("ureq", log::LevelFilter::Info)
    .level_for("rustls", log::LevelFilter::Info)
    .chain(download::progress::ProgressOutput)
    .chain(fern::log_file("build/build.log").unwrap())
    .apply()
    .unwrap();
}
