# Bamboo Data Generator

Output link: https://macmv.gitlab.io/bamboo-data

This is a tool written for my server, [Bamboo](https://gitlab.com/macmv/bamboo). It is written
primarily for Rust, but the output is portable to other languages. You can see the most recent output
[here](https://macmv.gitlab.io/bamboo-data).

In order to use this tool, I recommend downloading everything from
https://macmv.gitlab.io/bamboo-data. This page is updated anytime I push to this respository, and
will be stable after I am finished with development of the parser. That means its not entirely
stable yet! The protocol reader/writer format is ugly, and that is subject to change at any point.
Everything else that is present is stable, as things like block data aren't going to change at all.
Any of the data may expand at any point (including in the far future), so your reader should not fail
if there are new fields added to the json. This is so I can add things like block hardness without
breaking anyone's tools.

Also note that the gitlab version only contains full releases of minecraft. If you use the tool locally,
you can access all snapshot versions as well.

## Features

- [x] Protocol Data
  - [x] Packet fields
    - These are the java fields of each packet class.
  - [x] Protocol reader (read from bytecode)
    - As far as I have bothered to check, these are complete, but there are bound to be errors.
  - [ ] Protocol writer (generated from the reader)
    - This is never going to be completed. Instead, it makes more sense to generate the writer in the server.
  - This is by far the most complex dataset in the entire output. I decided to parse the `read`
    function of all the packets, and essentially spit out an AST into json. Because fernflower is
    for suckers, I decided to generate this AST from the bytecode. The main problem is that the AST
    is mostly formed from Rust enums, so the output json is in a terrible format. Please send a PR
    if you can think of a good way to improve this.
- [x] Block Data
  - [x] Block names/classes/ids
  - [x] Block material
  - [x] Block hardness, resistance, etc.
  - [x] Block states/properties (1.13+).
  - Blocks are complex in general, so I'm not going to list every block propery here.
- [ ] Item Data
  - [x] Item names/classes/ids
  - [ ] Store what block each item places. Maybe even parse things like water buckets.
- [x] Entity Data
  - [x] Entity names/classes/ids
  - [ ] Entity metadata fields
    - Required for pretty much any custom functionality for entities.
  - [ ] Some inheritence structure, so that entity metadata will be easy.
  - [ ] Some behavioral category, so I can easily given all hostile mobs the same AI.
- [ ] Particles Data
  - [ ] Particle names/classes/ids
  - 1.16 go brrr, names need to be accurate, as they are used in the protocol (they switched from
    ids to named particles in 1.16, for whatever reason).
- [ ] Potions Data
  - I don't know much about potions, so this includes all status effects and potion recipes.
- [ ] Biomes Data
  - Because 1.16 go brrr, this means I need the entire overworld, nether, and end biome registries.
    This checkbox includes all of that data.

## Design

This data generator downloads all the vanilla client jars (from 1.8 up). This tool then reads the
deobfuscation mappings from either [mcp](https://www.modcoderpack.com/) or
[yarn](https://github.com/FabricMC/yarn). Instead of deobfuscating the jars, I decided to just start
reading them while obfuscated, and deobfuscate things on the fly. This means I do very little disk
I/O, as I all need to read are some csv files and a single jar off disk.

Because I dediced that fernflower is for suckers, I wrote a decompiler. It reads bytecode, and generates
an AST from that bytecode. This includes if statements, for loops, and switch statements. It has
worked well for everything I've tried to decompile so far, but is still very much in development.
Because I don't use fernflower, this tool is very fast. It takes a little under a second to generate
all of the protocol and block data for a single version.

The reason I need this AST is because of the protocol. The
[Prismarine Data](https://github.com/PrismarineJS/minecraft-data) repository was a great starting
point, and it had almost everything I needed. However, for complex packets, it didn't have a good
way of representing the output data and the packet reader as seperate concepts. It also does a lot
of things that work well in javascript (like writing to an array at an out of bounds index, which
will extend the array for you) which make a Rust implementation very akwards and backwards.

So, I decided to represent the packet in three fields: the data of the packet, the reader function,
and the writer function. These are all easy to parse from the minecraft jar, and will create a very
structured packet definition that will be easy to port to Rust. The one problem with the minecraft
jar is their writer functions; they are generally very inconsistent, and hard to understand (from
a computer's perspective). So, I have opted to reverse the reader function AST instead of parsing
another function from bytecode. This is the most error-prone part of the protocol, but it will be
fixed up once I start using this data in my server.
