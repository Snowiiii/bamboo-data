Upgrading this repository is where most of the work is for upgrading `bamboo`. It requires
a lot of manual intervention, because mojang changes things all time time. Also, this repo
needs to keep every old version present (including minor versions), so that past commits of
`bamboo` can still build.

The first thing to change is the macro call to `versions` list in `version/mod.rs`. Even if
this is a minor bump, a new entry must be added, so that we have all the versions that
`bamboo` has depended on in the past.

The next thing is `Version::from_num`. The argument is a major version, and that should map
to the latest minor version. So, in this function, you should replace the match for latest
with the new minor version. In my case, I replaced `18 => 1.18.1` with `18 => 1.18.2`.

Finally, update `ReleaseVersion::latest`, in order to get the webpage to display correctly.

Now, once these have been updated, try running the program. Make sure to not pass the
`--no-download` and `--no-extract` flags, as you will need to actually download a new
version.

In my case, everything worked, right up until the entity analyzer hit for 1.18.2. This
immediately crashed, with a nonsense looking error:
```
thread '<unnamed>' panicked at 'unknown class for entity Entity {
  id: 27,
  name: "falling_block",
  class: "",
  tags: [],
  category: "",
  width: 0.0,
  height: 0.0,
  fire_immune: false,
  tracking_range: 0,
  metadata: []
}',
src/analyzer/entity/new.rs:62:11
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
```

So, if you get any errors, go fix them, and then run the extractor with `--webpage`,
and make sure everything looks correct. If it does, you're done! Push to `main`, and
let the CI update. Once the public page is done, you can go update `bamboo`.

I am just going to write my steps here, to make sure I don't miss anything.

- Add version to `VERSIONS` and to the `Version::from_num` function.
- Run `cargo run --release -- -p --webpage`, see what happens
- Got error:
  - Message:
    ```
    thread '<unnamed>' panicked at 'unexpected indent 3', src/analyzer/convert.rs:343:18
    note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
    ```
  - Yarn changed their mapping format. They added some indents, *somewhere*
  - I'm now cloning `yarn` to see what the mapping is.
  - The new mapping is just more nested classes. I can actually ignore this, so I replaced
    the `panic!` with a `warn!`
- After re-running, I got this:
  ```
  could not process version 1.19: Custom { kind: InvalidData, error: Error("missing field `replace`", line: 8, column: 1) }
  ```
  - I ran `rg "replace"`, which showed me the a field `replace` in `tag/mod.rs`.
  - I added `#[serde(default)]` to this field.
- After re-running, I got this:
  ```
  thread '<unnamed>' panicked at 'no entry found for key', src/analyzer/block/new.rs:921:42
  note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
  ```
  - I added some `dbg!()` calls, and it turns out it's a problem with the property fields
    of `net/minecraft/block/MultifaceGrowthBlock`
  - I then decompiled `yarn` for 1.19, to read the source of this class.
  - `MultifaceGrowthBlock` is the new `AbstractGlowLichen` block. Updated the property analyzer
    to match this.
- After re-runnning, I got this:
  ```
  thread 'main' panicked at 'multifaceGrowthDrops', src/analyzer/block/new.rs:85:37
  ```
  - This is a new drops function in the loot table. This does... something. Idk what.
  - I'm going to ignore this, as the function looks complicated.
- Fixed block drop edge case for `mangrove_slab`
- Added new entity metadata
- Fixed frog width and height edge case

After all of this, it works! I should probably trim this down, but I think it's somewhat valuable
to leave this here (at least for now).
